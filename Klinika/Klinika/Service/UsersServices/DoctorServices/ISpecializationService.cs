﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.DoctorServices
{
    public interface ISpecializationService
    {
        Boolean CreateSpecialization(Model.users.doctor.TypeOfSpecialization specialization);
        Boolean EditSpecialization(Model.users.doctor.TypeOfSpecialization specialization);
        Boolean DeleteSpecialization(Model.users.doctor.TypeOfSpecialization specialization);
        List<Model.users.doctor.TypeOfSpecialization> GetAllSpecializations();
        Model.users.doctor.TypeOfSpecialization GetSpecializationById(String Id);

    }
}
