﻿using Controller.HospitalController;
using Model.Storage;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for MedicineView.xaml
    /// </summary>
    public partial class MedicineView : UserControl
    {
        public MedicineView()
        {
            InitializeComponent();
            dataGridMedicine.ItemsSource = App.MedicineController.GetAllMedicines();
            dataGridMedicine.HeadersVisibility = DataGridHeadersVisibility.Column;
            statusComboBox.ItemsSource = Enum.GetValues(typeof(MedicineStatus));
        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Medicine medicine = (Medicine)dataGridMedicine.SelectedItem;
            if (medicine == null) 
            {
                MessageBoxInformation message = new MessageBoxInformation("Error", "Please select item");
                message.Show();
            }
            medicine.Status = (MedicineStatus)statusComboBox.SelectedItem;
            if (medicine.Status == MedicineStatus.approved)
            {
                medicine.ApprovalDate = DateTime.Now;
            }
            App.MedicineController.EditMedicine(medicine);
            dataGridMedicine.ItemsSource = null;
            dataGridMedicine.ItemsSource = App.MedicineController.GetAllMedicines();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
