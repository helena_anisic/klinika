﻿using Controller.HospitalController;
using Controller.UsersController.ManagerController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for ZakaziRenoviranje.xaml
    /// </summary>
    public partial class ZakaziRenoviranje : Page
    {
        DateTime dt;
        public ZakaziRenoviranje()
        {
            String date = DateTime.Now.ToString("dd.MM.yyyy");
            InitializeComponent();
            if (App.Pomoc == false)
                upitnik.Visibility = Visibility.Hidden;
            else
                upitnik.Visibility = Visibility.Visible;
            DataGridHeader.Header = date;

            god.SelectedIndex = DateTime.Now.Year - 2020;
            mes.SelectedIndex = DateTime.Now.Month - 1;

            foreach (Room r in App.RoomController.GetAllRooms())
            {
                if(!r.id.Equals("Magacin"))
                {
                    sala.Items.Add(r);
                    salaZ.Items.Add(r);
                }
            }

            sala.SelectedIndex = 0;
            salaZ.SelectedIndex = 0;
            Calendar.ItemsSource = RenoviranjaCalendar.Data(DateTime.Now, ((Room)sala.SelectedItem).id);

            dt = DateTime.Now;
            MedOprema.ItemsSource = TerminiData.Data(dt, ((Room)sala.SelectedItem).id);
            popuniListeTermina();
        }

        private void God_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mes.SelectedIndex == -1)
                return;
            dt = new DateTime(god.SelectedIndex + 2020, mes.SelectedIndex + 1, 1);
            Calendar.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void Mes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dt = new DateTime(god.SelectedIndex + 2020, mes.SelectedIndex + 1, 1);
            if(sala.SelectedItem != null)
                Calendar.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void Calendar_SelectionChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (Nazad.Visibility == Visibility.Visible)
                return;

            int kolona = Calendar.SelectedCells[0].Column.DisplayIndex;
            int red = Calendar.Items.IndexOf(Calendar.SelectedCells[0].Item);
            int index = 7 * red + kolona; ;
            DateTime date = new DateTime(Int16.Parse(god.SelectedIndex.ToString()) + 2020, Int16.Parse(mes.SelectedIndex.ToString()) + 1, 1);
            int firstIndexOfCurrentMonth = (int)date.DayOfWeek;
            if (firstIndexOfCurrentMonth == 0)
                firstIndexOfCurrentMonth = 7;
            int lastIndexOfCurrentMonth = firstIndexOfCurrentMonth + DateTime.DaysInMonth(Int16.Parse(god.SelectedIndex.ToString()) + 2020, Int16.Parse(mes.SelectedIndex.ToString()) + 1);
            if (index >= firstIndexOfCurrentMonth && index <= lastIndexOfCurrentMonth)
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            else
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }

            dt = date;

            String dateString = date.ToString("dd.MM.yyyy");
            MedOprema.ItemsSource = TerminiData.Data(dt, ((Room)sala.SelectedItem).id);
            DataGridHeader.Header = dateString;
            datumOd.SelectedDate = date;
            datumDo.SelectedDate = date;
            Calendar.Visibility = Visibility.Hidden;
            god.Visibility = Visibility.Hidden;
            mes.Visibility = Visibility.Hidden;
            GodinaLabel.Visibility = Visibility.Hidden;
            MesecLabel.Visibility = Visibility.Hidden;
            sala.Visibility = Visibility.Hidden;
            salaLbl.Visibility = Visibility.Hidden;
            MedOprema.Visibility = Visibility.Visible;
            Nazad.Visibility = Visibility.Visible;
            MedOprema.ItemsSource = TerminiData.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void Nazad_Click(object sender, RoutedEventArgs e)
        {
            Calendar.UnselectAllCells();
            Nazad.Visibility = Visibility.Collapsed;
            MedOprema.Visibility = Visibility.Hidden;
            Calendar.Visibility = Visibility.Visible;
            god.Visibility = Visibility.Visible;
            mes.Visibility = Visibility.Visible;
            GodinaLabel.Visibility = Visibility.Visible;
            MesecLabel.Visibility = Visibility.Visible;
            sala.Visibility = Visibility.Visible;
            salaLbl.Visibility = Visibility.Visible;
            datumOd.SelectedDate = null;
            datumDo.SelectedDate = null;
            satiOd.SelectedIndex = -1;
            satiDo.SelectedIndex = -1;
        }

        private void MeoOprema_SelectionChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count <= 0)
                return;
            int prviRed = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            int drugiRed = MedOprema.Items.IndexOf(MedOprema.SelectedCells[MedOprema.SelectedCells.Count - 1].Item);
            satiOd.SelectedIndex = prviRed;
            satiDo.SelectedIndex = drugiRed + 1;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            String[] sTime = ((String)satiOd.SelectedItem).Split(':');
            String[] eTime = ((String)satiDo.SelectedItem).Split(':');

            int sH = Int32.Parse(sTime[0]);
            int sM = Int32.Parse(sTime[1]);
            int eH = Int32.Parse(eTime[0]);
            int eM = Int32.Parse(eTime[1]);
            DateTime sdt = datumOd.SelectedDate.Value;
            DateTime edt = datumDo.SelectedDate.Value;
            sdt = sdt.AddHours(sH);
            sdt = sdt.AddMinutes(sM);
            edt = edt.AddHours(eH);
            edt = edt.AddMinutes(eM);
            Renovation ren = new Renovation() { StartDateTime = sdt, EndDateTime = edt, Description = description.Text, Id = rnd.Next(100,10000).ToString(), RoomId = ((Room)salaZ.SelectedItem).id };
            App.RenovationController.CreateRenovation(ren);

            MedOprema.ItemsSource = TerminiData.Data(dt, ((Room)sala.SelectedItem).id);
            dt = new DateTime(god.SelectedIndex + 2020, mes.SelectedIndex + 1, 1);
            Calendar.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);

        }

        private void PackIconMaterial_MouseEnter(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Visible;
        }

        private void PackIconMaterial_MouseLeave(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Hidden;
        }

        private void popuniListeTermina()
        {
            List<string> termini = new List<string>();
            termini.Add("00:00");
            termini.Add("00:30");
            termini.Add("01:00");
            termini.Add("01:30");
            termini.Add("02:00");
            termini.Add("02:30");
            termini.Add("03:00");
            termini.Add("03:30");
            termini.Add("04:00");
            termini.Add("04:30");
            termini.Add("05:00");
            termini.Add("05:30");
            termini.Add("06:00");
            termini.Add("06:30");
            termini.Add("07:00");
            termini.Add("07:30");
            termini.Add("08:00");
            termini.Add("08:30");
            termini.Add("09:00");
            termini.Add("09:30");
            termini.Add("10:00");
            termini.Add("10:30");
            termini.Add("11:00");
            termini.Add("11:30");
            termini.Add("12:00");
            termini.Add("12:30");
            termini.Add("13:00");
            termini.Add("13:30");
            termini.Add("14:00");
            termini.Add("14:30");
            termini.Add("15:00");
            termini.Add("15:30");
            termini.Add("16:00");
            termini.Add("16:30");
            termini.Add("17:00");
            termini.Add("17:30");
            termini.Add("18:00");
            termini.Add("18:30");
            termini.Add("19:00");
            termini.Add("19:30");
            termini.Add("20:00");
            termini.Add("20:30");
            termini.Add("21:00");
            termini.Add("21:30");
            termini.Add("22:00");
            termini.Add("22:30");
            termini.Add("23:00");
            termini.Add("23:30");
            termini.Add("00:00");

            satiOd.ItemsSource = termini;
            satiDo.ItemsSource = termini;
        }

        private void Sala_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Calendar.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
            salaZ.SelectedItem = sala.SelectedItem;
        }
    }
}
