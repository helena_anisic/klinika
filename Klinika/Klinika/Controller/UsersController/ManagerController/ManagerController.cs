// File:    ManagerController.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:48:58 PM
// Purpose: Definition of Class ManagerController

using Klinika.Controller.UsersController.ManagerController;
using Klinika.Service.UsersServices.ManagerServices;
using Model.users.user;
using Service.UsersServices.ManagerServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.ManagerController
{
    public class ManagerController : IManagerController
    {
        private IManagerService _managerService;

        public ManagerController(IManagerService managerService)
        {
            _managerService = managerService;
        }
        public Boolean CreateManager(Manager manager)
        {
            if (manager == null)
            {
                return false;
            }
            return _managerService.CreateManager(manager);
        }

        public Boolean EditManager(Manager manager)
        {
            return _managerService.EditManager(manager);
        }
        public Boolean DeleteManager(Manager manager)
        {
            return _managerService.DeleteManager(manager);
        }

        public Manager GetManagerByAccount(Account account)
        {
            return _managerService.GetManagerByAccount(account);
        }

        public List<Manager> GetAllManagers()
        {
            return _managerService.GetAllManagers();
        }
    }
}