﻿using Controller.UsersController.DoctorController;
using Klinika.Data;
using Klinika.i18N;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Karton.xaml
    /// </summary>
    public partial class Karton : Page
    {
        public ResultOfMedicalExamination result;
        public Karton()
        {
            InitializeComponent();
            String id = App.patient.Id;
            PatientFile.ItemsSource = App.PatientFileController.GetPatientFile(id).ResultsOfMedicalExamination;

            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
        }

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Window win = new KartonDetaljno(result);
            win.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Window win = new Alergije();
            win.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window win = new HronicneBolesti();
            win.ShowDialog();
        }

        private void PatientFile_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            result = (e.OriginalSource as FrameworkElement).DataContext as ResultOfMedicalExamination;
        }
    }
}
