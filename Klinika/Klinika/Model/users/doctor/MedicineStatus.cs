// File:    MedicineStatus.cs
// Author:  Admin
// Created: Tuesday, April 21, 2020 12:46:39 PM
// Purpose: Definition of Enum MedicineStatus

using System;

namespace Model.users.doctor
{
   public enum MedicineStatus
   {
      approved,
      denied,
      inProces
   }
}