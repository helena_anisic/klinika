// File:    SurveyController.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 6:33:44 PM
// Purpose: Definition of Class SurveyController

using Klinika.Controller.BlogFeedbackController;
using Klinika.Service.BlogFeedbackServices;
using Model.users.patient;
using Service.BlogFeedbackServices;
using System;
using System.Collections.Generic;

namespace Controller.BlogFeedbackController
{
   public class SurveyController : ISurveyController
   {
        private ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }
        public Boolean CreateSurvey(Survey survey) => _surveyService.CreateSurvey(survey);

        public List<Survey> GetAllSurveys() => _surveyService.GetAllSurveys();

    }
}