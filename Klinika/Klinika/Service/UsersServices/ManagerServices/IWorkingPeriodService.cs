﻿using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.ManagerServices
{
    public interface IWorkingPeriodService
    {
        Boolean CreateWorkingPeriod(WorkingPeriod workingPeriod);
        Boolean DeleteWorkingPeriod(WorkingPeriod workingPeriod);
        List<WorkingPeriod> GetAllWorkingPeriods();
        List<WorkingPeriod> GetAllWorkingPeriodsForDay(DateTime date);
        List<WorkingPeriod> GetAllWorkingPeriods(DateTime startDate, DateTime endDate);
        List<WorkingPeriod> GetAllWorkingPeriods(DateTime startDate, DateTime endDate, String doctorId);
        List<WorkingPeriod> GetAllWorkingPeriodsForDoctor(String DoctorId);
        List<WorkingPeriod> GetAllWorkingPeriodsForDoctor(String DoctorId, DateTime date);

    }
}
