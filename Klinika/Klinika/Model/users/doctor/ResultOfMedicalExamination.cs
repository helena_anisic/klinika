// File:    ResultOfMedicalExamination.cs
// Author:  Admin
// Created: Monday, April 20, 2020 12:17:12 PM
// Purpose: Definition of Class ResultOfMedicalExamination

using System;
using System.Collections.Generic;

namespace Model.users.doctor
{
    public class ResultOfMedicalExamination
    {
        public String Id { get; set; }
        public String Symptoms { get; set; }
        public String Diagnosis { get; set; }
        public String Content { get; set; }
        public DateTime DateOfDiagnosis { get; set; }
        public String DoctorId { get; set; }

        public ResultOfMedicalExamination() { }


    }
}