// File:    Gender.cs
// Author:  nikol
// Created: Friday, April 17, 2020 6:48:41 PM
// Purpose: Definition of Enum Gender

using System;

namespace Model.users.user
{
   public enum Gender
   {
      male,
      female
   }
}