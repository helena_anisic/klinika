﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.DoctorController
{
    public interface ISpecializationController
    {
        Boolean CreateSpecialization(TypeOfSpecialization specialization);
        Boolean EditSpecialization(TypeOfSpecialization specialization);
        List<TypeOfSpecialization> GetAllSpecialization();
        Boolean DeleteSpecialization(TypeOfSpecialization specialization);

    }
}
