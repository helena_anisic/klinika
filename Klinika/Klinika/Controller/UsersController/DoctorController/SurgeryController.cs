// File:    SurgeryController.cs
// Author:  nikol
// Created: Tuesday, May 26, 2020 9:01:46 PM
// Purpose: Definition of Class SurgeryController

using Klinika.Controller.UsersController.DoctorController;
using Klinika.Service.UsersServices.DoctorServices;
using Model.users.patient;
using Service.UsersServices.DoctorServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.DoctorController
{
   public class SurgeryController : ISurgeryController
   {
        private ISurgeryService _surgeryService;

        public SurgeryController(ISurgeryService surgeryService)
        {
            _surgeryService = surgeryService;
        }
        public Boolean CreateSurgery(Model.users.patient.Surgery surgery)
      {
            if (surgery == null)
                return false;
            return _surgeryService.CreateSurgery(surgery);
        }
      
      public Boolean EditSurgery(Model.users.patient.Surgery surgery)
      {
            return _surgeryService.EditSurgery(surgery);
        }
      
      public Boolean DeleteSurgery(Model.users.patient.Surgery surgery)
      {
            return _surgeryService.DeleteSurgery(surgery);
      }

        public List<Surgery> GetAllSurgeries()
        {
            return _surgeryService.GetAllSugeries();
        }

        public List<Surgery> GetAllSurgeriesForDoctor(String doctorId)
        {
            if (doctorId == null || doctorId.Equals(""))
                return null;
            return _surgeryService.GetAllSurgeriesForDoctor(doctorId);
        }

        public List<Surgery> GetAllSurgeriesForPatient(String patientId)
        {
            if (patientId == null || patientId.Equals(""))
                return null;
            return _surgeryService.GetAllSurgeriesForPatient(patientId);
        }
        public List<Surgery> GetAllSurgeriesForPatient(String patientId, DateTime date)
        {
            if (patientId == null || patientId.Equals("") || date == null)
                return null;
            return _surgeryService.GetAllSurgeriesForPatient(patientId, date);
        }

        
   
   }
}