﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.UserController;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for DodajLekara.xaml
    /// </summary>
    public partial class DodajLekara : Page
    { 
        public DodajLekara()
        {
            InitializeComponent();
            country.ItemsSource = App.LocationController.GetAllCountries();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new SpisakLekara());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            bool valid = true;
            if (name.Text.Equals(""))
            {
                nameBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                nameBorder.BorderBrush = Brushes.Silver;
            }
            if (phone.Text.Equals(""))
            {
                phoneBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                phoneBorder.BorderBrush = Brushes.Silver;
            }
            if (email.Text.Equals(""))
            {
                emailBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                emailBorder.BorderBrush = Brushes.Silver;
            }
            if (valid)
            {
                Model.users.user.Doctor doctor = new Model.users.user.Doctor();

                doctor.Id = id.Text;
                doctor.Name = name.Text;
                doctor.Surname = surname.Text;
                doctor.DateOfBirth = dateOfBirth.SelectedDate.Value;
                doctor.Address = adress.Text;
                Country c = (Country)country.SelectedItem;
                if (c != null)
                {
                    doctor.Country = c;
                    City ci = (City)city.SelectedItem;
                    if (ci != null)
                    {
                        doctor.City = ci;
                    }
                }
                doctor.PhoneNumber = phone.Text;
                doctor.Email = email.Text;

                App.DoctorController.CreateDoctor(doctor);

                logErr.Visibility = Visibility.Hidden;
                HomePage hp = (HomePage)Application.Current.MainWindow.Content;
                hp.setContentBox(new SpisakLekara());
            }
            else
            {
                logErr.Visibility = Visibility.Visible;
            }

        }

        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Phone_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Email_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            city.Items.Clear();
            city.ItemsSource = App.LocationController.GetAllCitiesByCountryName(((Country)country.SelectedItem).Name);
        }
    }
}
