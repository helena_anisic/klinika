// File:    GuestController.cs
// Author:  nikol
// Created: Tuesday, May 26, 2020 9:27:59 PM
// Purpose: Definition of Class GuestController

using Model.users.user;
using System;
using Service.UsersServices.SecretaryServices;
using System.Collections.Generic;
using Klinika.Service.UsersServices.SecretaryServices;
using Klinika.Controller.UsersController.SecretaryController;

namespace Controller.UsersController.SecretaryController
{
   public class GuestController : IGuestController
   {
        private IGuestService _guestService;
        public GuestController(IGuestService guestService)
        {
            _guestService = guestService;
        }
        public Boolean CreateGuestAccount(GuestAccount guestAccount)
      {
            if (guestAccount == null)
            {
                return false;
            }
            return _guestService.CreateGuestAccount(guestAccount);
        }
      
        public List<GuestAccount> GetAllGuests() => _guestService.GetAllGuestss();



    }
}