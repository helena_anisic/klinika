﻿using Controller.HospitalController;
using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Controller.UsersController.PatientController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.users.manager;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for NapraviRaspored.xaml
    /// </summary>
    public partial class NapraviRaspored : Page
    {
        DateTime currDate = DateTime.Now;
        public NapraviRaspored()
        {
            String date = DateTime.Now.ToString("dd.MM.yyyy");
            InitializeComponent();
            MedOprema1.ItemsSource = RadnoVremeLekara.Data(DateTime.Now);
            DataGridHeader.Header = date;

            MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, currDate, currDate);
            datumOd.SelectedDate = DateTime.Now;
            datumDo.SelectedDate = DateTime.Now;
            svi.IsChecked = true;
            lekari.ItemsSource = App.DoctorController.GetAllDoctors();
            lekari1.ItemsSource = lekari.ItemsSource;
            sale.ItemsSource = App.RoomController.GetAllRooms();
            popuniListeTermina();
        }

        private void DatumOd_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datumDo.SelectedDate == null)
                return;
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(datumOd.SelectedDate.Value, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(datumOd.SelectedDate.Value, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void DatumDo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void MedOprema_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count <= 0)
                return;
            int kolona = MedOprema.SelectedCells[0].Column.DisplayIndex;
            int red = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            int index = 7 * red + kolona; ;
            DateTime date = new DateTime(currDate.Year, currDate.Month, 1);
            int firstIndexOfCurrentMonth = (int)date.DayOfWeek;
            if (firstIndexOfCurrentMonth == 0)
                firstIndexOfCurrentMonth = 7;
            int lastIndexOfCurrentMonth = firstIndexOfCurrentMonth + DateTime.DaysInMonth(currDate.Year, currDate.Month);
            if (index >= firstIndexOfCurrentMonth && index <= lastIndexOfCurrentMonth)
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            else
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            startDate.SelectedDate = date;
            endDate.SelectedDate = date;
            MedOprema1.ItemsSource = RadnoVremeLekara.Data(date);
            String dateString = date.ToString("dd.MM.yyyy");
            DataGridHeader.Header = dateString;
            MedOprema.UnselectAllCells();
            if (svi.IsChecked == true)
            {
                MedOprema.Visibility = Visibility.Hidden;
                datumOd.Visibility = Visibility.Hidden;
                datumDo.Visibility = Visibility.Hidden;
                odOkvir.Visibility = Visibility.Hidden;
                doOkvir.Visibility = Visibility.Hidden;
                GodinaLabel.Visibility = Visibility.Hidden;
                MesecLabel.Visibility = Visibility.Hidden;
                svi.Visibility = Visibility.Hidden;
                pojedinacno.Visibility = Visibility.Hidden;
                lekari.Visibility = Visibility.Hidden;
                pButton.Visibility = Visibility.Hidden;
                sButton.Visibility = Visibility.Hidden;
                MedOprema1.Visibility = Visibility.Visible;
                Nazad.Visibility = Visibility.Visible;
            }
        }

        private void prethodnaClick(object sender, RoutedEventArgs e)
        {
            currDate = currDate.AddMonths(-1);
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void sledecaClick(object sender, RoutedEventArgs e)
        {
            currDate = currDate.AddMonths(1);
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void Nazad_Click(object sender, RoutedEventArgs e)
        {
            MedOprema.Visibility = Visibility.Visible;
            datumOd.Visibility = Visibility.Visible;
            datumDo.Visibility = Visibility.Visible;
            odOkvir.Visibility = Visibility.Visible;
            doOkvir.Visibility = Visibility.Visible;
            GodinaLabel.Visibility = Visibility.Visible;
            MesecLabel.Visibility = Visibility.Visible;
            svi.Visibility = Visibility.Visible;
            pojedinacno.Visibility = Visibility.Visible;
            lekari.Visibility = Visibility.Visible;
            pButton.Visibility = Visibility.Visible;
            sButton.Visibility = Visibility.Visible;
            MedOprema1.Visibility = Visibility.Hidden;
            Nazad.Visibility = Visibility.Collapsed;
        }

        private void Svi_Checked(object sender, RoutedEventArgs e)
        {
            MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            lekari.SelectedIndex = -1;
            lekari.IsEnabled = false;
        }

        private void Pojedinacno_Checked(object sender, RoutedEventArgs e)
        {
            lekari.IsEnabled = true;
            lekari.SelectedIndex = 0;
            Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
            if (doctor == null)
                return;
            MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
        }

        private void PackIconMaterial_MouseEnter(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Visible;
        }

        private void PackIconMaterial_MouseLeave(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Hidden;
        }

        private void Lekari_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
            if (doctor == null)
                return;
            lekari1.SelectedItem = doctor;
            MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime sDate = startDate.SelectedDate.Value;
            DateTime eDate = endDate.SelectedDate.Value;

            String[] sTime = ((String)satiOd.SelectedItem).Split(':');
            String[] eTime = ((String)satiDo.SelectedItem).Split(':');

            int sH = Int32.Parse(sTime[0]);
            int sM = Int32.Parse(sTime[1]);
            int eH = Int32.Parse(eTime[0]);
            int eM = Int32.Parse(eTime[1]);

            sDate = sDate.AddHours(sH);
            sDate = sDate.AddMinutes(sM);
            eDate = eDate.AddHours(eH);
            eDate = eDate.AddMinutes(eM);

            Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari1.SelectedItem;
            Room room = (Room)sale.SelectedItem;
            //List<Renovation> rcl = rc.GetAllRenovationsForRoom(room.id, sDate, eDate);
            //List<Surgery> scl = sc.GetAllSurgeriesForRoom(room.id, sDate, eDate);
            //List<ExaminationAppointment> exl = exc.GetAllAppointmentsForRoom(room.id, sDate, eDate);


            WorkingPeriod workingPeriod = new WorkingPeriod() { DoctorId = doctor.Id, RoomId = room.id, Shift = new Shift() { StartDateTime = sDate, EndDateTime = eDate } };
            App.WorkingPeriodController.CreateWorkingPeriod(workingPeriod);

            if (MedOprema1.Visibility == Visibility.Visible)
            {
                DateTime dt = DateTime.ParseExact(DataGridHeader.Header.ToString(), "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                MedOprema1.ItemsSource = RadnoVremeLekara.Data(dt);
            }
            else if (pojedinacno.IsChecked == true)
            {
                Model.users.user.Doctor dc = (Model.users.user.Doctor)lekari.SelectedItem;
                if (dc == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, dc);

            }
        }

        private void popuniListeTermina()
        {
            List<string> termini = new List<string>();
            termini.Add("00:00");
            termini.Add("00:30");
            termini.Add("01:00");
            termini.Add("01:30");
            termini.Add("02:00");
            termini.Add("02:30");
            termini.Add("03:00");
            termini.Add("03:30");
            termini.Add("04:00");
            termini.Add("04:30");
            termini.Add("05:00");
            termini.Add("05:30");
            termini.Add("06:00");
            termini.Add("06:30");
            termini.Add("07:00");
            termini.Add("07:30");
            termini.Add("08:00");
            termini.Add("08:30");
            termini.Add("09:00");
            termini.Add("09:30");
            termini.Add("10:00");
            termini.Add("10:30");
            termini.Add("11:00");
            termini.Add("11:30");
            termini.Add("12:00");
            termini.Add("12:30");
            termini.Add("13:00");
            termini.Add("13:30");
            termini.Add("14:00");
            termini.Add("14:30");
            termini.Add("15:00");
            termini.Add("15:30");
            termini.Add("16:00");
            termini.Add("16:30");
            termini.Add("17:00");
            termini.Add("17:30");
            termini.Add("18:00");
            termini.Add("18:30");
            termini.Add("19:00");
            termini.Add("19:30");
            termini.Add("20:00");
            termini.Add("20:30");
            termini.Add("21:00");
            termini.Add("21:30");
            termini.Add("22:00");
            termini.Add("22:30");
            termini.Add("23:00");
            termini.Add("23:30");

            satiOd.ItemsSource = termini;
            satiDo.ItemsSource = termini;
        }
    }
}
