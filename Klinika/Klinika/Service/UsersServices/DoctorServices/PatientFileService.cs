// File:    PatientFileService.cs
// Author:  Admin
// Created: Saturday, May 30, 2020 2:23:25 AM
// Purpose: Definition of Class PatientFileService

using Klinika.Repository.UsersRepositories.DoctorRepositories;
using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using Model.users.user;
using Repository.UsersRepositories.DoctorRepositories;
using Syncfusion.Windows.Controls.Printing;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.DoctorServices
{

    public class PatientFileService : IPatientFileService
    {
        private PatientFileRepository _patientFileRepository;
        private ResultOfExaminationRepository _resultOfExaminationRepository;
        private TherapyRepository _therapyRepository;
        private MedicalCareRepository _medicalCareRepository;

        public PatientFileService(PatientFileRepository patientFileRepository)
        {
            _patientFileRepository = patientFileRepository;
        }


        public Boolean CreatePatientFile(PatientFile patientFile)
        {
            List<PatientFile> patientFiles = _patientFileRepository.GetAll(PatientFileRepository.path);
            if (patientFiles == null)
            {
                patientFiles = new List<PatientFile>();
                patientFiles.Add(patientFile);
                _patientFileRepository.Save(patientFiles, PatientFileRepository.path);
                return true;
            }

            foreach (PatientFile pf in patientFiles)
            {
                if (pf.Id.Equals(patientFile.Id))
                {
                    return false;
                }
            }

            patientFiles.Add(patientFile);
            _patientFileRepository.Save(patientFiles, PatientFileRepository.path);
            return true;
        }

        public Boolean EditPatientFile(PatientFile patientFile)
        {
            if (DeletePatientFile(patientFile) == false)
                return false;
            return CreatePatientFile(patientFile);
        }

        public Boolean DeletePatientFile(PatientFile patientFile)
        {
            List<PatientFile> patientFiles = _patientFileRepository.GetAll(PatientFileRepository.path);

            if (patientFiles == null)
                return false;

            foreach (PatientFile pf in patientFiles)
            {
                if (pf.Id.Equals(patientFile.Id))
                {
                    patientFiles.Remove(pf);
                    break;
                }
            }

            return _patientFileRepository.Save(patientFiles, PatientFileRepository.path);
        }

        public List<PatientFile> GetAllPatientFiles()
        {
            return _patientFileRepository.GetAll(PatientFileRepository.path);
        }

        public PatientFile GetPatientFile(String id)
        {
            List<PatientFile> patientFiles = _patientFileRepository.GetAll(PatientFileRepository.path);
            foreach (PatientFile pf in patientFiles)
            {
                if (pf.Id.Equals(id))
                {
                    return pf;
                }
            }

            return null;
        }
        public Boolean CreateResultOfExamination(Model.users.doctor.ResultOfMedicalExamination resultOfExamination)
        {
            List<ResultOfMedicalExamination> resultOfMedicalExaminations = _resultOfExaminationRepository.GetAll(ResultOfExaminationRepository.path);
            if (resultOfMedicalExaminations == null)
            {
                resultOfMedicalExaminations = new List<ResultOfMedicalExamination>();
                resultOfMedicalExaminations.Add(resultOfExamination);
                _resultOfExaminationRepository.Save(resultOfMedicalExaminations, ResultOfExaminationRepository.path);
                return true;
            }

            foreach (ResultOfMedicalExamination res in resultOfMedicalExaminations)
            {
                if (res.Id.Equals(resultOfExamination.Id))
                {
                    return false;
                }
            }

            resultOfMedicalExaminations.Add(resultOfExamination);
            _resultOfExaminationRepository.Save(resultOfMedicalExaminations, ResultOfExaminationRepository.path);
            return true;
        }

        public Boolean CreateTherapy(Model.users.doctor.Therapy therapy)
        {
            List<Therapy> therapies = _therapyRepository.GetAll(TherapyRepository.path);
            if (therapies == null)
            {
                therapies = new List<Therapy>();
                therapies.Add(therapy);
                _therapyRepository.Save(therapies, TherapyRepository.path);
                return true;
            }

            foreach (Therapy t in therapies)
            {
                if (t.Id.Equals(therapy.Id))
                {
                    return false;
                }
            }

            therapies.Add(therapy);
            _therapyRepository.Save(therapies, TherapyRepository.path);
            return true;
        }

        public Boolean EditTherapy(Model.users.doctor.Therapy therapy)
        {
            if (DeleteTherapy(therapy) == false)
                return false;
            return CreateTherapy(therapy);
        }

        public Boolean DeleteTherapy(Model.users.doctor.Therapy therapy)
        {
            List<Therapy> therapies = _therapyRepository.GetAll(TherapyRepository.path);

            if (therapies == null)
                return false;

            foreach (Therapy t in therapies)
            {
                if (t.Id.Equals(therapy.Id))
                {
                    therapies.Remove(t);
                    break;
                }
            }

            return _therapyRepository.Save(therapies, TherapyRepository.path);
        }

        public Boolean CreateMedicalCares(Model.users.doctor.MedicalCare medicalCare)
        {
            List<MedicalCare> medicalCares = _medicalCareRepository.GetAll(MedicalCareRepository.path);
            if (medicalCare == null)
            {
                medicalCares = new List<MedicalCare>();
                medicalCares.Add(medicalCare);
               _medicalCareRepository.Save(medicalCares, MedicalCareRepository.path);
                return true;
            }

            foreach (MedicalCare mC in medicalCares)
            {
                if (mC.Id.Equals(medicalCare.Id))
                {
                    return false;
                }
            }
            medicalCares.Add(medicalCare);
            _medicalCareRepository.Save(medicalCares, MedicalCareRepository.path);
            return true;
        }

        public Boolean EditMedicalCare(Model.users.doctor.MedicalCare medicalCare)
        {
            if (DeleteMedicalCare(medicalCare) == false)
                return false;
            return CreateMedicalCares(medicalCare);
        }

        public Boolean DeleteMedicalCare(Model.users.doctor.MedicalCare medicalCare)
        {
            List<MedicalCare> medicalCares = _medicalCareRepository.GetAll(MedicalCareRepository.path);

            if (medicalCares == null)
                return false;

            foreach (MedicalCare m in medicalCares)
            {
                if (m.Id.Equals(medicalCare.Id))
                {
                    medicalCares.Remove(m);
                    break;
                }
            }

            return _medicalCareRepository.Save(medicalCares, MedicalCareRepository.path);
        }

        public List<MedicalCare> GetAllMedicalCares()
        {
            return _medicalCareRepository.GetAll(MedicalCareRepository.path);
        }

        public List<ResultOfMedicalExamination> GetAllResultOfMedicalExamination()
        {
            return _resultOfExaminationRepository.GetAll(ResultOfExaminationRepository.path);
        }
    }
}