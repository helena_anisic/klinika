// File:    Account.cs
// Author:  nikol
// Created: Friday, April 17, 2020 9:37:48 PM
// Purpose: Definition of Class Account

using Newtonsoft.Json;
using System;

namespace Model.users.user
{
    public class Account
    {
        public String Username { get; set; }
        public String Password { get; set; }
        public Language Language { get; set; }
        public String ProfileImageSource { get; set; }

        public Account()
        {

        }

        public Account(string username, string password)
        {
            this.Username = username;
            this.Password = password;
            //language
        }
    }
}