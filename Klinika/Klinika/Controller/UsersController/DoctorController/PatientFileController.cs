// File:    PatientFileController.cs
// Author:  nikol
// Created: Saturday, May 30, 2020 2:23:25 AM
// Purpose: Definition of Class PatientFileController

using Klinika.Controller.UsersController.DoctorController;
using Klinika.Controller.UsersController.PatientController;
using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using Model.users.user;
using Service.UsersServices.DoctorServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.DoctorController
{
   public class PatientFileController : IPatietnFileController
   {
        private IPatientFileService _patientFileService;

        public PatientFileController(IPatientFileService patientFileService)
        {
            _patientFileService = patientFileService;
        }

        public Boolean CreatePatientFile(PatientFile patientFile)
        {
            if (patientFile == null)
            {
                return false;
            }
            return _patientFileService.CreatePatientFile(patientFile);
        }

        public Boolean EditPatientFile(PatientFile patientFile)
        {
            return _patientFileService.EditPatientFile(patientFile);
        }


        public List<PatientFile> GetAllPatientFiles()
        {
            return _patientFileService.GetAllPatientFiles();
        }

        public PatientFile GetPatientFile(String id)
        {
            return _patientFileService.GetPatientFile(id);
        }


      public Boolean CreateResultOfExamination(Model.users.doctor.ResultOfMedicalExamination resultOfExamination)
      {
            if (resultOfExamination == null)
            {
                return false;
            }
            return _patientFileService.CreateResultOfExamination(resultOfExamination);
        }
      
      public Boolean CreateTherapy(Model.users.doctor.Therapy therapy)
      {
            if (therapy == null)
            {
                return false;
            }
            return _patientFileService.CreateTherapy(therapy);
        }
      
      public Boolean EditTherapy(Model.users.doctor.Therapy therapy)
      {
            return _patientFileService.EditTherapy(therapy);
        }
      
      public Boolean CreateMedicalCare(Model.users.doctor.MedicalCare medicalCare)
      {
            if (medicalCare == null)
            {
                return false;
            }
            return _patientFileService.CreateMedicalCares(medicalCare);
        }
      
      public Boolean EditMedicalCare(Model.users.doctor.MedicalCare medicalCare)
      {
            return _patientFileService.EditMedicalCare(medicalCare);
      }
      
      public Boolean DeleteMedicalCare(Model.users.doctor.MedicalCare medicalCare)
      {
            return _patientFileService.DeleteMedicalCare(medicalCare);
        }

        public List<MedicalCare> GetAllMedicalCares()
        {
            return _patientFileService.GetAllMedicalCares();
        }

    }
}