﻿using Model.users.doctor;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.DoctorController
{
    public interface IDoctorController
    {
        Boolean CreateDoctor(Doctor doctor);
        Boolean EditDoctor(Doctor doctor);
        List<Doctor> GetAllDoctors();
        Doctor GetDoctorById(String id);
        Boolean CreateAllergy(Allergy allergy);
        Boolean EditAllergy(Allergy allergy);
        Boolean DeleteAllergy(Allergy allergy);
        Boolean DeleteDoctor(Doctor doctor);
        List<TypeOfSpecialization> GetAllSpecialization();

    }
}
