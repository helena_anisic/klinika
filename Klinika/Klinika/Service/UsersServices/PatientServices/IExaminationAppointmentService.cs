﻿using Model.users.patient;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.PatientServices
{
    public interface IExaminationAppointmentService
    {
        Boolean CreateExaminationAppointment(ExaminationAppointment examinationAppointment);
        Boolean DeleteExaminationAppointment(Model.users.patient.ExaminationAppointment examinationAppointment);
        Boolean EditExaminationAppointment(ExaminationAppointment examinationAppointment);
        List<ExaminationAppointment> GetAllAppointments();
        List<ExaminationAppointment> GetAllAppointmentsForDoctor(String doctorId);
        List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId);
        List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId, DateTime date);
        List<ExaminationAppointment> GetAllAppointmentsForDay(DateTime date);
        Doctor GetFreeDoctorForTerm(DateTime startDateTime, DateTime endDateTime);
        Boolean isTermFree(String doctorId, DateTime startDateTime);
        List<ExaminationAppointment> GetAllAppointmentsForPeriod(DateTime startDate, DateTime endDate);
        List<ExaminationAppointment> GetAllAppointmentsForPeriod(DateTime startDate, DateTime endDate, String doctorId);
        ExaminationAppointment GetFreeAppointment(DateTime startDateTime, DateTime endDateTime);
        ExaminationAppointment GetFreeAppointment(DateTime startDateTime, DateTime endDateTime, String doctorId);

    }
}
