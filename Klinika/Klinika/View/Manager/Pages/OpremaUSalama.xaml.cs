﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for OpremaUSalama.xaml
    /// </summary>
    public partial class OpremaUSalama : Page
    {
        public OpremaUSalama()
        {
            InitializeComponent();
            fillForm();
        }

        private void Button_Click_Izmeni_Opremu(object sender, RoutedEventArgs e)
        {
            /*
            MedicinskaOprema item = (MedicinskaOprema)MedOprema.SelectedItem;
            Window w;
            if (item == null)
            {
                w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }*/
            //w = new IzmeniOpremu();
            //w.ShowDialog();
        }

        private void Button_Click_Obrisi_Opremu(object sender, RoutedEventArgs e)
        {
            Room room = (Room)Sale.SelectedItem;
            Equipment equipment = (Equipment)MedOprema.SelectedItem;
            MessageBoxYesNo mb = new MessageBoxYesNo("Ukloni opremu", "Da li želite da uklonite opremu\n " + equipment.name + "id " + equipment.id + "?", "Odustani", "Ukloni");
            mb.ShowDialog();
            if (mb.GetCloseStatus() == 1)
            {
                foreach (Equipment ee in room.equipment)
                {
                    if (ee.id.Equals(equipment.id))
                    {
                        room.equipment.Remove(ee);
                        break;
                    }
                }
                App.RoomController.EditRoom(room);
                MedOprema.DataContext = new OpremaView(room);
            }

        }

        private void fillForm()
        {
            Sale.ItemsSource = App.RoomController.GetAllRooms();
            Sale.SelectedIndex = 0;
            MedOprema.DataContext = new OpremaView((Room)Sale.SelectedItem);
            MedOprema.HeadersVisibility = DataGridHeadersVisibility.Column;
        }
        private void Sale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema.DataContext = new OpremaView((Room)Sale.SelectedItem);
        }
    }
}
