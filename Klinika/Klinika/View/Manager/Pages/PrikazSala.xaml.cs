﻿using Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for PrikazSala.xaml
    /// </summary>
    public partial class PrikazSala : Page
    {
        public PrikazSala()
        {
            InitializeComponent();
            MedOprema.ItemsSource = App.RoomController.GetAllRooms();
        }

        private void Button_Click_Dodaj_Opremu(object sender, RoutedEventArgs e)
        {
            Window w = new DodajSalu();
            w.ShowDialog();
            MedOprema.ItemsSource = App.RoomController.GetAllRooms();
        }

        private void Button_Click_Izmeni_Opremu(object sender, RoutedEventArgs e)
        {
            Room room = (Room)MedOprema.SelectedItem;
            if (room == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti salu u tabeli koju\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }
            Window ww = new IzmeniSalu(room);
            ww.ShowDialog();
            MedOprema.ItemsSource = App.RoomController.GetAllRooms();
        }

        private void Button_Click_Obrisi_Opremu(object sender, RoutedEventArgs e)
        {
            Room room = (Room)MedOprema.SelectedItem;
            if (room == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti salu u tabeli koju\nzelite da obrisete!", "Ok");
                w.ShowDialog();
            }
            else
            {
                MessageBoxYesNo mb = new MessageBoxYesNo("Obriši opremu", "Da li želite da obrišete salu\n" + room.id + "?", "Odustani", "Obriši");
                mb.ShowDialog();
                if (mb.GetCloseStatus() == 1)
                {
                    App.RoomController.DeleteRoom(room);
                    MedOprema.ItemsSource = App.RoomController.GetAllRooms();
                }
            }
        }
    }
}
