﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.SecretaryController
{
    public interface ISecretaryController
    {
        Boolean EditSecretary(Secretary secretary);
        Secretary GetSecretaryByAccount(Account account);
        Boolean CreateSecretary(Secretary secretary);

    }
}
