// File:    Doctor.cs
// Author:  Admin
// Created: Monday, April 20, 2020 1:54:21 PM
// Purpose: Definition of Class Doctor

using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace Model.users.user
{
    public class Doctor : User
    {
        public TypeOfSpecialization TypeOfSpecialization { get; set; }
        public List<int> Rating = new List<int>();

        public Doctor() { }

        public Doctor(String username, String password, String name, String surname, DateTime dateOfBirth, String address, String phoneNumber, String email, Gender gender, City city, Country country, TypeOfSpecialization specialization,List<int> rating)
        {
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            Address = address;
            PhoneNumber = phoneNumber;
            Email = email;
            Gender = gender;
            City = city;
            Country = country;
            TypeOfSpecialization = specialization;
            Rating = rating;
        }
        public override string ToString()
        {
            return "dr " + Name + " " + Surname;
        }
    }
}