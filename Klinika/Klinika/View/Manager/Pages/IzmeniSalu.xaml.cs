﻿using Controller.HospitalController;
using Model.Hospital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for IzmeniSalu.xaml
    /// </summary>
    public partial class IzmeniSalu : Window
    {
        public Room room;
        public IzmeniSalu(Room room)
        {
            this.room = room;
            InitializeComponent();
            fillForm();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            Room room = new Room();
            room.id = id.Text;
            room.floor = int.Parse(floor.Text);
            if (roomType.SelectedIndex >= 0)
                room.roomType = (RoomType)roomType.SelectedItem;
            if (inUse.IsChecked == true)
                room.inUse = true;
            else
                room.inUse = false;
            App.RoomController.EditRoom(room);
            this.Close();
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void fillForm()
        {
            id.Text = room.id;
            floor.Text = room.floor.ToString();
            roomType.ItemsSource = App.RoomController.GetAllRoomTypes();
            int i = 0;
            foreach (RoomType r in roomType.Items)
            {
                if (r.type.Equals(room.roomType.type))
                {
                    roomType.SelectedIndex = i;
                    return;
                }
                i++;
            }
            if (room.inUse == true)
                inUse.IsChecked = true;
            else
                notInUse.IsChecked = true;
        }
    }
}
