// File:    SurveyService.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 6:33:44 PM
// Purpose: Definition of Class SurveyService

using Klinika.Service.BlogFeedbackServices;
using Model.users.patient;
using Repository.BlogFeedbackRepositories;
using System;
using System.Collections.Generic;

namespace Service.BlogFeedbackServices
{
   public class SurveyService : ISurveyService
   {
        private SurveyRepository _surveyRepository;

        public SurveyService(SurveyRepository surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }
        public Boolean CreateSurvey(Survey survey)
        {
            List<Survey> surveys = _surveyRepository.GetAll(SurveyRepository.path);
            if(surveys == null)
            {
                surveys = new List<Survey>();
            }
            surveys.Add(survey);
            return _surveyRepository.Save(surveys, SurveyRepository.path);
        }

        public List<Survey> GetAllSurveys() => _surveyRepository.GetAll(SurveyRepository.path);

    }
}