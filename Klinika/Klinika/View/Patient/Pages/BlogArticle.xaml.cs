﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for BlogArticle.xaml
    /// </summary>
    public partial class BlogArticle : Page
    {
        Model.users.doctor.BlogArticle blogArticle;
        public BlogArticle()
        {
            InitializeComponent();
        }

        public BlogArticle(Model.users.doctor.BlogArticle blogArticle)
        {
            InitializeComponent();
            this.blogArticle = blogArticle;
            naslov.Text = blogArticle.Title;
            tekst.Text = blogArticle.Content; 
        }

        public DateTime PublishingDate { get; internal set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Menu meni = new Menu();
            meni.ContentBox.Content = new Blog();
            Application.Current.MainWindow.Content = meni;
        }
    }
}
