﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for EditSurgery.xaml
    /// </summary>
    public partial class EditSurgery : Window
    { 
        public static Surgery ExistingSurgery { get; set; }
        public DateTime Date { get; set; }
        public EditSurgery(Surgery surgery)
        {
            ExistingSurgery = surgery;
            InitializeComponent();
            surgeryId.Text = ExistingSurgery.Id;
            typeOfSurgery.Text = ExistingSurgery.Cause;
            PatientsComboBox.ItemsSource = App.PatientController.GetAllPatients();
            PatientsComboBox.SelectedIndex = getIndexOfPatient();
            DoctorsComboBox.ItemsSource = App.DoctorController.GetAllDoctors();
            DoctorsComboBox.SelectedIndex = getIndexOfDoctor();
            date.SelectedDate = ExistingSurgery.StartDateTime;
            startTime.Value = ExistingSurgery.StartDateTime;
            endTime.Value = ExistingSurgery.EndDateTime;
            roomId.Text = ExistingSurgery.RoomId;
        }

        private int getIndexOfPatient()
        {
            for (int i = 0; i < App.PatientController.GetAllPatients().Count; i++)
            {
                if (App.PatientController.GetAllPatients().ElementAt(i).Id.Equals(ExistingSurgery.PatientId))
                {
                    return i;
                }
            }
            return -1;
        }
        private int getIndexOfDoctor()
        {
            for (int i = 0; i < App.DoctorController.GetAllDoctors().Count; i++)
            {
                if (App.DoctorController.GetAllDoctors().ElementAt(i).Id.Equals(ExistingSurgery.DoctorId))
                {
                    return i;
                }
            }
            return -1;
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Surgery updateSurgery = new Surgery();
            updateSurgery.Id = ExistingSurgery.Id;
            updateSurgery.Cause = typeOfSurgery.Text;
            updateSurgery.Urgency = ExistingSurgery.Urgency;
            updateSurgery.PatientId = ((Model.users.user.Patient)PatientsComboBox.SelectedItem).Id;
            updateSurgery.DoctorId = ((Model.users.user.Doctor)DoctorsComboBox.SelectedItem).Id;
            updateSurgery.RoomId = roomId.Text;

            Date = (DateTime)date.SelectedDate;
            updateSurgery.StartDateTime = new DateTime(Date.Year, Date.Month, Date.Day,
                ((DateTime)startTime.Value).Hour, ((DateTime)startTime.Value).Minute, 0);

            updateSurgery.EndDateTime = new DateTime(Date.Year, Date.Month, Date.Day,
                ((DateTime)endTime.Value).Hour, ((DateTime)endTime.Value).Minute, 0);

            for (int i = 0; i < App.SurgeryController.GetAllSurgeries().Count; i++)
            {
                if (App.SurgeryController.GetAllSurgeries().ElementAt(i).Id.Equals(updateSurgery.Id))
                {
                    App.SurgeryController.EditSurgery(updateSurgery);
                }
            }
            ExistingSurgery = updateSurgery;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
