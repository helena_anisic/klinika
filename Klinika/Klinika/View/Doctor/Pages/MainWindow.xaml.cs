﻿using Klinika.View.Doctor.Pages.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjY1NDUxQDMxMzgyZTMxMmUzME5sYTlydXZyakdMemRwaFNKc2djMkZqNEdoNXNBT29sTmxpTlUybGZ4ck09");
            InitializeComponent();
            DataContext = new HomePageViewModel();
        }

        private void ListViewItem_MouseEnter(object sender, MouseEventArgs e)
        {
            if (tgBtn.IsChecked == true)
            {
                toolTipHome.Visibility = Visibility.Collapsed;
                toolTipAccount.Visibility = Visibility.Collapsed;
                toolTipSchedule.Visibility = Visibility.Collapsed;
                toolTipPatients.Visibility = Visibility.Collapsed;
                toolTipMedicines.Visibility = Visibility.Collapsed;
                toolTipBlog.Visibility = Visibility.Collapsed;
                toolTipReport.Visibility = Visibility.Collapsed;
                toolTipHelp.Visibility = Visibility.Collapsed;
                toolTipLogOut.Visibility = Visibility.Collapsed;

            }
            else
            {
                toolTipHome.Visibility = Visibility.Visible;
                toolTipAccount.Visibility = Visibility.Visible;
                toolTipSchedule.Visibility = Visibility.Visible;
                toolTipPatients.Visibility = Visibility.Visible;
                toolTipMedicines.Visibility = Visibility.Visible;
                toolTipBlog.Visibility = Visibility.Visible;
                toolTipReport.Visibility = Visibility.Visible;
                toolTipHelp.Visibility = Visibility.Visible;
                toolTipLogOut.Visibility = Visibility.Visible;

            }
        }

        private void tgBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            contentControl.Opacity = 1;
        }

        private void tgBtn_Checked(object sender, RoutedEventArgs e)
        {
            contentControl.Opacity = 0.3;
        }

        private void BG_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            tgBtn.IsChecked = false;
        }

        private void HomePage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new HomePageViewModel();
        }

        private void Menu_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Menu_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            contentControl.Opacity = 1;
        }

        private void HomePage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void HomePage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new HomePageViewModel();
        }

        private void Schedule_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Schedule_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new ScheduleViewModel();
        }

        private void Medicine_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Medicine_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new MedicineViewModel();
        }

        private void Patients_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Patients_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new PatientsViewModel();
        }

        private void Report_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Report_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new ReportViewModel();
        }

        private void Blog_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Blog_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new BlogViewModel();
        }

        private void MyAccount_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void MyAccount_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataContext = new MyAccountViewModel();
        }

        private void MyAccount_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new MyAccountViewModel();
        }

        private void Help_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //DataContext = new HelpViewModel();
            System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=8FKEUyGn-qE&feature=youtu.be");
        }

        private void LogOut_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void LogOut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
//            Util.saveData();
            LogIn logIn = new LogIn();
            logIn.Show();
            this.Close();
        }

        private void Schedule_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new ScheduleViewModel();
        }

        private void Patients_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new PatientsViewModel();
        }

        private void Medicines_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new MedicineViewModel();
        }

        private void Blog_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new BlogViewModel();
        }

        private void Report_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataContext = new ReportViewModel();
        }

        private void Help_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=8FKEUyGn-qE&feature=youtu.be");
        }

        private void LogOut_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
//            Util.saveData();
            LogIn logIn = new LogIn();
            logIn.Show();
            this.Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
