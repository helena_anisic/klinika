// File:    MedicineController.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:31:57 PM
// Purpose: Definition of Class MedicineController

using Klinika.Controller.HospitalController;
using Klinika.Service.HospitalServices;
using Model.Storage;
using Service.HospitalServices;
using System;
using System.Collections.Generic;

namespace Controller.HospitalController
{
    public class MedicineController : IMedicineController
    {
        private IMedicineService _medicineService;

        public MedicineController(IMedicineService medicineService)
        {
            _medicineService = medicineService;
        }
        public Boolean CreateMedicine(Medicine medicine)
        {
            if (medicine == null)
                return false;
            return _medicineService.CreateMedicine(medicine);
        }

        public List<MedicineType> GetAllMedicinesTypes() => _medicineService.GetAllMedicinesTypes();

        public List<Medicine> GetAllMedicines() => _medicineService.GetAllMedicine();

        public Boolean EditMedicine(Medicine medicine)
        {
            if (medicine == null)
                return false;
            return _medicineService.EditMedicine(medicine);
        }

        public Boolean DeleteMedicine(Medicine medicine)
        {
            if (medicine == null)
                return false;
            return _medicineService.DeleteMedicine(medicine);
        }

    }
}