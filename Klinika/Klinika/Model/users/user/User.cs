// File:    User.cs
// Author:  nikol
// Created: Friday, April 17, 2020 6:30:00 PM
// Purpose: Definition of Class User

using System;

namespace Model.users.user
{
    public class User:Account
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public String Address { get; set; }
        public String PhoneNumber { get; set; }
        public String Email { get; set; }
        public Gender Gender { get; set; }
        public Doctor Doctor { get; set; }

        public City City { get; set; }
        public Country Country { get; set; }

        public User()
        {

        }

    }
}