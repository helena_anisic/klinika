﻿using Controller.UsersController.UserController;
using Klinika.i18N;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for register2.xaml
    /// </summary>
    public partial class register2 : Page
    {
        Patient patient = null;
        public register2(Patient patient)
        {
            this.patient = patient;
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            fillForm();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            patient.Address = adress.Text;
            if (country.SelectedValue != null)
            {
                patient.Country = App.LocationController.GetCountryByName(country.SelectedValue.ToString());
            }
            if (city.SelectedValue != null)
            {
                patient.City = App.LocationController.GetCityByName(city.SelectedValue.ToString());
            }
            patient.Email = email.Text;
            //provera da li je potvrdjena sifra
            patient.Username = email.Text;
            patient.Password = password.Password.ToString();
            Application.Current.MainWindow.Content = new register(patient);
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            patient.Address = adress.Text;
            if (country.SelectedValue != null)
            {
                patient.Country = App.LocationController.GetCountryByName(country.SelectedValue.ToString());
            }
            if (city.SelectedValue != null)
            {
                patient.City = App.LocationController.GetCityByName(city.SelectedValue.ToString());
            }
            patient.Email = email.Text;
            //provera da li je potvrdjena sifra
            patient.Username = email.Text;
            patient.Password = password.Password.ToString();
            if(adress.Text.Length != 0 && email.Text.Length != 0 && password.Password.Length>=8 && confirmPassword.Password == password.Password)
            {
                Application.Current.MainWindow.Content = new register3(patient);
            }
            else
            {
                if(adress.Text.Length == 0)
                {
                    adress.BorderBrush = Brushes.Red;
                    addressMessage.Visibility = Visibility.Visible;
                }
                if(email.Text.Length == 0)
                {
                    email.BorderBrush = Brushes.Red;
                    emailMessage.Visibility = Visibility.Visible;
                }
                if(password.Password.Length < 8)
                {
                    password.BorderBrush = Brushes.Red;
                    passwordMessage.Visibility = Visibility.Visible;
                }
                if(confirmPassword.Password != password.Password)
                {
                    confirmPassword.BorderBrush = Brushes.Red;
                    passwordConfirmMessage.Visibility = Visibility.Visible;
                }
            }
            
        }

        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<City> cities = App.LocationController.GetAllCitiesByCountryName(country.SelectedValue.ToString());
            city.Items.Clear();
            foreach (City c in cities)
            {
                city.Items.Add(c.Name);
            }

        }

        private void fillForm()
        {
            List<Country> countries = App.LocationController.GetAllCountries();
            foreach (Country c in countries)
            {
                country.Items.Add(c.Name);
            }


            if (patient.City != null)
                city.Text = patient.City.Name;
            if (patient.Country != null)
            {
                country.Text = patient.Country.Name;
            }
            adress.Text = patient.Address;
            email.Text = patient.Email;
            password.Password = patient.Password;
        }

        private void adress_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (adress.Text.Length == 0)
            {
                adress.BorderBrush = Brushes.Red;
                addressMessage.Visibility = Visibility.Visible;
            }
            else
            {
                adress.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                addressMessage.Visibility = Visibility.Hidden;
            }
        }

        private void email_TextChanged(object sender, TextChangedEventArgs e)
        {
                if (!email.Text.Contains("@"))
                {
                    email.BorderBrush = Brushes.Red;
                    emailMessage.Visibility = Visibility.Visible;
                }
                else
                {
                    email.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    emailMessage.Visibility = Visibility.Hidden;
                }
            
        }

        private void password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (password.Password.Length<8)
            {
                password.BorderBrush = Brushes.Red;
                passwordMessage.Visibility = Visibility.Visible;
            }
            else
            {
                password.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                passwordMessage.Visibility = Visibility.Hidden;
            }
        }

        private void confirmPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (password.Password != confirmPassword.Password)
            {
                confirmPassword.BorderBrush = Brushes.Red;
                passwordConfirmMessage.Visibility = Visibility.Visible;
            }
            else
            {
                confirmPassword.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                passwordConfirmMessage.Visibility = Visibility.Hidden;
            }
        }
    }
}
