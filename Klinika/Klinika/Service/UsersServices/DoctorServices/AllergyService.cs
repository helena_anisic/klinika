// File:    AllergyService.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:21:51 AM
// Purpose: Definition of Class AllergyService

using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using Repository.UsersRepositories.DoctorRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.DoctorServices
{
   public class AllergyService : IAllergyService
   {
      private AllergyRepository _allergyRepository;
        public AllergyService(AllergyRepository allergyRepository)
        {
            _allergyRepository = allergyRepository;
        }
      public Boolean CreateAllergy(Allergy allergy)
      {
            List<Allergy> allergies = _allergyRepository.GetAll(AllergyRepository.path);
            if (allergies == null)
            {
                allergies = new List<Allergy>();
            }
            allergies.Add(allergy);
            _allergyRepository.Save(allergies, AllergyRepository.path);
            return true;
        }
      
      public Boolean EditAllergy(Model.users.doctor.Allergy allergy)
      {
            if (DeleteAllergy(allergy) == false)
                return false;
            return CreateAllergy(allergy);
        }
      
      public List<Model.users.doctor.Allergy> GetAllAllergies()
      {
            return _allergyRepository.GetAll(AllergyRepository.path);
      }
      
      public Model.users.doctor.Allergy GetAllergy(String id)
      {
            List<Allergy> allergies = _allergyRepository.GetAll(AllergyRepository.path);
            if (allergies == null)
                return null;
            foreach (Allergy allergy in allergies)
            {
                if (allergy.Id == id)
                {
                    return allergy;
                }
            }
            return null;
        }
      
      public Boolean DeleteAllergy(Model.users.doctor.Allergy allergy)
      {
            List<Allergy> allergies = _allergyRepository.GetAll(AllergyRepository.path);

            if (allergies == null)
                return false;

            foreach (Allergy a in allergies)
            {
                if (a.Id.Equals(allergy.Id))
                {
                    allergies.Remove(a);
                    break;
                }
            }

            return _allergyRepository.Save(allergies, AllergyRepository.path);
        }


      
   
   }
}