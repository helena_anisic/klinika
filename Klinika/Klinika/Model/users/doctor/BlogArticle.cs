// File:    BlogArticle.cs
// Author:  Admin
// Created: Tuesday, April 21, 2020 5:22:51 PM
// Purpose: Definition of Class BlogArticle

using System;

namespace Model.users.doctor
{
    public class BlogArticle
    {
        public String Id { get; set; }
        public String Title { get; set; }
        public String Content { get; set; }
        public DateTime PublishingDate { get; set; }

        public BlogArticle()
        {

        }

        public BlogArticle(String Id, String Title, String Content, DateTime Publishing)
        {
            this.Id = Id;
            this.Title = Title;
            this.Content = Content;
            this.PublishingDate = Publishing;
        }

        public BlogArticle(String Id, String Title)
        {
            this.Id = Id;
            this.Title = Title;
        }
    }
}