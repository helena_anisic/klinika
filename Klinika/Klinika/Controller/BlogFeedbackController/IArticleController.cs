﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.BlogFeedbackController
{
    public interface IArticleController
    {
        Boolean CreateBlogArticle(BlogArticle blogArticle);
        BlogArticle GetBlogArticleById(String id);
        List<BlogArticle> GetAllBlogArticles();
    }
}
