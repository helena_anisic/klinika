// File:    SecretaryRepository.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:49:01 PM
// Purpose: Definition of Class SecretaryRepository

using Klinika.Repository;
using Model.users.user;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.UsersRepositories.SecretaryRepositories
{
    public class SecretaryRepository : Repository<Secretary>
    {
        public static String path = @"..\..\Resources\Data\Secretary.txt";

    }
}