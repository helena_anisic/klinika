// File:    MaritalStatus.cs
// Author:  nikol
// Created: Sunday, April 19, 2020 11:14:05 PM
// Purpose: Definition of Enum MaritalStatus

using System;

namespace Model.users.patient
{
   public enum MaritalStatus
   {
      notMarried,
      married,
      widowed,
      divorced
   }
}