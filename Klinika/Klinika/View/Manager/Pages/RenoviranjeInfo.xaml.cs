﻿using Controller.UsersController.ManagerController;
using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for RenoviranjeInfo.xaml
    /// </summary>
    public partial class RenoviranjeInfo : Window
    {
        Renovation renovation;
        public RenoviranjeInfo(Renovation renovation)
        {
            this.renovation = renovation;
            InitializeComponent();
            datumOd.SelectedDate = renovation.StartDateTime.Date;
            datumDo.SelectedDate = renovation.EndDateTime.Date;
            satiOd.Text = renovation.StartDateTime.ToString("HH:mm");
            satiDo.Text = renovation.EndDateTime.ToString("HH:mm");
            sala.Text = renovation.RoomId;
            razlog.Text = renovation.Description;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Otkazi_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxYesNo w = new MessageBoxYesNo("Otkazi renoviranje", "Da li želite da otkazete renoviranje?", "Odustani", "Otkazi");
            w.ShowDialog();
            if(w.GetCloseStatus() == 1)
            {
                App.RenovationController.DeleteRenovation(renovation);
                this.Close();
            }
        }
    }
}
