// File:    PatientFileRepository.cs
// Author:  nikol
// Created: Saturday, May 30, 2020 2:23:25 AM
// Purpose: Definition of Class PatientFileRepository

using Klinika.Repository;
using Model.users.doctor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.UsersRepositories.DoctorRepositories
{
    public class PatientFileRepository : Repository<PatientFile>
    {
        public static String path = @"..\..\Resources\Data\PatientFiles.txt";

    }
}