﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Klinika.View.Doctor;
using Model.users.patient;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for EditAppointment.xaml
    /// </summary>
    public partial class EditAppointment : Window
    {
        public static ExaminationAppointment ExistingAppointment { get; set; }
        public DateTime Date { get; set; }
        public EditAppointment(ExaminationAppointment appointment)
        {
            ExistingAppointment = appointment;
            InitializeComponent();
            appointmentId.Text = ExistingAppointment.Id;
            cause.Text = ExistingAppointment.Cause;
            patientCondition.Text = ExistingAppointment.PatientCondition;
            PatientsComboBox.ItemsSource = App.PatientController.GetAllPatients();
            PatientsComboBox.SelectedIndex = getIndexOfPatient();
            DoctorsComboBox.ItemsSource = App.DoctorController.GetAllDoctors();
            DoctorsComboBox.SelectedIndex = getIndexOfDoctor();
            date.SelectedDate = ExistingAppointment.StartDateTime;
            startTime.Value = ExistingAppointment.StartDateTime;
            endTime.Value = ExistingAppointment.EndDateTime;
            roomId.Text = ExistingAppointment.RoomId;
        }


        private int getIndexOfPatient()
        {
            for (int i = 0; i < App.PatientController.GetAllPatients().Count; i++)
            {
                if (App.PatientController.GetAllPatients().ElementAt(i).Id.Equals(ExistingAppointment.PatientId))
                {
                    return i;
                }
            }
            return -1;
        }
        private int getIndexOfDoctor()
        {
            for (int i = 0; i < App.DoctorController.GetAllDoctors().Count; i++)
            {
                if (App.DoctorController.GetAllDoctors().ElementAt(i).Id.Equals(ExistingAppointment.DoctorId))
                {
                    return i;
                }
            }
            return -1;
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            ExaminationAppointment UpdatedAppointment = new ExaminationAppointment();
            UpdatedAppointment.Id = ExistingAppointment.Id;
            UpdatedAppointment.Cause = cause.Text;
            UpdatedAppointment.Urgency = ExistingAppointment.Urgency;
            UpdatedAppointment.PatientCondition = patientCondition.Text;
            UpdatedAppointment.PatientId = ((Model.users.user.Patient)PatientsComboBox.SelectedItem).Id;
            UpdatedAppointment.DoctorId = ((Model.users.user.Doctor)DoctorsComboBox.SelectedItem).Id;
            UpdatedAppointment.RoomId = roomId.Text;

            Date = (DateTime)date.SelectedDate;
            UpdatedAppointment.StartDateTime = new DateTime(Date.Year, Date.Month, Date.Day,
                ((DateTime)startTime.Value).Hour, ((DateTime)startTime.Value).Minute, 0);

            UpdatedAppointment.EndDateTime = new DateTime(Date.Year, Date.Month, Date.Day,
                ((DateTime)endTime.Value).Hour, ((DateTime)endTime.Value).Minute, 0);

            for (int i = 0; i < App.ExaminationAppointmentController.GetAllAppointments().Count; i++)
            {
                if (App.ExaminationAppointmentController.GetAllAppointments().ElementAt(i).Id.Equals(UpdatedAppointment.Id))
                {
                    App.ExaminationAppointmentController.EditExaminationAppointment(UpdatedAppointment);
                }
            }
            ExistingAppointment = UpdatedAppointment;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
