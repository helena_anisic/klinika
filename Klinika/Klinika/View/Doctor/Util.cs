﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Klinika.View.Doctor.Model.Collections;
using Klinika.View.Doctor.Model;

namespace Klinika.View.Doctor
{
    [Serializable]
    class Util
    {
        public static ObservableCollection<PatientClass> ListPatients { get; set; }
        public static ObservableCollection<Appointment> ListAppointments { get; set; }
        public static ObservableCollection<User> ListDoctors { get; set; }
        public static ObservableCollection<Medicine> ListMedicines { get; set; }
        public static ObservableCollection<BlogPost> ListBlogPosts { get; set; }
        public static ObservableCollection<String> ListFeedbacks { get; set; }

        public static User loggedInUser;

        public static void loadData()
        {

            string path = "save.xml";
            Application app;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Application));

                StreamReader reader = new StreamReader(path);
                app = ((Application)serializer.Deserialize(reader));
                reader.Close();
            }
            catch
            {
                app = new Application();
            }

            ListPatients = app.Patients;
            ListAppointments = app.Appointments;
            ListDoctors = app.Doctors;
            ListMedicines = app.Medicines;
            ListBlogPosts = app.BlogPosts;
            ListFeedbacks = app.Feedbacks;

        }

        public static void saveData()
        {
            Application app = new Application();
            app.Appointments = ListAppointments;
            app.Patients = ListPatients;
            app.Doctors = ListDoctors;
            app.Medicines = ListMedicines;
            app.BlogPosts = ListBlogPosts;
            app.Feedbacks = ListFeedbacks;
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(Application));
                using (StreamWriter sw = System.IO.File.CreateText("save.xml"))
                {
                    xs.Serialize(sw, app);
                }
            }
            catch (Exception) { }
        }

        public static void snimiPreglede()
        {
            /*Patient p = new Patient();
            p.ID = "1";
            p.Name = "Mira Katic";
            p.Phone = "0612218332";
            ListPatients.Add(p);*/
        }


    }

}
