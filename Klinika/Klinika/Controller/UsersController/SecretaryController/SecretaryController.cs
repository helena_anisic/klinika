// File:    SecretaryController.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:49:01 PM
// Purpose: Definition of Class SecretaryController

using Klinika.Controller.UsersController.SecretaryController;
using Klinika.Service.UsersServices.SecretaryServices;
using Model.users.user;
using Service.UsersServices.SecretaryServices;
using System;

namespace Controller.UsersController.SecretaryController
{
   public class SecretaryController : ISecretaryController
   {
        private ISecretaryService _secretaryService;
        public SecretaryController(ISecretaryService secretaryService)
        {
            _secretaryService = secretaryService;
        }
        public Boolean EditSecretary(Secretary secretary)
      {
            if (secretary == null)
                return false;
            return _secretaryService.EditSecretary(secretary);
        }
        public Secretary GetSecretaryByAccount(Account account) => _secretaryService.GetSecretaryByAccount(account);

        public Boolean CreateSecretary(Secretary secretary)
        {
            if (secretary == null)
            {
                return false;
            }
            return _secretaryService.CreateSecretary(secretary);
        }

        
   
   }
}