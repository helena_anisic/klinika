﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.Hospital;
using Model.users.doctor;
using Syncfusion.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for MedicalCareView.xaml
    /// </summary>
    public partial class MedicalCareView : Window
    {
        private MedicalCare medicalCareLocal;
        public MedicalCareView(PatientFile patientFile, Model.users.user.Patient patient)
        {
            InitializeComponent();
            foreach (MedicalCare m in App.PatientFileController.GetAllMedicalCares())
            {
                if (m.PatientId.Equals(patientFile.PatientId))
                {
                    medicalCareLocal = m;
                    PatientName.Text = patientFile.PatientId;
                }
            }
            if (medicalCareLocal != null)
            {
                medicalCareId.Text = medicalCareLocal.Id;
                hospitalisationCause.Text = medicalCareLocal.HospitalisationCause;
                startDate.SelectedDate = medicalCareLocal.StartDateTime;
                endDate.SelectedDate = medicalCareLocal.EndDateTime;
                roomId.Text = medicalCareLocal.RoomId;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_SaveClick(object sender, RoutedEventArgs e)
        {

            foreach (MedicalCare medicalCare in App.PatientFileController.GetAllMedicalCares())
            {
                if (medicalCare.Id.Equals(medicalCareLocal.Id))
                {
                    medicalCare.Id = medicalCareId.Text;
                    medicalCare.HospitalisationCause = hospitalisationCause.Text;
                    medicalCare.StartDateTime = startDate.SelectedDate.ToDateTime();
                    medicalCare.EndDateTime = endDate.SelectedDate.ToDateTime();
                    medicalCare.RoomId = roomId.Text;

                    App.PatientFileController.EditMedicalCare(medicalCare);
                }
                else
                {
                    medicalCare.Id = medicalCareId.Text;
                    medicalCare.HospitalisationCause = hospitalisationCause.Text;
                    medicalCare.StartDateTime = startDate.SelectedDate.ToDateTime();
                    medicalCare.EndDateTime = endDate.SelectedDate.ToDateTime();
                    medicalCare.RoomId = roomId.Text;

                    App.PatientFileController.CreateMedicalCare(medicalCare);
                }
            }
            Close();
        }
    }
}
