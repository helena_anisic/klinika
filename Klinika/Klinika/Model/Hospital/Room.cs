// File:    Room.cs
// Author:  helen
// Created: Saturday, April 18, 2020 4:22:48 PM
// Purpose: Definition of Class Room

using Model.Storage;
using System;
using System.Collections.Generic;

namespace Model.Hospital
{
    public class Room
    {
        public String id { get; set; }
        public int floor { get; set; }
        public Boolean inUse { get; set; }

        public RoomType roomType { get; set; }
        public List<Equipment> equipment { get; set; }

        public override string ToString()
        {
            return id;
        }
    }
}