﻿using Controller.UsersController.PatientController;
using Controller.UsersController.UserController;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for Prikaz_pacijenta.xaml
    /// </summary>
    public partial class Prikaz_pacijenta : Page
    {
        Model.users.user.Patient p = new Model.users.user.Patient();
        public Prikaz_pacijenta(Model.users.user.Patient p)
        {
            this.p = p;
            List<Country> countries = App.LocationController.GetAllCountries();
            InitializeComponent();
            foreach (Country c in countries)
            {
                Drzava.Items.Add(c.Name);
            }
            if (App.secretary.City != null)
                Grad.Text = p.City.Name;
            if (App.secretary.Country != null)
            {
                Drzava.Text = p.Country.Name;
            }
            Ime.Text = (String) p.Name;
            Prezime.Text = (String)p.Surname;
            JMBG.Text = (String)p.Id;
            BrojTelefona.Text = (String)p.PhoneNumber;
            Grad.Text = p.City.Name;
            Adresa.Text = (String)p.Address;
            datum.Text = p.DateOfBirth.ToString("dd.MM.yyyy.");
            Drzava.Text = p.Country.Name;
            email.Text = (String)p.Email;
            lozinka.Password = (String)p.Password;


        }
        
        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<City> cities = App.LocationController.GetAllCitiesByCountryName(Drzava.SelectedValue.ToString());
            Grad.Items.Clear();
            foreach (City c in cities)
            {
                Grad.Items.Add(c.Name);
            }

        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new PretragaPacijenata();
        }
        private new void MouseDown(object sender, MouseButtonEventArgs e)
        {
            PotvrdaBrisanja pb = new PotvrdaBrisanja();
            pb.Show();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Ime.IsEnabled = true;
            Prezime.IsEnabled = true;
            JMBG.IsEnabled = true;
            datum.IsEnabled = true;
            BrojTelefona.IsEnabled = true;
            Grad.IsEnabled = true;
            Drzava.IsEnabled = true;
            Adresa.IsEnabled = true;
            email.IsEnabled = true;
            lozinka.IsEnabled = true;
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            p.Name = Ime.Text;
            p.Surname = Prezime.Text;
            p.Id = JMBG.Text;
            p.PhoneNumber = BrojTelefona.Text;
            p.DateOfBirth = datum.SelectedDate.Value;
            p.Address = Adresa.Text;
            p.Email = email.Text;
            if (Drzava.SelectedValue != null)
            {
                p.Country = App.LocationController.GetCountryByName(Drzava.SelectedValue.ToString());
            }
            if (Grad.SelectedValue != null)
            {
                p.City = App.LocationController.GetCityByName(Grad.SelectedValue.ToString());
            }
            App.PatientController.EditPatient(p);
            Application.Current.MainWindow.Content = new Meni();
        }
    }
}
