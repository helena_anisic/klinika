﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model
{
    [Serializable]
    public class Medicine
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
        public string Manufactured { get; set; }
        public string Description { get; set; }
        public DateTime ApprovalDate { get; set; }
        public bool Approved { get; set; }



    }
}
