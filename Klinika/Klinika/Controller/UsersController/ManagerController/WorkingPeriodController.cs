// File:    WorkingPeriodController.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:15:17 AM
// Purpose: Definition of Class WorkingPeriodController

using Klinika.Controller.UsersController.ManagerController;
using Klinika.Service.UsersServices.ManagerServices;
using Model.users.manager;
using Model.users.user;
using Service.UsersServices.ManagerServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.ManagerController
{
    public class WorkingPeriodController : IWorkingPeriodController
    {
        private IWorkingPeriodService _workingPeriodService;
        public WorkingPeriodController(IWorkingPeriodService workingPeriodService)
        {
            _workingPeriodService = workingPeriodService;
        }

        public WorkingPeriodController()
        {
        }

        public Boolean CreateWorkingPeriod(WorkingPeriod workingPeriod)
        {
            if (workingPeriod == null)
                return false;
            return _workingPeriodService.CreateWorkingPeriod(workingPeriod);
        }
        public Boolean DeleteWorkingPeriod(WorkingPeriod workingPeriod)
        {
            if (workingPeriod == null)
                return false;
            return _workingPeriodService.DeleteWorkingPeriod(workingPeriod);
        }

        public List<WorkingPeriod> GetAllWorkingPeriods() => _workingPeriodService.GetAllWorkingPeriods();

        public List<WorkingPeriod> GetAllWorkingPeriodsForDay(DateTime date) => _workingPeriodService.GetAllWorkingPeriodsForDay(date);

        public List<WorkingPeriod> GetAllWorkingPeriodsForDoctor(String id) => _workingPeriodService.GetAllWorkingPeriodsForDoctor(id);

        public List<WorkingPeriod> GetAllWorkingPeriods(String DoctorId, DateTime date) => _workingPeriodService.GetAllWorkingPeriodsForDoctor(DoctorId, date);
    }
}