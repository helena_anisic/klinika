﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Lekar.xaml
    /// </summary>
    public partial class Lekar : Page
    {
        public Lekar()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Menu meni = new Menu();
            meni.ContentBox.Content = new NasiLekari();
            Application.Current.MainWindow.Content = meni;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window win = new OceniLekara();
            win.ShowDialog();
        }
    }
}
