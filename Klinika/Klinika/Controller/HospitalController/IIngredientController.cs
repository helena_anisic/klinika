﻿using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.HospitalController
{
    public interface IIngredientController
    {
        List<Ingredient> GetAllIngredients();
    }
}
