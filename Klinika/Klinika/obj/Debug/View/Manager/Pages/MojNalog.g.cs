﻿#pragma checksum "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C61516E2DAE61CE7132FC69E6BFFFFEFC0E7539E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Klinika.View.Manager.Pages;
using MahApps.Metro.IconPacks;
using MahApps.Metro.IconPacks.Converter;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Klinika.View.Manager.Pages {
    
    
    /// <summary>
    /// MojNalog
    /// </summary>
    public partial class MojNalog : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 322 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock name;
        
        #line default
        #line hidden
        
        
        #line 334 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dateOfBirth;
        
        #line default
        #line hidden
        
        
        #line 346 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock adress;
        
        #line default
        #line hidden
        
        
        #line 358 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock place;
        
        #line default
        #line hidden
        
        
        #line 370 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock phone;
        
        #line default
        #line hidden
        
        
        #line 382 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock email;
        
        #line default
        #line hidden
        
        
        #line 409 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton toggle;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Klinika;component/view/manager/pages/mojnalog.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 299 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.name = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.dateOfBirth = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.adress = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.place = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.phone = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.email = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            
            #line 401 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_3);
            
            #line default
            #line hidden
            return;
            case 9:
            this.toggle = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 413 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
            this.toggle.IsEnabledChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.Toggle_IsEnabledChanged);
            
            #line default
            #line hidden
            
            #line 414 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
            this.toggle.Checked += new System.Windows.RoutedEventHandler(this.Toggle_Checked);
            
            #line default
            #line hidden
            
            #line 414 "..\..\..\..\..\View\Manager\Pages\MojNalog.xaml"
            this.toggle.Unchecked += new System.Windows.RoutedEventHandler(this.Toggle_Unchecked);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

