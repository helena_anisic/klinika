﻿using Controller.UsersController.PatientController;
using Controller.UsersController.SecretaryController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for SearchPatient.xaml
    /// </summary>
    public partial class SearchPatient : Page
    {
        public SearchPatient()
        {
            InitializeComponent();
            List<Model.users.user.Patient> pacijenti = App.PatientController.GetAllPatients();
            List<Model.users.user.GuestAccount> guests = App.GuestController.GetAllGuests();
            

            pacijentiGrid.ItemsSource = pacijenti;
            guestGrid.ItemsSource = guests;


            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(pacijentiGrid.ItemsSource);
            
        }
        

        class Pacijenti
        {
            public string imeIprezime { get; set; }
        }

        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {

            Application.Current.MainWindow.Content = new Meni();
        }

        



        private void LekariGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Model.users.user.Patient p = (Model.users.user.Patient)pacijentiGrid.SelectedItem;
            Application.Current.MainWindow.Content = new Zakazivanje_termina(p);
        }

        private void SearchTextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(pacijentiGrid.ItemsSource).Refresh();
            if (pacijentiGrid.Items.Count > 0)
            {
                pacijentiGrid.SelectedIndex = 0;
            }
        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Brza_registracija_zakazivanje();
        }

        private void PacijentiGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
