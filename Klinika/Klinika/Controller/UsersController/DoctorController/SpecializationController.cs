﻿// File:    DoctorController.cs
// Author:  nikol
// Created: Saturday, May 30, 2020 1:31:43 AM
// Purpose: Definition of Class DoctorController

using Klinika.Controller.UsersController.DoctorController;
using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using Service.UsersServices.DoctorServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.DoctorController
{
    public class SpecializationController : ISpecializationController
    {
        private ISpecializationService _specializationService;

        public SpecializationController(ISpecializationService specializationService)
        {
            _specializationService = specializationService;
        }
        public Boolean CreateSpecialization(TypeOfSpecialization specialization)
        {
            if (specialization == null)
            {
                return false;
            }
            return _specializationService.CreateSpecialization(specialization);
        }

        public Boolean EditSpecialization(TypeOfSpecialization specialization)
        {
            return _specializationService.EditSpecialization(specialization);
        }


        public List<TypeOfSpecialization> GetAllSpecialization()
        {
            return _specializationService.GetAllSpecializations();
        }

        public Boolean DeleteSpecialization(TypeOfSpecialization specialization)
        {
            return _specializationService.DeleteSpecialization(specialization);
        }
    }
}