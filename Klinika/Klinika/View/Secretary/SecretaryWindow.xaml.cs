﻿using Controller.UsersController.SecretaryController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for SecretaryWindow.xaml
    /// </summary>
    public partial class SecretaryWindow : Window
    {
        private string _username;
        private string password;

        public string username
        {
            get
            {
                return _username;
            }
            set
            {
                if (value != _username)
                {
                    _username = value;
                    OnPropertyChanged("username");
                }
            }
        }
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public SecretaryWindow()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjY1NDUxQDMxMzgyZTMxMmUzME5sYTlydXZyakdMemRwaFNKc2djMkZqNEdoNXNBT29sTmxpTlUybGZ4ck09");
            InitializeComponent();
            
            this.DataContext = this;
            username = "";
            password = "";
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Model.users.user.Secretary secretary = App.SecretaryController.GetSecretaryByAccount(new Model.users.user.Account(txtUsername.Text, txtPassword.Password));
            App.secretary = secretary;
            if (secretary != null)
                
            {
                Application.Current.MainWindow.Content = new Pocetna();
            }
            else
            {
                MessageBox.Show("Niste dobro uneli vase korisnicko ime ili sifru!");
                return;
            }
            
        }




    }
}