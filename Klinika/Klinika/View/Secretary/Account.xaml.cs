﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for Account.xaml
    /// </summary>
    public partial class Account : Page
    {
        public Account()
        {
            InitializeComponent();
        }

        private void ButtonMojNalog_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new mojNalog();
        }


        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Pocetna();
        }

        private void ButtonPromenaLozinke_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new PromenaLozinke();
        }

        private void ButtonKontakti_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Kontakti_pretraga();
        }

        private void ButtonOdjava_Click(object sender, RoutedEventArgs e)
        {
            
            Application.Current.MainWindow.Content = new SecretaryWindow();
        }
    }
}

