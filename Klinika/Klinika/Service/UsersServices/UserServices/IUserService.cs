﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.UserServices
{
    public interface IUserService
    {
        Boolean CreateUser(User user);
        Boolean EditUser(User user);
        Boolean DeleteUser(string id);
        User GetUserByAccount(Account account);
        List<User> GetAllUsers();

    }
}
