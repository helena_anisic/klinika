﻿using Controller.UsersController.DoctorController;
using Klinika.Controller.UsersController.UserController;
using Model.users.patient;
using Model.users.user;
using Service.UsersServices.DoctorServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        public static Model.users.user.Doctor loggedInUser;

        private string name;
        public new string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                if (value != password)
                {
                    password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public Model.users.user.Doctor Doctor { get; set; }

        public LogIn()
        {
            InitializeComponent();

            Doctor = new Model.users.user.Doctor();
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            foreach (Model.users.user.Doctor doctor in App.DoctorController.GetAllDoctors())
            {
                if (doctor.Username == txtUsername.Text)
                {
                    if (doctor.Password == txtPassword.Password)
                    {
                        loggedInUser = doctor;
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.Visibility = Visibility.Visible;
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (txtPassword.Password == "")
                        {
                            MessageBoxInformation messageBoxInformation = new MessageBoxInformation("Login error", "Enter password.");
                            messageBoxInformation.ShowDialog();
                            return;
                        }
                        else
                        {
                            MessageBoxInformation messageBoxInformation1 = new MessageBoxInformation("Login error", "You entered the wrong password.");
                            messageBoxInformation1.ShowDialog();
                            return;
                        }
                    }
                }
            }
            if (txtUsername.Text == "")
            {
                MessageBoxInformation messageBoxInformation = new MessageBoxInformation("Login error", "Enter username.");
                messageBoxInformation.ShowDialog();
                return;
            }
            MessageBoxInformation messageBoxInformation2 = new MessageBoxInformation("Login error", "You entered the wrong username.");
            messageBoxInformation2.ShowDialog();
            return;

        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
