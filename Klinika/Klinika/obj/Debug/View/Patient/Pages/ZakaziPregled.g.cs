﻿#pragma checksum "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "493D50579A0FC825457CB84869DCF6522E17AAE2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Klinika.Pages;
using MahApps.Metro.IconPacks;
using MahApps.Metro.IconPacks.Converter;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Klinika.Pages {
    
    
    /// <summary>
    /// ZakaziPregled
    /// </summary>
    public partial class ZakaziPregled : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 204 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border kalendar1;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateFrom;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border kalendar2;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateTo;
        
        #line default
        #line hidden
        
        
        #line 263 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox doctor;
        
        #line default
        #line hidden
        
        
        #line 316 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border kalendar3;
        
        #line default
        #line hidden
        
        
        #line 326 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateFrom1;
        
        #line default
        #line hidden
        
        
        #line 346 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border kalendar4;
        
        #line default
        #line hidden
        
        
        #line 356 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateTo2;
        
        #line default
        #line hidden
        
        
        #line 375 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox doctor1;
        
        #line default
        #line hidden
        
        
        #line 384 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton dr;
        
        #line default
        #line hidden
        
        
        #line 388 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton vreme;
        
        #line default
        #line hidden
        
        
        #line 402 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button zakazi;
        
        #line default
        #line hidden
        
        
        #line 408 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid MedOprema;
        
        #line default
        #line hidden
        
        
        #line 440 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona1;
        
        #line default
        #line hidden
        
        
        #line 450 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona2;
        
        #line default
        #line hidden
        
        
        #line 460 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona3;
        
        #line default
        #line hidden
        
        
        #line 470 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona4;
        
        #line default
        #line hidden
        
        
        #line 480 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona5;
        
        #line default
        #line hidden
        
        
        #line 490 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona6;
        
        #line default
        #line hidden
        
        
        #line 500 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn Kolona7;
        
        #line default
        #line hidden
        
        
        #line 545 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Prev;
        
        #line default
        #line hidden
        
        
        #line 546 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock pagin;
        
        #line default
        #line hidden
        
        
        #line 547 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Next;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Klinika;component/view/patient/pages/zakazipregled.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.kalendar1 = ((System.Windows.Controls.Border)(target));
            return;
            case 2:
            this.dateFrom = ((System.Windows.Controls.DatePicker)(target));
            
            #line 221 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.dateFrom.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dateFromChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.kalendar2 = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.dateTo = ((System.Windows.Controls.DatePicker)(target));
            
            #line 251 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.dateTo.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dateToChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.doctor = ((System.Windows.Controls.ComboBox)(target));
            
            #line 263 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.doctor.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Doctor_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 268 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            return;
            case 7:
            this.kalendar3 = ((System.Windows.Controls.Border)(target));
            return;
            case 8:
            this.dateFrom1 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 9:
            this.kalendar4 = ((System.Windows.Controls.Border)(target));
            return;
            case 10:
            this.dateTo2 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 11:
            this.doctor1 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.dr = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 13:
            this.vreme = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 14:
            
            #line 395 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 15:
            this.zakazi = ((System.Windows.Controls.Button)(target));
            
            #line 402 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.zakazi.Click += new System.Windows.RoutedEventHandler(this.zakazi_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.MedOprema = ((System.Windows.Controls.DataGrid)(target));
            
            #line 427 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.MedOprema.SelectedCellsChanged += new System.Windows.Controls.SelectedCellsChangedEventHandler(this.MedOprema_SelectedCellsChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Kolona1 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 18:
            this.Kolona2 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 19:
            this.Kolona3 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 20:
            this.Kolona4 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 21:
            this.Kolona5 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 22:
            this.Kolona6 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 23:
            this.Kolona7 = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 24:
            this.Prev = ((System.Windows.Controls.Button)(target));
            
            #line 545 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.Prev.Click += new System.Windows.RoutedEventHandler(this.prev);
            
            #line default
            #line hidden
            return;
            case 25:
            this.pagin = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 26:
            this.Next = ((System.Windows.Controls.Button)(target));
            
            #line 547 "..\..\..\..\..\View\Patient\Pages\ZakaziPregled.xaml"
            this.Next.Click += new System.Windows.RoutedEventHandler(this.next);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

