// File:    RoomService.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:12:42 PM
// Purpose: Definition of Class RoomService

using Klinika.Service.HospitalServices;
using Model.Hospital;
using Model.users.manager;
using Repository.HospitalRepositories;
using System;
using System.Collections.Generic;

namespace Service.HospitalServices
{
    public class RoomService : IRoomService
    {
        private RoomRepository _roomRepository;

        public RoomService(RoomRepository roomRepository)
        {
            _roomRepository = roomRepository;
        }
        public Boolean CreateRoom(Room room)
        {
            List<Room> rooms = _roomRepository.GetAll(RoomRepository.path);
            if (rooms == null)
            {
                rooms = new List<Room>();
            }
            rooms.Add(room);
            _roomRepository.Save(rooms, RoomRepository.path);
            return true;
        }

        public Boolean EditRoom(Room room)
        {
            if (DeleteRoom(room) == false)
                return false;
            return CreateRoom(room);
        }

        public Boolean DeleteRoom(Room room)
        {
            List<Room> rooms = _roomRepository.GetAll(RoomRepository.path);

            if (rooms == null)
                return false;

            foreach (Room r in rooms)
            {
                if (r.id.Equals(room.id))
                {
                    rooms.Remove(r);
                    break;
                }
            }

            return _roomRepository.Save(rooms, RoomRepository.path);
        }

        public List<Room> GetAllRooms() => _roomRepository.GetAll(RoomRepository.path);


        public Room GetRoomById(String id)
        {
            List<Room> rooms = _roomRepository.GetAll(RoomRepository.path);
            if (rooms == null)
                return null;
            foreach (Room room in rooms)
            {
                if (room.id.Equals(id))
                {
                    return room;
                }
            }
            return null;
        }

    }
}