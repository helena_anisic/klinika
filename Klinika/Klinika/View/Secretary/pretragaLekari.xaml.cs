﻿using Controller.UsersController.DoctorController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for pretragaLekari.xaml
    /// </summary>
    public partial class pretragaLekari : Page
    {
        public pretragaLekari()
        {
            InitializeComponent();
            List<Lekari> pac = new List<Lekari>();
            /*pac.Add(new Lekari { imeIprezime = "Petar Petrovic" });
            pac.Add(new Lekari { imeIprezime = "Marko Markovic" });
            pac.Add(new Lekari { imeIprezime = "Jovan Jovic" });
            pac.Add(new Lekari { imeIprezime = "Marina Jankovic" });
            pac.Add(new Lekari { imeIprezime = "Jelena Jovanovic" });
            pac.Add(new Lekari { imeIprezime = "Marko Nikolic" });
            pac.Add(new Lekari { imeIprezime = "Petar Marinkovic" });
            pac.Add(new Lekari { imeIprezime = "Marko Simic" });
            pac.Add(new Lekari { imeIprezime = "Petar Petrovic" });
            pac.Add(new Lekari { imeIprezime = "Marko Markovic" });
            pac.Add(new Lekari { imeIprezime = "Jovan Jovic" });
            pac.Add(new Lekari { imeIprezime = "Marina Jankovic" });
            pac.Add(new Lekari { imeIprezime = "Jelena Jovanovic" });
            pac.Add(new Lekari { imeIprezime = "Marko Nikolic" });
            pac.Add(new Lekari { imeIprezime = "Petar Marinkovic" });
            pac.Add(new Lekari { imeIprezime = "Marko Simic" });*/
            lekariGrid.ItemsSource = App.DoctorController.GetAllDoctors();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lekariGrid.ItemsSource);
            view.Filter = Search;
        }

        private bool Search(object item)
        {
            if (String.IsNullOrEmpty(SearchTextBox.Text))
                return true;
            else
            {
                if ((item as Lekari).imeIprezime.IndexOf(SearchTextBox.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return true;
                }
                else return false;
            }

        }

        class Lekari
        {
            public string imeIprezime { get; set; }
        }

        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }



        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lekariGrid.ItemsSource).Refresh();
            if (lekariGrid.Items.Count > 0)
            {
                lekariGrid.SelectedIndex = 0;
            }
        }

        private void LekariGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Model.users.user.Doctor d = (Model.users.user.Doctor)lekariGrid.SelectedItem;

            Application.Current.MainWindow.Content = new Prikaz_lekara(d);
        }
    }
}
