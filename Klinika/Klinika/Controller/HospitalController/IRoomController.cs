﻿using Model.Hospital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.HospitalController
{
    public interface IRoomController
    {
        Boolean CreateRoom(Room room);
        Boolean EditRoom(Room room);
        Boolean DeleteRoom(Room room);
        List<Room> GetAllRooms();
        Room GetRoomById(String id);
        Boolean CreateRoomType(RoomType roomType);
        Boolean DeleteRoomType(RoomType roomType);
        List<RoomType> GetAllRoomTypes();

    }
}
