﻿using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.ManagerController
{
    public interface IWorkingPeriodController
    {
        Boolean CreateWorkingPeriod(WorkingPeriod workingPeriod);
        Boolean DeleteWorkingPeriod(WorkingPeriod workingPeriod);
        List<WorkingPeriod> GetAllWorkingPeriods();
        List<WorkingPeriod> GetAllWorkingPeriodsForDay(DateTime date);
        List<WorkingPeriod> GetAllWorkingPeriodsForDoctor(String id);
        List<WorkingPeriod> GetAllWorkingPeriods(String DoctorId, DateTime date);

    }
}
