// File:    RoomController.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:12:42 PM
// Purpose: Definition of Class RoomController

using Klinika.Controller.HospitalController;
using Klinika.Service.HospitalServices;
using Model.Hospital;
using Model.users.manager;
using Service.HospitalServices;
using System;
using System.Collections.Generic;

namespace Controller.HospitalController
{
    public class RoomController : IRoomController
    {
        private IRoomService _roomService;
        private IRoomTypeService _roomTypeService;

        public RoomController(IRoomService roomService,  IRoomTypeService roomTypeService)
        {
            _roomService = roomService;
            _roomService = roomService;
        }

        public RoomController()
        {
        }

        public Boolean CreateRoom(Room room) => _roomService.CreateRoom(room);


        public Boolean EditRoom(Room room) => _roomService.EditRoom(room);

        public Boolean DeleteRoom(Room room) => _roomService.DeleteRoom(room);

        public List<Room> GetAllRooms() => _roomService.GetAllRooms();

        public Room GetRoomById(String id) => _roomService.GetRoomById(id);

        public Boolean CreateRoomType(RoomType roomType) => _roomTypeService.CreateRoomType(roomType);

        public Boolean DeleteRoomType(RoomType roomType) => _roomTypeService.DeleteRoomType(roomType);

        public List<RoomType> GetAllRoomTypes() => _roomTypeService.GetAllRoomTypes();
    }
}