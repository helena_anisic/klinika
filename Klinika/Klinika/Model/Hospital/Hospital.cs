// File:    Hospital.cs
// Author:  helen
// Created: Saturday, April 18, 2020 4:18:46 PM
// Purpose: Definition of Class Hospital

using Model.users.user;
using System;

namespace Model.Hospital
{
   public class Hospital
   {
      private long pib;
      private String name;
      private String address;
      
      public City city;
      
      /// <summary>
      /// Property for City
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Room[] room;
   
   }
}