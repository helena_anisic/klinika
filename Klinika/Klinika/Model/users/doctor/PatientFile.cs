// File:    PatientFile.cs
// Author:  Admin
// Created: Monday, April 20, 2020 12:17:09 PM
// Purpose: Definition of Class PatientFile

using System;
using System.Collections.Generic;

namespace Model.users.doctor
{
    public class PatientFile
    {
        public String Id { get; set; }
        public String PatientId { get; set; }
        public String Anamnesis { get; set; }
        public String Vaccubation { get; set; }
        public String PhoneOfFamily { get; set; }
        public List<ResultOfMedicalExamination> ResultsOfMedicalExamination { get; set; }
        public List<Allergy> Allergy { get; set; }

    }
}