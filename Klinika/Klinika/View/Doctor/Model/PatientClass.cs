﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model
{
    [Serializable]
    public class PatientClass
    {
        public string Name { get; set; }

        public string ID { get; set; }
        public string DateOfBirth { get; set; }
        public string Phone { get; set; }
        public string ResidentialAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }

        public PatientFile PatientFile { get; set; }

    }
}
