// File:    MedicineService.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:31:57 PM
// Purpose: Definition of Class MedicineService

using Klinika.Repository.HospitalRepositories;
using Klinika.Service.HospitalServices;
using Model.Storage;
using Repository.HospitalRepositories;
using System;
using System.Collections.Generic;

namespace Service.HospitalServices
{
    public class MedicineService : IMedicineService
    {
        private MedicineRepository _medicineRepository;
        private MedicineTypeRepository _medicineTypeRepository;

        public MedicineService(MedicineRepository medicineRepository, MedicineTypeRepository medicineTypeRepository)
        {
            _medicineRepository = medicineRepository;
            _medicineTypeRepository = medicineTypeRepository;
        }
        public Boolean CreateMedicine(Medicine medicine)
        {
            List<Medicine> medicines = _medicineRepository.GetAll(MedicineRepository.path);
            if (medicines == null)
            {
                medicines = new List<Medicine>();
            }
            medicines.Add(medicine);
            _medicineRepository.Save(medicines, MedicineRepository.path);
            return true;
        }

        public List<MedicineType> GetAllMedicinesTypes() => _medicineTypeRepository.GetAll(MedicineTypeRepository.path);

        public List<Medicine> GetAllMedicine() => _medicineRepository.GetAll(MedicineRepository.path);

        public Boolean EditMedicine(Medicine medicine)
        {
            if (DeleteMedicine(medicine) == false)
                return false;
            return CreateMedicine(medicine);
        }

        public Boolean DeleteMedicine(Medicine medicine)
        {
            List<Medicine> medicines = _medicineRepository.GetAll(MedicineRepository.path);

            if (medicines == null)
                return false;

            foreach (Medicine m in medicines)
            {
                if (m.Id.Equals(medicine.Id))
                {
                    medicines.Remove(m);
                    break;
                }
            }

            return _medicineRepository.Save(medicines, MedicineRepository.path);
        }

    }
}