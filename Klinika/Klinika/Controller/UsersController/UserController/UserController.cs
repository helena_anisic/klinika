// File:    UserController.cs
// Author:  nikol
// Created: Tuesday, May 26, 2020 9:11:51 PM
// Purpose: Definition of Class UserController

using Klinika.Service.UsersServices.UserServices;
using Model.users.user;
using Service.UsersServices.PatientServices;
using Service.UsersServices.UserServices;
using System;
using System.Collections.Generic;

namespace Klinika.Controller.UsersController.UserController
{
    public class UserController : IUserController
    {
        private IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        public List<User> GetAllUsers() => _userService.GetAllUsers();
    }
}