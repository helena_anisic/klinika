// File:    PatientRepository.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:48:59 PM
// Purpose: Definition of Class PatientRepository

using Klinika.Repository;
using Model.users.user;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.UsersRepositories.PatientRepositories
{
    public class PatientRepository : Repository<Patient>
    {
        public static String path = @"..\..\Resources\Data\Patients.txt";

    }
}