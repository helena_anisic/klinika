﻿using Controller.UsersController.DoctorController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika
{
    /// <summary>
    /// Interaction logic for OperacijaDetaljno.xaml
    /// </summary>
    public partial class OperacijaDetaljno : Window
    {
        public Surgery surgery;
        public OperacijaDetaljno(Surgery surgery)
        {
            this.surgery = surgery;
            InitializeComponent();
            datumm.Text = surgery.StartDateTime.ToString("dd.MM.yyyy.");
            vreme.Text = surgery.StartDateTime.ToString("HH:mm") + " - " + surgery.EndDateTime.ToString("HH:mm");
            Model.users.user.Doctor dr = App.DoctorController.GetDoctorById(surgery.DoctorId);
            if (dr != null)
                doktor.Text = dr.ToString();
            soba.Text = surgery.RoomId;

            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Otkazivanje_MouseDown(object sender, MouseButtonEventArgs e)
        {
            App.SurgeryController.DeleteSurgery(surgery);
            this.Close();
        }
    }
}
