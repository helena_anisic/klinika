﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Syncfusion.Windows.Shared;
using Model.users.patient;
using Controller.UsersController.PatientController;
using Controller.UsersController.DoctorController;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for AddAppointment.xaml
    /// </summary>
    public partial class AddAppointment : Window
    {
        private ExaminationAppointment appointment;
        private List<Model.users.user.Doctor> Doctors;
        private List<Model.users.user.Patient> Patients;

        public DateTime Date { get; set; }

        public AddAppointment()
        {
            InitializeComponent();
            appointment = new ExaminationAppointment();
            appointment.StartDateTime = new DateTime();
            appointment.EndDateTime = new DateTime();
            Doctors = App.DoctorController.GetAllDoctors();
            Patients = App.PatientController.GetAllPatients();

            DoctorsComboBox.ItemsSource = Doctors;
            PatientsComboBox.ItemsSource = Patients;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            appointment.Id = appointmentId.Text;
            appointment.Cause = cause.Text;
            appointment.Urgency = false;
            appointment.PatientCondition = patientCondition.Text;
            appointment.PatientId = ((Model.users.user.Patient)PatientsComboBox.SelectedItem).Id;
            appointment.DoctorId = ((Model.users.user.Doctor)DoctorsComboBox.SelectedItem).Id;
            appointment.RoomId = roomId.Text;


            if (date.SelectedDate.HasValue)
            {
                string dateCalendar = date.SelectedDate.Value.ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                Date = (DateTime)date.SelectedDate;
            }
            DateTime start = (DateTime)startTime.Value;
            appointment.StartDateTime = new DateTime(Date.Year, Date.Month, Date.Day, start.Hour, start.Minute, 0);
            DateTime end = (DateTime)endTime.Value;
            appointment.EndDateTime = new DateTime(Date.Year, Date.Month, Date.Day, end.Hour, end.Minute, 0);

            App.ExaminationAppointmentController.CreateExaminationAppointment(appointment);

            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
