﻿using Controller.UsersController.PatientController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for PretragaPacijenata.xaml
    /// </summary>
    public partial class PretragaPacijenata : Page
    {
        public PretragaPacijenata()
        {
            InitializeComponent();
            List<Pacijenti> lek = new List<Pacijenti>();
            /*lek.Add(new Pacijenti { imeIprezime = "Petar Petrovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marko Markovic" });
            lek.Add(new Pacijenti { imeIprezime = "Jovan Jovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marina Jankovic" });
            lek.Add(new Pacijenti { imeIprezime = "Jelena Jovanovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marko Nikolic" });
            lek.Add(new Pacijenti { imeIprezime = "Petar Marinkovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marko Simic" });
            lek.Add(new Pacijenti { imeIprezime = "Petar Petrovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marko Markovic" });
            lek.Add(new Pacijenti { imeIprezime = "Jovan Jovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marina Jankovic" });
            lek.Add(new Pacijenti { imeIprezime = "Jelena Jovanovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marko Nikolic" });
            lek.Add(new Pacijenti { imeIprezime = "Petar Marinkovic" });
            lek.Add(new Pacijenti { imeIprezime = "Marko Simic" });*/
            pacijentiGrid.ItemsSource = App.PatientController.GetAllPatients();

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(pacijentiGrid.ItemsSource);
            view.Filter = Search;
        }
        private bool Search(object item)
        {
            if (String.IsNullOrEmpty(SearchTextBox.Text))
                return true;
            else
            {
                if ((item as Pacijenti).imeIprezime.IndexOf(SearchTextBox.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return true;
                }
                else return false;
            }

        }

        class Pacijenti
        {
            public string imeIprezime { get; set; }
        }

        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }
        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(pacijentiGrid.ItemsSource).Refresh();
            if (pacijentiGrid.Items.Count > 0)
            {
                pacijentiGrid.SelectedIndex = 0;
            }
        }




        private void LekariGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            Model.users.user.Patient p = (Model.users.user.Patient)pacijentiGrid.SelectedItem;
            Application.Current.MainWindow.Content = new Prikaz_pacijenta(p);

        }
    }
}
