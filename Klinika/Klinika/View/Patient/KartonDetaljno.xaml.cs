﻿using Klinika.Controller.UsersController.DoctorController;
using Klinika.i18N;
using Klinika.Pages;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika
{
    /// <summary>
    /// Interaction logic for KartonDetaljno.xaml
    /// </summary>
    public partial class KartonDetaljno : Window
    {
        public KartonDetaljno(ResultOfMedicalExamination result)
        {
            InitializeComponent();
            String sifra = App.patient.Id;
            sifra += "/";
            sifra += result.Id;
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            symptoms.Text = result.Symptoms;
            List<Therapy> therapies = App.TherapyController.GetAllTherapies();
            String terapije = "";
            foreach(Therapy terapija in therapies)
            {
                if (terapija.Id.Equals(sifra))
                {
                     terapije += terapija.MedicineName;
                    
                }
            }
            therapy.Text = terapije;
            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
