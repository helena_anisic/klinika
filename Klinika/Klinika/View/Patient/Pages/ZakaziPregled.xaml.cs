﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Controller.UsersController.PatientController;
using Klinika.Data;
using Klinika.i18N;
using Klinika.View.Patient;
using Model.users.manager;
using Model.users.patient;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for ZakaziPregled.xaml
    /// </summary>
    public partial class ZakaziPregled : Page
    {
        public ZakaziPregled()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            dateFrom.SelectedDate = DateTime.Now;
            dateTo.SelectedDate = DateTime.Now.AddDays(6);
            doctor.ItemsSource = App.DoctorController.GetAllDoctors();
            doctor1.ItemsSource = doctor.ItemsSource;
            MedOprema.ItemsSource = Data.Calendar.Data(DateTime.Now.Date, DateTime.Now.Date, dateTo.SelectedDate.Value, null);

            Kolona1.Header = DateTime.Now.ToString("dd.MM.yyyy");
            Kolona2.Header = DateTime.Now.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = DateTime.Now.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = DateTime.Now.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = DateTime.Now.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = DateTime.Now.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = DateTime.Now.AddDays(6).ToString("dd.MM.yyyy");

            pagin.Text = "1 of 1";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dateFromChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dateTo.SelectedDate == null)
                return;
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, null);

            Kolona1.Header = dateFrom.SelectedDate.Value.ToString("dd.MM.yyyy");
            Kolona2.Header = dateFrom.SelectedDate.Value.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = dateFrom.SelectedDate.Value.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = dateFrom.SelectedDate.Value.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = dateFrom.SelectedDate.Value.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = dateFrom.SelectedDate.Value.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = dateFrom.SelectedDate.Value.AddDays(6).ToString("dd.MM.yyyy");
        }

        private void dateToChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, null);

            Kolona1.Header = dateFrom.SelectedDate.Value.ToString("dd.MM.yyyy");
            Kolona2.Header = dateFrom.SelectedDate.Value.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = dateFrom.SelectedDate.Value.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = dateFrom.SelectedDate.Value.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = dateFrom.SelectedDate.Value.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = dateFrom.SelectedDate.Value.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = dateFrom.SelectedDate.Value.AddDays(6).ToString("dd.MM.yyyy");
        }

        private void prev(object sender, RoutedEventArgs e)
        {
            DateTime pocetak = DateTime.ParseExact(Kolona1.Header.ToString(), "dd.MM.yyyy", null).AddDays(-7);
            MedOprema.ItemsSource = Data.Calendar.Data(pocetak, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, null);

            Kolona1.Header = pocetak.ToString("dd.MM.yyyy");
            Kolona2.Header = pocetak.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = pocetak.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = pocetak.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = pocetak.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = pocetak.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = pocetak.AddDays(6).ToString("dd.MM.yyyy");
        }

        private void next(object sender, RoutedEventArgs e)
        {
            DateTime pocetak = DateTime.ParseExact(Kolona1.Header.ToString(), "dd.MM.yyyy", null).AddDays(+7);
            MedOprema.ItemsSource = Data.Calendar.Data(pocetak, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, null);

            Kolona1.Header = pocetak.ToString("dd.MM.yyyy");
            Kolona2.Header = pocetak.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = pocetak.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = pocetak.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = pocetak.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = pocetak.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = pocetak.AddDays(6).ToString("dd.MM.yyyy");
        }

        private void MedOprema_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            
        }

        private void zakazi_Click(object sender, RoutedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count <= 0)
                return;
            int kolona = MedOprema.SelectedCells[0].Column.DisplayIndex;
            int red = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            String datum = MedOprema.SelectedCells[0].Column.Header.ToString();

            int sati = red / 2;
            int psati = sati;
            int minuta = (red % 2 == 1) ? 30 : 0;
            String termin = (sati<10)? "0" + sati.ToString() : sati.ToString();
            termin += ":";
            termin += (minuta == 30) ?  "30" : "00";
            if (minuta == 30)
                sati++;
            if (sati == 24)
                sati = 0;
            termin += " - ";
            termin += (sati < 10) ? "0" + sati.ToString() : sati.ToString();
            termin += ":";
            termin += (minuta == 30) ? "00" : "30";

            Doctor dc = (Doctor)doctor.SelectedItem;

            DateTime date = DateTime.ParseExact(datum, "dd.MM.yyyy", null);
            date = date.AddHours(psati).AddMinutes(minuta);

            if (dc == null)
            {
                dc = App.ExaminationAppointmentController.GetFreeDoctorForTerm(date, date.AddHours(sati).AddMinutes(minuta));
            }

            Window win = new PotvrdaTermina(DateTime.Now, termin, date, dc);
            win.ShowDialog();
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Doctor)doctor.SelectedItem);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DateTime startDate = dateFrom1.SelectedDate.Value;
            DateTime endDate = dateTo2.SelectedDate.Value;
            if (dr.IsChecked == true)
            {
                ExaminationAppointment examinationAppointment = App.ExaminationAppointmentController.GetFreeAppointment(startDate, endDate, ((Doctor)doctor1.SelectedItem).Id);
                if (examinationAppointment == null)
                    MessageBox.Show("Nema termina!");
                else
                {
                    examinationAppointment.PatientId = App.patient.Id;
                    Window win = new PotvrdaTermina(DateTime.Now, examinationAppointment.StartDateTime, App.DoctorController.GetDoctorById(examinationAppointment.DoctorId));
                    win.ShowDialog();
                }
            }
            else
            {
                ExaminationAppointment examinationAppointment = App.ExaminationAppointmentController.GetFreeAppointment(startDate, endDate);
                if (examinationAppointment == null)
                    MessageBox.Show("Nema termina!");
                else
                {
                    examinationAppointment.PatientId = App.patient.Id;
                    Window win = new PotvrdaTermina(DateTime.Now, examinationAppointment.StartDateTime, App.DoctorController.GetDoctorById(examinationAppointment.DoctorId));
                    win.ShowDialog();
                }
            }
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Doctor)doctor.SelectedItem);
        }

        private void Doctor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Doctor)doctor.SelectedItem);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if ((Doctor)doctor.SelectedItem == null)
                return;
            DateTime pocetak = DateTime.ParseExact(Kolona1.Header.ToString(), "dd.MM.yyyy", null);
            MedOprema.ItemsSource = Data.Calendar.Data(pocetak, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Doctor)doctor.SelectedItem);

            Kolona1.Header = pocetak.ToString("dd.MM.yyyy");
            Kolona2.Header = pocetak.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = pocetak.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = pocetak.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = pocetak.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = pocetak.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = pocetak.AddDays(6).ToString("dd.MM.yyyy");
        }
    }
}
