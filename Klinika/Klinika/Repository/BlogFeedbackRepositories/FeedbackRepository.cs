// File:    FeedbackRepository.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:07:01 AM
// Purpose: Definition of Class FeedbackRepository

using Klinika.Repository;
using Model.users.user;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.BlogFeedbackRepositories
{
    public class FeedbackRepository : Repository<Feedback>
    {
        public static String path = @"..\..\Resources\Data\Feedbacks.txt";

    }
}