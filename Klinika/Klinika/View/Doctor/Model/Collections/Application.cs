﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model.Collections
{
    [Serializable]
    public class Application
    {
        public ObservableCollection<Appointment> Appointments { get; set; }
        public ObservableCollection<PatientClass> Patients { get; set; }
        public ObservableCollection<User> Doctors { get; set; }
        public ObservableCollection<Medicine> Medicines { get; set; }
        public ObservableCollection<BlogPost> BlogPosts { get; set; }
        public ObservableCollection<String> Feedbacks { get; set; }

        public Application()
        {
            Appointments = new ObservableCollection<Appointment>();
            Patients = new ObservableCollection<PatientClass>();
            Doctors = new ObservableCollection<User>();
            Medicines = new ObservableCollection<Medicine>();
            BlogPosts = new ObservableCollection<BlogPost>();
            Feedbacks = new ObservableCollection<string>();
        }
    }
}
