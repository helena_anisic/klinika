﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.UserController
{
    public interface IUserController
    {
        List<User> GetAllUsers();
    }
}
