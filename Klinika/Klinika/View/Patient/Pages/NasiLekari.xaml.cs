﻿using Controller.UsersController.DoctorController;
using Klinika.i18N;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for NasiLekari.xaml
    /// </summary>
    public partial class NasiLekari : Page
    {
        Doctor doctor;
        public NasiLekari()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            TabelaLekari.ItemsSource = App.DoctorController.GetAllDoctors();
            
            
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Menu meni = new Menu();
            meni.ContentBox.Content = new Lekar();
            Application.Current.MainWindow.Content = meni;
        }

        private void TabelaLekari_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            
            

        }

        private void zvezdice_ValueChanged(object sender, RoutedPropertyChangedEventArgs<int> e)
        {
            doctor = (e.OriginalSource as FrameworkElement).DataContext as Doctor;
            MessageBox.Show(doctor.Password);
            List<Doctor> doktori = App.DoctorController.GetAllDoctors();
            foreach(Doctor d in doktori)
            {
                MessageBox.Show(d.Name);
                MessageBox.Show(doctor.Id);


                if (d.Name.Equals(doctor.Name))
                {
                    d.Rating.Add(e.NewValue);
                    App.DoctorController.EditDoctor(d);
                }
            }

            
        }
    }
}
