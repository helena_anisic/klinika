// File:    ArticleService.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 4:57:19 PM
// Purpose: Definition of Class ArticleService


using Klinika.Service.BlogFeedbackServices;
using Model.users.doctor;
using Repository.BlogFeedbackRepositories;
using System;
using System.Collections.Generic;

namespace Service.BlogFeedbackServices
{
    public class ArticleService : IArticleService
    {
        private ArticleRepository _articleRepository;

        public ArticleService(ArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }
        public Boolean CreateBlogArticle(BlogArticle blogArticle)
        {
            List<BlogArticle> blogArticles = _articleRepository.GetAll(ArticleRepository.path);
            blogArticles.Add(blogArticle);
            return _articleRepository.Save(blogArticles, ArticleRepository.path);
        }


        public BlogArticle GetBlogArticleById(String Id)
        {
            List<BlogArticle> blogArticles = _articleRepository.GetAll(ArticleRepository.path);
            if (blogArticles == null)
                return null;
            foreach (BlogArticle article in blogArticles)
            {
                if (article.Id.Equals(Id))
                {
                    return article;
                }
            }
            return null;
        }

        public List<BlogArticle> GetAllBlogArticles()
        {
            return _articleRepository.GetAll(ArticleRepository.path);
        }
    }
}