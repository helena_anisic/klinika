﻿using Controller.UsersController.ManagerController;
using Controller.UsersController.PatientController;
using Model.users.manager;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Patient
{
    /// <summary>
    /// Interaction logic for PotvrdaTermina.xaml
    /// </summary>
    public partial class PotvrdaTermina : Window
    {
        public String termin;
        public DateTime datumm;
        public Model.users.user.Doctor doctor;
        public String id;
        public PotvrdaTermina(DateTime dt, String termin, DateTime datum, Model.users.user.Doctor doctor)
        {
            if(doctor == null)
            {
                this.Close();
            }
            this.doctor = doctor;
            this.termin = termin;
            this.datumm = datum;
            InitializeComponent();
            lekar.Text = doctor.ToString();
            vreme.Text = termin;
            datummm.Text = datumm.ToString("dd.MM.yyyy.");
            this.id = dt.ToString();

        }

        public PotvrdaTermina(DateTime dt, DateTime datum, Model.users.user.Doctor doctor)
        {
            if (doctor == null)
            {
                this.Close();
            }
            this.doctor = doctor;
            this.termin = datum.ToString();
            this.datumm = datum;
            InitializeComponent();
            lekar.Text = doctor.ToString();
            vreme.Text = termin;
            datummm.Text = datumm.ToString("dd.MM.yyyy.");
            this.id = dt.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            List<WorkingPeriod> wps = App.WorkingPeriodController.GetAllWorkingPeriods(doctor.Id, datumm);
            String roomId = "";
            if (wps != null)
                roomId = wps[0].RoomId;
            ExaminationAppointment examination = new ExaminationAppointment() { Id = id, DoctorId = doctor.Id, StartDateTime = datumm, EndDateTime = datumm.AddMinutes(30), PatientId = App.patient.Id, RoomId = roomId };
            App.ExaminationAppointmentController.CreateExaminationAppointment(examination);
            this.Close();
        }
    }
}
