﻿using Klinika.i18N;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Homepage.xaml
    /// </summary>
    public partial class Homepage : Page
    {
        public Homepage()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            fillForm();
        }

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Menu meni = new Menu();
            meni.ContentBox.Content = new ZakazaniPregledi();
            Application.Current.MainWindow.Content = meni;
        }

        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Menu meni = new Menu();
            meni.ContentBox.Content = new ZakazaneOperacije();
            Application.Current.MainWindow.Content = meni;
        }

        private void fillForm()
        {
            name.Text = App.patient.Name;
        }
    }
}
