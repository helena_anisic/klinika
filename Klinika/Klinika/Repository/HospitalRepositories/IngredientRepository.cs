// File:    IngredientRepository.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:35:23 AM
// Purpose: Definition of Class IngredientRepository

using Klinika.Repository;
using Model.Storage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.HospitalRepositories
{
    public class IngredientRepository : Repository<Ingredient>
    {
        public static String path = @"..\..\Resources\Data\Ingredients.txt";


    }
}