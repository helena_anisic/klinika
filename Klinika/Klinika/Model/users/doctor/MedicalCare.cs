// File:    MedicalCare.cs
// Author:  Admin
// Created: Monday, April 20, 2020 5:33:38 PM
// Purpose: Definition of Class MedicalCare

using Model.users.manager;
using System;

namespace Model.users.doctor
{
    public class MedicalCare : Term
    {
        public String Id { get; set; }
        public String PatientId { get; set; }
        public String HospitalisationCause { get; set; }

        public MedicalCare() { }

        public MedicalCare(String id, String patientId, String hospitalisationCause, DateTime startDate, DateTime endDate, String room)
        {
            Id = id;
            PatientId = patientId;
            HospitalisationCause = hospitalisationCause;
            StartDateTime = startDate;
            EndDateTime = endDate;
            RoomId = room;
        }

    }
}