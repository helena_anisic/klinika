// File:    CountryService.cs
// Author:  nikol
// Created: Friday, May 29, 2020 11:29:11 AM
// Purpose: Definition of Class CountryService

using Klinika.Service.UsersServices.UserServices;
using Model.Hospital;
using Model.users.user;
using Repository.UsersRepositories.UserRepository;
using System;
using System.Collections.Generic;
using System.Security.Policy;

namespace Service.UsersServices.UserServices
{
    public class CountryService : ICountryService
    {
        private CountryRepository _countryRepository;

        public CountryService(CountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }
        public List<Country> GetAllCountries() => _countryRepository.GetAll(CountryRepository.path);

        public Country GetCountryByName(String countryName)
        {
            {
                List<Country> countries = _countryRepository.GetAll(CountryRepository.path);
                if (countries == null)
                    return null;
                foreach (Country country in countries)
                {
                    if (country.Name.Equals(countryName))
                    {
                        return country;
                    }
                }
                return null;
            }
        }
    }
}