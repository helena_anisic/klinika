﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Repository.UsersRepositories.DoctorRepositories
{
    public class ResultOfExaminationRepository : Repository<ResultOfMedicalExamination>
    {
        public static String path = @"..\..\Resources\Data\ResultOfMedicalExamination.txt";
    }
}
