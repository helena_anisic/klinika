﻿using Klinika.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Data
{
    class KartonPodaci
    {
        public string Tip { get; set; }
        public string Dijagnoza { get; set; }
        public string Lekar { get; set; }
        public string DatumDijagnoze { get; set; }
        public string Napomena { get; set; }
        public static ObservableCollection<KartonPodaci> Data()
        {
            ObservableCollection<KartonPodaci> lista = new ObservableCollection<KartonPodaci>();
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            lista.Add(new KartonPodaci
            {
                Tip = "alergija",
                Dijagnoza = "polen",
                Lekar = "dr Pera Peric",
                DatumDijagnoze = "13.4.2020",
                Napomena = ""
            });
            return lista;
        }
    }
}
