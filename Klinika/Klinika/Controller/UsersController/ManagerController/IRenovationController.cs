﻿using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.ManagerController
{
    public interface IRenovationController
    {
        Boolean CreateRenovation(Renovation renovation);
        Boolean EditRenovation(Renovation renovation);
        Boolean DeleteRenovation(Renovation renovation);
        List<Renovation> GetAllRenovations();

    }
}
