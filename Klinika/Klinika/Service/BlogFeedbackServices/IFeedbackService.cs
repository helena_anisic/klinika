﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.BlogFeedbackServices
{
    public interface IFeedbackService
    {
        Boolean CreateFeedback(Feedback feedback);
    }
}
