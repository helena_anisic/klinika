﻿#pragma checksum "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "05DB7AEF0C83EA63F1D50043B9D48D3E333CF6D3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Klinika.View.Secretary;
using MahApps.Metro.IconPacks;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Klinika.View.Secretary {
    
    
    /// <summary>
    /// Prikaz_pacijenta
    /// </summary>
    public partial class Prikaz_pacijenta : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 142 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Ime;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Prezime;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox JMBG;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker datum;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox BrojTelefona;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Grad;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Drzava;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Adresa;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox email;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox lozinka;
        
        #line default
        #line hidden
        
        
        #line 204 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnIzmeni;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOK;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Klinika;component/view/secretary/prikaz%20pacijenta.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 140 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
            ((MahApps.Metro.IconPacks.PackIconMaterial)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.PackIconMaterial_MouseDown_1);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 141 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
            ((MahApps.Metro.IconPacks.PackIconMaterial)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.MouseDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Ime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.Prezime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.JMBG = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.datum = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 7:
            this.BrojTelefona = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Grad = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.Drzava = ((System.Windows.Controls.ComboBox)(target));
            
            #line 180 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
            this.Drzava.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Country_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.Adresa = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.email = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.lozinka = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 13:
            this.btnIzmeni = ((System.Windows.Controls.Button)(target));
            
            #line 204 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
            this.btnIzmeni.Click += new System.Windows.RoutedEventHandler(this.BtnIzmeni_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btnOK = ((System.Windows.Controls.Button)(target));
            
            #line 205 "..\..\..\..\View\Secretary\Prikaz pacijenta.xaml"
            this.btnOK.Click += new System.Windows.RoutedEventHandler(this.BtnOK_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

