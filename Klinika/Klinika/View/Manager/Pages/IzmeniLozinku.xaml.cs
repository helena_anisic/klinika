﻿using Controller.UsersController.ManagerController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for IzmeniLozinku.xaml
    /// </summary>
    public partial class IzmeniLozinku : Window
    {
        public IzmeniLozinku()
        {
            InitializeComponent();
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            if (App.manager.Password.Equals(oldPass.Text))
            {
                if (!newPass.Text.Equals(newPassConf.Text))
                {
                    message.Text = "Nove lozinke se ne poklapaju";
                }
                else
                {
                    App.manager.Password = newPass.Text;
                    App.ManagerController.EditManager(App.manager);
                    this.Close();
                }
            }
            else
            {
                message.Text = "Stara lozinka neispravna!";
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            message.Text = "";
        }
    }
}
