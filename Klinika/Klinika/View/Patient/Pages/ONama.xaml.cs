﻿using Klinika.i18N;
using Klinika.View.Patient.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for ONama.xaml
    /// </summary>
    public partial class ONama : Page
    {
        public ONama()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
  
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Window win = new OceniNas();
            win.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Menu meni = new Menu();
            meni.ContentBox.Content = new Anketa();
            Application.Current.MainWindow.Content = meni;
        }
    }
}
