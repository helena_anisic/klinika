﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.SecretaryController
{
    public interface IGuestController
    {
        Boolean CreateGuestAccount(GuestAccount guestAccount);
        List<GuestAccount> GetAllGuests();

    }
}
