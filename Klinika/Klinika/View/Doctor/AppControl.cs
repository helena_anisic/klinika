﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Klinika.View.Doctor
{
    public static class AppControl
    {
        public static readonly RoutedUICommand Menu = new RoutedUICommand(
            "Menu",
            "Menu",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.O, ModifierKeys.Control)
            }
            );

        public static readonly RoutedUICommand HomePage = new RoutedUICommand(
            "HomePage",
            "HomePage",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.Home, ModifierKeys.Alt)
            }
            );

        public static readonly RoutedUICommand Schedule = new RoutedUICommand(
            "Schedule",
            "Schedule",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.C, ModifierKeys.Control)
            }
            );

        public static readonly RoutedUICommand Medicine = new RoutedUICommand(
            "Medicine",
            "Medicine",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.M, ModifierKeys.Control),
            }
            );

        public static readonly RoutedUICommand Patients = new RoutedUICommand(
            "Patients",
            "Patients",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.P, ModifierKeys.Control)
            }
            );
        public static readonly RoutedUICommand Report = new RoutedUICommand(
            "Report",
            "Report",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.R, ModifierKeys.Control)
            }
            );

        public static readonly RoutedUICommand Blog = new RoutedUICommand(
            "Blog",
            "Blog",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.B, ModifierKeys.Control)
            }
            );

        public static readonly RoutedUICommand MyAccount = new RoutedUICommand(
            "MyAccount",
            "MyAccount",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.A, ModifierKeys.Control)
            }
            );

        public static readonly RoutedUICommand Help = new RoutedUICommand(
            "Help",
            "Help",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.H, ModifierKeys.Control)
            }
            );

        public static readonly RoutedUICommand LogOut = new RoutedUICommand(
            "LogOut",
            "LogOut",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.L, ModifierKeys.Control)
            }
            );
        public static readonly RoutedUICommand AddAppointment = new RoutedUICommand(
            "AddAppointment",
            "AddAppointment",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.A, ModifierKeys.Control)
            }
            );
        public static readonly RoutedUICommand EditAppointment = new RoutedUICommand(
            "EditAppointment",
            "EditAppointment",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.E, ModifierKeys.Control)
            }
            );
        public static readonly RoutedUICommand DeleteAppointment = new RoutedUICommand(
            "DeleteAppointment",
            "DeleteAppointment",
            typeof(AppControl),
            new InputGestureCollection()
            {
                new KeyGesture(Key.Delete)
            }
            );
    }
}
