﻿using Syncfusion.UI.Xaml.Schedule;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Klinika.View.Doctor;
using Klinika.View.Doctor.Pages;
using Controller.UsersController.PatientController;
using Model.users.patient;
using Controller.UsersController.DoctorController;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for ScheduleView.xaml
    /// </summary>
    public partial class ScheduleView : UserControl
    {


        ScheduleAppointment selectedAppointment;
        String selectedAppointmentId;

        public ScheduleView()
        {
            InitializeComponent();
//            Console.Write(ScheduleAppointmentObject.ItemsSource);
            
        }

        private void SfSchedule_ScheduleClick(object sender, ScheduleClickEventArgs e)
        {
            ScheduleAppointment scheduleAppointment = ((SfSchedule)sender).SelectedAppointment;
            if (scheduleAppointment != null)
            {
                selectedAppointment = scheduleAppointment;
                selectedAppointmentId = scheduleAppointment.Notes;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddAppointment addAppointment = new AddAppointment();
            addAppointment.Closed += AddAppointment_Closed;
            addAppointment.Visibility = Visibility.Visible;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (isExamination())
            {
                foreach (ExaminationAppointment appointment in App.ExaminationAppointmentController.GetAllAppointments())
                {
                    if (appointment.Id == selectedAppointmentId)
                    {
                        EditAppointment editAppointment = new EditAppointment(appointment);
                        editAppointment.Closed += EditAppointment_Closed;
                        editAppointment.Visibility = Visibility.Visible;
                    }
                }
            }
            else
            {
                MessageBoxInformation message = new MessageBoxInformation("Error", "You didn't select examination.");
                message.Show();
            }
        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (isExamination())
            {
                for (int i = 0; i < App.ExaminationAppointmentController.GetAllAppointments().Count; i++)
                {
                    ExaminationAppointment appointment = App.ExaminationAppointmentController.GetAllAppointments().ElementAt(i);
                    if (appointment.Id == selectedAppointmentId)
                    {
                        App.ExaminationAppointmentController.DeleteExaminationAppointment(appointment);
                        ScheduleAppointmentObject.ItemsSource = new AppointmentSchedule().Appointments;
                    }
                }
            }
            else
            {
                MessageBoxInformation message = new MessageBoxInformation("Error", "You didn't select examination.");
                message.Show();
            }
        }

        private void btnAddSurgery_Click(object sender, RoutedEventArgs e)
        {
            if (LogIn.loggedInUser.TypeOfSpecialization.Name.Equals("/"))
            {
                MessageBoxInformation message = new MessageBoxInformation("Error", "Only a specialist doctor can add surgery");
                message.Show();
            }
            else
            {
                AddSurgery addSurgery = new AddSurgery();
                addSurgery.Closed += AddSurgery_Closed;
                addSurgery.Visibility = Visibility.Visible;
            }
        }

        private void btnEditSurgery_Click(object sender, RoutedEventArgs e)
        {
            if (!isExamination())
            {
                if (LogIn.loggedInUser.TypeOfSpecialization.Name.Equals("/"))
                {
                    MessageBoxInformation message = new MessageBoxInformation("Error", "Only a specialist doctor can edit surgery");
                    message.Show();
                }
                else
                {
                    foreach (Surgery surgery in App.SurgeryController.GetAllSurgeries())
                    {
                        if (surgery.Id == selectedAppointmentId)
                        {
                            EditSurgery editSurgery = new EditSurgery(surgery);
                            editSurgery.Closed += EditSurgery_Closed;
                            editSurgery.Visibility = Visibility.Visible;
                        }
                    }
                }
            }
            else 
            {
                MessageBoxInformation message = new MessageBoxInformation("Error", "You didn't select surgery.");
                message.Show();
            }
        }

        private void btnDeleteSurgery_Click(object sender, RoutedEventArgs e)
        {
            if (!isExamination())
            {
                if (LogIn.loggedInUser.TypeOfSpecialization.Name.Equals("/"))
                {
                    MessageBoxInformation message = new MessageBoxInformation("Error", "Only a specialist doctor can delete surgery");
                    message.Show();
                }
                else
                {
                    for (int i = 0; i < App.SurgeryController.GetAllSurgeries().Count; i++)
                    {
                        Surgery surgery = App.SurgeryController.GetAllSurgeries().ElementAt(i);
                        if (surgery.Id == selectedAppointmentId)
                        {
                            App.SurgeryController.DeleteSurgery(surgery);
                            ScheduleAppointmentObject.ItemsSource = new AppointmentSchedule().Appointments;
                        }
                    }
                }
            }
            else
            {
                MessageBoxInformation message = new MessageBoxInformation("Error", "You didn't select surgery.");
                message.Show();
            }
        }
        private Boolean isExamination()
        {
            foreach (ExaminationAppointment a in App.ExaminationAppointmentController.GetAllAppointments())
            {
                if (a.Id.Equals(selectedAppointmentId))
                {
                    return true;
                }
            }
            return false;
        }

        private void AddAppointment_Closed(object sender, EventArgs e)
        {
            ScheduleAppointmentObject.ItemsSource = new AppointmentSchedule().Appointments;
        }

        private void EditAppointment_Closed(object sender, EventArgs e)
        {
            ScheduleAppointmentObject.ItemsSource = new AppointmentSchedule().Appointments;
        }

        private void AddSurgery_Closed(object sender, EventArgs e)
        {
            ScheduleAppointmentObject.ItemsSource = new AppointmentSchedule().Appointments;
        }

        private void EditSurgery_Closed(object sender, EventArgs e)
        {
            ScheduleAppointmentObject.ItemsSource = new AppointmentSchedule().Appointments;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void AddCommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; ;
        }

        private void EditCommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; ;
        }

        private void DeleteCommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; ;
        }
    }
}
