﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.DoctorController
{
    public interface IPatietnFileController
    {
        Boolean CreatePatientFile(PatientFile patientFile);
        Boolean EditPatientFile(PatientFile patientFile);
        List<PatientFile> GetAllPatientFiles();
        PatientFile GetPatientFile(String id);
        Boolean CreateResultOfExamination(Model.users.doctor.ResultOfMedicalExamination resultOfExamination);
        Boolean CreateTherapy(Model.users.doctor.Therapy therapy);
        Boolean EditTherapy(Model.users.doctor.Therapy therapy);
        Boolean CreateMedicalCare(Model.users.doctor.MedicalCare medicalCare);
        Boolean EditMedicalCare(Model.users.doctor.MedicalCare medicalCare);
        Boolean DeleteMedicalCare(Model.users.doctor.MedicalCare medicalCare);
        List<MedicalCare> GetAllMedicalCares();

    }
}
