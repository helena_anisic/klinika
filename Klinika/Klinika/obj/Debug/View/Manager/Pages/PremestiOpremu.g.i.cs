﻿#pragma checksum "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D8B29A58009321147E4C08E2A5AD72F67FBC0F43"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Klinika.View.Manager.Pages;
using MahApps.Metro.IconPacks;
using MahApps.Metro.IconPacks.Converter;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Klinika.View.Manager.Pages {
    
    
    /// <summary>
    /// PremestiOpremu
    /// </summary>
    public partial class PremestiOpremu : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 229 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid help1;
        
        #line default
        #line hidden
        
        
        #line 259 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.IconPacks.PackIconMaterial upitnik;
        
        #line default
        #line hidden
        
        
        #line 279 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MesecLabel;
        
        #line default
        #line hidden
        
        
        #line 283 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Room1;
        
        #line default
        #line hidden
        
        
        #line 297 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid MedOprema;
        
        #line default
        #line hidden
        
        
        #line 422 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox All;
        
        #line default
        #line hidden
        
        
        #line 434 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock salalbl;
        
        #line default
        #line hidden
        
        
        #line 438 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Room2;
        
        #line default
        #line hidden
        
        
        #line 452 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid MedOprema1;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Klinika;component/view/manager/pages/premestiopremu.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.help1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            
            #line 257 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseEnter += new System.Windows.Input.MouseEventHandler(this.PackIconMaterial_MouseEnter);
            
            #line default
            #line hidden
            
            #line 258 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseLeave += new System.Windows.Input.MouseEventHandler(this.PackIconMaterial_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 3:
            this.upitnik = ((MahApps.Metro.IconPacks.PackIconMaterial)(target));
            return;
            case 4:
            this.MesecLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.Room1 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 291 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
            this.Room1.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Room1_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.MedOprema = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 7:
            
            #line 409 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.All = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.salalbl = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.Room2 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 446 "..\..\..\..\..\View\Manager\Pages\PremestiOpremu.xaml"
            this.Room2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Room2_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.MedOprema1 = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

