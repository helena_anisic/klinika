﻿using Klinika.Service.UsersServices.UserServices;
using Model.users.user;
using Repository.UsersRepositories.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.UsersServices.UserServices
{
    public class UserService : IUserService
    {
        private UserRepository _userRepository;
        public UserService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public Boolean CreateUser(User user)
        {
            List<User> users = _userRepository.GetAll(UserRepository.path);
            if (users == null)
            {
                users = new List<User>();
            }
            users.Add(user);
            _userRepository.Save(users, UserRepository.path);
            return true;
        }

        public Boolean EditUser(User user)
        {
            if (DeleteUser(user.Id) == false)
                return false;
            return CreateUser(user);
        }

        public Boolean DeleteUser(string id)
        {
            List<User> users = _userRepository.GetAll(UserRepository.path);

            if (users == null)
                return false;

            foreach (User user in users)
            {
                if (user.Id.Equals(id))
                {
                    users.Remove(user);
                    break;
                }
            }

            return _userRepository.Save(users, UserRepository.path);
        }


        public User GetUserByAccount(Account account)
        {
            List<User> users = _userRepository.GetAll(UserRepository.path);
            foreach(User user in users)
            {
                if(user.Username.Equals(account.Username) && user.Password.Equals(account.Password))
                {
                    return user;
                }
            }
            return null;
        }
        public List<User> GetAllUsers() => _userRepository.GetAll(UserRepository.path);
    }
}
