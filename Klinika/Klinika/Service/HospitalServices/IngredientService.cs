// File:    IngredientService.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:35:23 AM
// Purpose: Definition of Class IngredientService

using Klinika.Service.HospitalServices;
using Model.Storage;
using Repository.HospitalRepositories;
using System;
using System.Collections.Generic;

namespace Service.HospitalServices
{
    public class IngredientService : IIngredientService
    {
        private IngredientRepository _ingredientRepository;

        public IngredientService(IngredientRepository ingredientRepository)
        {
            _ingredientRepository = ingredientRepository;
        }
        public List<Ingredient> GetAllIngredients() => _ingredientRepository.GetAll(IngredientRepository.path);

    }
}