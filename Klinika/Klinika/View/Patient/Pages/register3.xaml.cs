﻿using Controller.UsersController.PatientController;
using Klinika.Controller.UsersController.UserController;
using Klinika.i18N;
using Microsoft.Win32;
using Model.users.user;
using Repository.UsersRepositories.UserRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for register3.xaml
    /// </summary>
    public partial class register3 : Page
    {
        Patient patient = null;
        public register3(Patient patient)
        {
            this.patient = patient;
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
        }

        private void Finish_Click(object sender, RoutedEventArgs e)
        {
            App.patient = patient;
            App.PatientController.CreatePatient(patient);
            Application.Current.MainWindow.Content = new Menu();
        }

        private void Previous_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new register2(patient);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                patient.ProfileImageSource = op.FileName;
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (imgPhoto.Source!=null)
            {
                imgPhoto.Source = new BitmapImage(new Uri("/Images/default.jpg", UriKind.Relative));
                patient.ProfileImageSource = null;
            }
        }
    }
}
