// File:    Term.cs
// Author:  nikol
// Created: Sunday, April 19, 2020 10:10:59 PM
// Purpose: Definition of Class Term

using Model.Hospital;
using System;

namespace Model.users.manager
{
    public class Term
    {
        public Term() {}
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public String RoomId { get; set; }

    }
}