// File:    Surgery.cs
// Author:  nikol
// Created: Monday, April 20, 2020 12:17:22 PM
// Purpose: Definition of Class Surgery

using System;

namespace Model.users.patient
{
    public class Surgery : Model.users.manager.Term
    {
      public String Id{ get; set; }
      public String Cause { get; set; }
      public Boolean Urgency { get; set; }
      public String DoctorId { get; set; }
      public String PatientId { get; set; }
      
      public Boolean CompleteSurgery()
      {
         throw new NotImplementedException();
      }

        public override string ToString()
        {
            return StartDateTime.ToString("HH:mm") + " sala " + RoomId;
        }
    }
}