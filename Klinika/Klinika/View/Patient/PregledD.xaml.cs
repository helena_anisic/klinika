﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Klinika.Pages;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Patient
{
    /// <summary>
    /// Interaction logic for PregledD.xaml
    /// </summary>
    public partial class PregledD : Window
    {
        public ExaminationAppointment examination;
        public PregledD(ExaminationAppointment examination)
        {
            this.examination = examination;
            InitializeComponent();
            datumm.Text = examination.StartDateTime.ToString("dd.MM.yyyy.");
            vreme.Text = examination.StartDateTime.ToString("HH:mm") + " - " + examination.EndDateTime.ToString("HH:mm");
            Model.users.user.Doctor dr = App.DoctorController.GetDoctorById(examination.DoctorId);
            if (dr != null)
                doktor.Text = dr.ToString();
            soba.Text = examination.RoomId;

            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Otkazivanje_MouseDown(object sender, MouseButtonEventArgs e)
        {
            App.ExaminationAppointmentController.DeleteExaminationAppointment(examination);
            this.Close();
        }

    }
}
