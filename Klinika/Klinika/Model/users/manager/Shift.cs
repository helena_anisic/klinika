// File:    Shift.cs
// Author:  nikol
// Created: Tuesday, April 21, 2020 10:43:48 AM
// Purpose: Definition of Class Shift

using System;

namespace Model.users.manager
{
    public class Shift
    {
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

    }
}