// File:    Country.cs
// Author:  nikol
// Created: Friday, April 17, 2020 6:37:26 PM
// Purpose: Definition of Class Country

using System;
using System.Collections.Generic;

namespace Model.users.user
{
    public class Country
    {
        public String Name { get; set; }
        public String Code { get; set; }

        public Country()
        {

        }

        public Country(String name, String code)
        {
            this.Name = name;
            this.Code = code;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}