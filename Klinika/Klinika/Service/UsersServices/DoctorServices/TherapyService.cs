﻿using Klinika.Repository.UsersRepositories.DoctorRepositories;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.DoctorServices
{
    public class TherapyService : ITherapyService
    {
        private TherapyRepository _therapyRepository;

        public TherapyService(TherapyRepository therapyRepository)
        {
            _therapyRepository = therapyRepository;
        }
        public Boolean CreateTherapy(Therapy therapy)
        {
            List<Therapy> therapies = _therapyRepository.GetAll(TherapyRepository.path);
            if (therapies == null)
            {
                therapies = new List<Therapy>();
            }
            therapies.Add(therapy);
            _therapyRepository.Save(therapies, TherapyRepository.path);
            return true;
        }

        public List<Therapy> GetAllTherapies()
        {
            return _therapyRepository.GetAll(TherapyRepository.path);
        }
    }
}
