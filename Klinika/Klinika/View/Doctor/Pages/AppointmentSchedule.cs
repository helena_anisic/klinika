﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Pages
{
    public class AppointmentSchedule
    {

        public List<object> Appointments { get; set; }
        public AppointmentSchedule()
        {
            Appointments = new List<object>();
            Appointments.AddRange(filterAppointmentForLoggedInUser());
            Appointments.AddRange(filterSurgeryForLoggedInUser());
            
        }

        private List<object> filterAppointmentForLoggedInUser()
        {
            List<ScheduleDisplay> filtered = new List<ScheduleDisplay>();
            foreach (ExaminationAppointment appointment in App.ExaminationAppointmentController.GetAllAppointments())
            {
                if (appointment.DoctorId.Equals(LogIn.loggedInUser.Id))
                {
                    filtered.Add(new ScheduleDisplay(appointment.Id, appointment.Cause, appointment.StartDateTime, appointment.EndDateTime));
                }
            }
            return new List<object>(filtered);
        }

        private List<object> filterSurgeryForLoggedInUser()
        {
            List<ScheduleDisplay> filtered = new List<ScheduleDisplay>();
            foreach (Surgery surgery in App.SurgeryController.GetAllSurgeries())
            {
                if (surgery.DoctorId.Equals(LogIn.loggedInUser.Id))
                {
                    filtered.Add(new ScheduleDisplay(surgery.Id, surgery.Cause, surgery.StartDateTime, surgery.EndDateTime));
                }
            }
            return new List<object>(filtered);
        }
    }
}
