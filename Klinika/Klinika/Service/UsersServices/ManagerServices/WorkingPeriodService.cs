// File:    WorkingPeriodService.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:15:17 AM
// Purpose: Definition of Class WorkingPeriodService

using Klinika.Service.UsersServices.DoctorServices;
using Klinika.Service.UsersServices.ManagerServices;
using Model.users.manager;
using Model.users.patient;
using Model.users.user;
using Repository.UsersRepositories.ManagerRepositories;
using Service.UsersServices.DoctorServices;
using Service.UsersServices.PatientServices;
using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace Service.UsersServices.ManagerServices
{
    public class WorkingPeriodService : IWorkingPeriodService
    {
        private WorkingPeriodRepository _workingPeriodRepository;
        private IDoctorService _doctorService;
        public WorkingPeriodService(WorkingPeriodRepository workingPeriodRepository, IDoctorService doctorService)
        {
            _workingPeriodRepository = workingPeriodRepository;
            _doctorService = doctorService;
        }

        public Boolean CreateWorkingPeriod(WorkingPeriod workingPeriod)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodRepository.GetAll(WorkingPeriodRepository.path);
            if (workingPeriods == null)
            {
                workingPeriods = new List<WorkingPeriod>();
            }
            workingPeriods.Add(workingPeriod);
            _workingPeriodRepository.Save(workingPeriods, WorkingPeriodRepository.path);
            return true;
        }

        public Boolean DeleteWorkingPeriod(WorkingPeriod workingPeriod)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodRepository.GetAll(WorkingPeriodRepository.path);

            if (workingPeriods == null)
                return false;

            foreach (WorkingPeriod wp in workingPeriods)
            {
                if (wp.DoctorId == workingPeriod.DoctorId && wp.Shift.StartDateTime == workingPeriod.Shift.StartDateTime && wp.Shift.EndDateTime == workingPeriod.Shift.EndDateTime)
                {
                    workingPeriods.Remove(wp);
                    break;
                }
            }

            return _workingPeriodRepository.Save(workingPeriods, WorkingPeriodRepository.path);
        }

        public List<WorkingPeriod> GetAllWorkingPeriods() => _workingPeriodRepository.GetAll(WorkingPeriodRepository.path);


        public List<WorkingPeriod> GetAllWorkingPeriodsForDay(DateTime date)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodRepository.GetAll(WorkingPeriodRepository.path);
            List<WorkingPeriod> ret = new List<WorkingPeriod>();
            foreach (WorkingPeriod wp in workingPeriods)
            {
                if (wp.Shift.StartDateTime.Date == date.Date)
                    ret.Add(wp);
            }
            return ret;
        }

        public List<WorkingPeriod> GetAllWorkingPeriods(DateTime startDate, DateTime endDate)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodRepository.GetAll(WorkingPeriodRepository.path);
            List<WorkingPeriod> ret = new List<WorkingPeriod>();
            foreach (WorkingPeriod wp in workingPeriods)
            {
                if (wp.Shift.StartDateTime.Date >= startDate.Date && wp.Shift.EndDateTime.Date <= endDate.Date)
                    ret.Add(wp);
            }
            return ret;
        }

        public List<WorkingPeriod> GetAllWorkingPeriods(DateTime startDate, DateTime endDate, String doctorId)
        {
            List<WorkingPeriod> workingPeriods = GetAllWorkingPeriodsForDoctor(doctorId);
            List<WorkingPeriod> ret = new List<WorkingPeriod>();
            foreach (WorkingPeriod wp in workingPeriods)
            {
                if (wp.Shift.StartDateTime.Date >= startDate.Date && wp.Shift.EndDateTime.Date <= endDate.Date)
                    ret.Add(wp);
            }
            return ret;
        }

        public List<WorkingPeriod> GetAllWorkingPeriodsForDoctor(String DoctorId)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodRepository.GetAll(WorkingPeriodRepository.path);
            List<WorkingPeriod> ret = new List<WorkingPeriod>();
            foreach (WorkingPeriod wp in workingPeriods)
            {
                if (wp.DoctorId.Equals(DoctorId))
                    ret.Add(wp);
            }
            return ret;
        }

        public List<WorkingPeriod> GetAllWorkingPeriodsForDoctor(String DoctorId, DateTime date)
        {
            List<WorkingPeriod> workingPeriods = GetAllWorkingPeriodsForDay(date);
            List<WorkingPeriod> ret = new List<WorkingPeriod>();
            foreach (WorkingPeriod wp in workingPeriods)
            {
                if (wp.DoctorId.Equals(DoctorId))
                    ret.Add(wp);
            }
            return ret;
        }
    }
}