﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model
{
    public class ResultOfExamination
    {
        public DateTime DateOfCreation { get; set; }
        public String Content { get; set; }

        public ResultOfExamination() { }
    }
}
