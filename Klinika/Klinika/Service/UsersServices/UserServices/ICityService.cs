﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.UserServices
{
    public interface ICityService
    {
        City GetCityByName(String cityName);
        List<City> GetAllCities();

    }
}
