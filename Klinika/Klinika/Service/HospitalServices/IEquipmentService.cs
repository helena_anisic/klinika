﻿using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.HospitalServices
{
    public interface IEquipmentService
    {
        Boolean CreateEquipment(Equipment equipment);
        List<Equipment> GetAllEquipment();
        Boolean EditEquipment(Equipment equipment);
        Boolean DeleteEquipment(Equipment equipment);
        Boolean RemoveAllEquipmentFromRoom(Room room);


    }
}
