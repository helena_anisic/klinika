// File:    DoctorRepository.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:48:59 PM
// Purpose: Definition of Class DoctorRepository

using Klinika.Repository;
using Model.users.user;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.UsersRepositories.DoctorRepositories
{
    public class DoctorRepository : Repository<Doctor>
    {
        public static String path = @"..\..\Resources\Data\Doctors.txt";

    }
}