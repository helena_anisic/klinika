﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Manager.Data
{
    class RadnoVremeLekara
    {
        public String doktor { get; set; }
        public String opis { get; set; }

        public static ObservableCollection<RadnoVremeLekara> Data(DateTime date)
        {
            ObservableCollection<RadnoVremeLekara> lista = new ObservableCollection<RadnoVremeLekara>();
            List<WorkingPeriod> workingPeriods = App.WorkingPeriodController.GetAllWorkingPeriodsForDay(date);

            foreach(WorkingPeriod wp in workingPeriods)
            {
                String startTime = (wp.Shift.StartDateTime.Hour < 10) ? "0" + wp.Shift.StartDateTime.Hour.ToString() : wp.Shift.StartDateTime.Hour.ToString();
                startTime += ":";
                startTime += (wp.Shift.StartDateTime.Minute < 10) ? "0" + wp.Shift.StartDateTime.Minute.ToString() : wp.Shift.StartDateTime.Minute.ToString();
                String endTime = (wp.Shift.EndDateTime.Hour < 10) ? "0" + wp.Shift.EndDateTime.Hour.ToString() : wp.Shift.EndDateTime.Hour.ToString();
                endTime += ":";
                endTime += (wp.Shift.EndDateTime.Minute < 10) ? "0" + wp.Shift.EndDateTime.Minute.ToString() : wp.Shift.EndDateTime.Minute.ToString();
                String time = startTime + " - " + endTime;
                Model.users.user.Doctor dr = App.DoctorController.GetDoctorById(wp.DoctorId);
                String doktorr = "###";
                if (dr != null)
                    doktorr = dr.ToString();
                lista.Add(new RadnoVremeLekara() { doktor = doktorr, opis = time + "  Sala " + wp.RoomId });
            }

            return lista;
        }
    }
}
