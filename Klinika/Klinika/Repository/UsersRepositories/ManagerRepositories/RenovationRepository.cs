// File:    RenovationRepository.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:23:06 PM
// Purpose: Definition of Class RenovationRepository

using Klinika.Repository;
using Model.users.manager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.UsersRepositories.ManagerRepositories
{
    public class RenovationRepository : Repository<Renovation>
    {
        public static String path = @"..\..\Resources\Data\Renovations.txt";

    }
}