﻿using Controller.UsersController.PatientController;
using Klinika.Controller.UsersController.UserController;
using Klinika.i18N;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Page
    {
        public Settings()
        {
            InitializeComponent();
            LocUtil.SetDefaultLanguage(this);

            foreach (MenuItem item in menuItemLanguages.Items)
            {
                if (item.Tag.ToString().Equals(LocUtil.GetCurrentCultureName(this)))
                    item.IsChecked = true;
            }

        }

        public void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (MenuItem item in menuItemLanguages.Items)
            {
                item.IsChecked = false;
            }

            MenuItem mi = sender as MenuItem;
            mi.IsChecked = true;
            LocUtil.SwitchLanguage(this, mi.Tag.ToString());


            Menu meni = new Menu();
            meni.ContentBox.Content = new Settings();
            Application.Current.MainWindow.Content = meni;

        }

        private void light_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void dark_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void Svetlo_Click(object sender, RoutedEventArgs e)
        {
            if (App.patient == null)
                return;
            App.patient.Mode = false;
            App.PatientController.EditPatient(App.patient);
            Menu meni = new Menu();
            meni.ContentBox.Content = new Settings();
            Application.Current.MainWindow.Content = meni;
        }

        private void Tamno_Click(object sender, RoutedEventArgs e)
        {
            if (App.patient == null)
                return;
            App.patient.Mode = true;
            App.PatientController.EditPatient(App.patient);
            Menu meni = new Menu();
            meni.ContentBox.Content = new Settings();
            Application.Current.MainWindow.Content = meni;
        }
    }
}
