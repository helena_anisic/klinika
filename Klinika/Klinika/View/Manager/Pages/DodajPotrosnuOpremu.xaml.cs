﻿using Klinika.Controller.HospitalController;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for DodajPotrosnuOpremu.xaml
    /// </summary>
    public partial class DodajPotrosnuOpremu : Window
    {
        public DodajPotrosnuOpremu()
        {
            InitializeComponent();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            int quant;
            if (quantity.Text.Equals(""))
                quant = 0;
            else
                quant = Int32.Parse(quantity.Text);
            EquipmentQuantity eq = new EquipmentQuantity() { equipmentB = new Equipment() { name = name.Text, id = id.Text }, quantity = quant };
            App.EquipmentController.CreateConsumable(eq);
            this.Close();
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
