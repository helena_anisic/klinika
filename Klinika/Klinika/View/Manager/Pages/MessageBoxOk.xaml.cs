﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for MessageBoxOk.xaml
    /// </summary>
    public partial class MessageBoxOk : Window
    {
        private int closeStatus;
        public MessageBoxOk(String title, String description, String buttonName)
        {
            InitializeComponent();
            Heading.Text = title;
            Description.Text = description;
            Yes.Content = buttonName;
            closeStatus = -1;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            this.closeStatus = 1;
            this.Close();
        }

        public int GetCloseStatus()
        {
            return closeStatus;
        }
    }
}
