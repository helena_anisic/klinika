﻿using Controller.UsersController.DoctorController;
using Klinika.View.Manager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for SpisakLekara.xaml
    /// </summary>
    public partial class SpisakLekara : Page
    {
        public SpisakLekara()
        {
            InitializeComponent();
            MedOprema.ItemsSource = App.DoctorController.GetAllDoctors();
        }

        private void Button_Click_Dodaj_Lekara(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new DodajLekara());
        }

        private void Button_Click_Izmeni_Lekara(object sender, RoutedEventArgs e)
        {
            Model.users.user.Doctor doctor = (Model.users.user.Doctor)MedOprema.SelectedItem;
            if (doctor == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti lekara u tabeli kojeg\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new IzmeniLekara(doctor));
        }

        private void Button_Click_Obrisi_Lekara(object sender, RoutedEventArgs e)
        {
            Model.users.user.Doctor doctor = (Model.users.user.Doctor)MedOprema.SelectedItem;
            if (doctor == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da obrisete!", "Ok");
                w.ShowDialog();
            }
            else
            {
                MessageBoxYesNo mb = new MessageBoxYesNo("Obriši opremu", "Da li želite da obrišete lekara\n" + doctor.Name + " " + doctor.Surname + "?", "Odustani", "Obriši");
                mb.ShowDialog();
                if(mb.GetCloseStatus() == 1)
                {
                    App.DoctorController.DeleteDoctor(doctor);
                    MedOprema.ItemsSource = App.DoctorController.GetAllDoctors();
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new NapraviRaspored());
        }
    }
}
