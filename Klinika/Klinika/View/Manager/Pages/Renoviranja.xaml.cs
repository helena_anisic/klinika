﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for Renoviranja.xaml
    /// </summary>
    public partial class Renoviranja : Page
    {
        public Renoviranja()
        {
            InitializeComponent();
        }

        private void Button_Click_Zakazi_Renoviranje(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new ZakaziRenoviranje());
        }

        private void Button_Click_Zakazana_Renoviranja(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new ZakazanaRenoviranja());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new PremestiOpremu());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new SpojPodeliSale());
        }
    }
}
