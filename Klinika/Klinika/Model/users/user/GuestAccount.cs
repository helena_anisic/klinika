// File:    GuestAccount.cs
// Author:  nasta
// Created: Tuesday, June 2, 2020 8:15:05 PM
// Purpose: Definition of Class GuestAccount

using System;

namespace Model.users.user
{
   public class GuestAccount
   {
      public String id;
        public String name;
        public String surname;
        public int jmbg;
        public String email;
   
   }
}