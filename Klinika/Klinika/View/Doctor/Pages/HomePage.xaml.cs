﻿using Controller.HospitalController;
using Controller.UsersController.PatientController;
using Klinika.View.Doctor.Pages.ViewModels;
using Model.Storage;
using Model.users.doctor;
using Model.users.patient;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : UserControl
    {

        public List<Model.users.user.Patient> patients;

        public HashSet<string> existingPatientIds { get; set; }

        private int newMedicinesCount;

        private static float time = 28800f;
        private DispatcherTimer Timer;

        public HomePage()
        {
            Timer = new DispatcherTimer();
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Tick += Timer_Tick;
            Timer.Start();

            InitializeComponent();
            this.DataContext = this;
            List<ExaminationAppointment> appointments = filterAppointmentForLoggedInUser();
            DateTime closestDate = new DateTime();


            foreach (Medicine medicine in App.MedicineController.GetAllMedicines())
            {
                if (!(medicine.Status == MedicineStatus.approved))
                {
                    newMedicinesCount++;
                }
            }

            NewMedicines.Text = newMedicinesCount.ToString();

            ExaminationAppointment nextAppointment = new ExaminationAppointment();
            foreach (ExaminationAppointment appointment in appointments)
            {
                if (appointments.IndexOf(appointment) == 0)
                {
                    closestDate = appointment.StartDateTime;
                    nextAppointment = appointment;
                }
                else
                {
                    if (DateTime.Compare(appointment.StartDateTime, closestDate) < 0)
                    {  // ap.StartTime ce biti pre nego closestDate
                        closestDate = appointment.StartDateTime;
                        nextAppointment = appointment;
                    }
                }
            }

            dataGridNextAppointment.Items.Add(nextAppointment);


            existingPatientIds = new HashSet<string>();
            patients = new List<Model.users.user.Patient>();
            foreach (ExaminationAppointment appointment in App.ExaminationAppointmentController.GetAllAppointments())
            {
                if (appointment.DoctorId.Equals(LogIn.loggedInUser.Id) && !existingPatientIds.Contains(appointment.PatientId))
                {
                    patients.Add(App.PatientController.GetPatientById(appointment.PatientId));
                    existingPatientIds.Add(appointment.PatientId);
                }
            }
            PatientsNumber.Text = patients.Count.ToString();

        }
        void Timer_Tick(object sender, EventArgs e)
        {

            if (time == 1)
            {
                Timer.Stop();
                MessageBox.Show("Your working hours are over!");
            }
            else
            {
                time--;
                //TBHours.Text = string.Format("00:0{0}:{1}", time/60, time%60);
                TimeSpan t = TimeSpan.FromSeconds(time);
                TBHours.Text = string.Format("{0}:{1}:{2}", ((int)t.TotalHours), t.Minutes, t.Seconds);
            }
        }

        private List<ExaminationAppointment> filterAppointmentForLoggedInUser()
        {
            List<ExaminationAppointment> filtered = new List<ExaminationAppointment>();
            foreach (ExaminationAppointment appointment in App.ExaminationAppointmentController.GetAllAppointments())
            {
                if (appointment.DoctorId.Equals(LogIn.loggedInUser.Id))
                {
                    filtered.Add(appointment);
                }
            }
            return new List<ExaminationAppointment>(filtered);
        }

        private void Feedback_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FeedbackWindow feedbackWindow = new FeedbackWindow();
            feedbackWindow.Show();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void Patients_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContext = new PatientsViewModel();
        }

        private void Medicine_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContext = new MedicineViewModel();
        }

        private void Schedule_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContext = new ScheduleViewModel();
        }

        private void Patients_MouseLeftButtonDown(object sender, DragEventArgs e)
        {
            DataContext = new PatientsViewModel();
        }

        private void Medicine_MouseLeftButtonDown(object sender, DragEventArgs e)
        {
            DataContext = new MedicineViewModel();
        }
    }
}
