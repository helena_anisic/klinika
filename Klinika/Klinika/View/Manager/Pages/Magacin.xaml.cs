﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for Magacin.xaml
    /// </summary>
    public partial class Magacin : Page
    {
        public Magacin()
        {
            InitializeComponent();
        }

        private void Button_Click_Aparati(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new MedicinskaOpremaIAparati());
        }

        private void Button_Click_Potrosna(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new PotrosnaOprema());
        }

        private void Button_Click_Lekovi(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new Lekovi());
        }

        private void Button_Click_Sve(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new MagacinSve());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
