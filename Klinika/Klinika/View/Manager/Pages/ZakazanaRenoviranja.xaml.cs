﻿using Controller.HospitalController;
using Controller.UsersController.ManagerController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for ZakazanaRenoviranja.xaml
    /// </summary>
    public partial class ZakazanaRenoviranja : Page
    {
        DateTime dt;
        public ZakazanaRenoviranja()
        {
            InitializeComponent();
            dt = DateTime.Now;
            godina.SelectedIndex = DateTime.Now.Year - 2020;
            mesec.SelectedIndex = DateTime.Now.Month - 1;
            foreach (Room r in App.RoomController.GetAllRooms())
            {
                if (!r.id.Equals("Magacin"))
                {
                    sala.Items.Add(r);
                }
            }
            sala.SelectedIndex = 0;
            MedOprema.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }
        private void Godina_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mesec.SelectedIndex == -1)
                return;
            dt = new DateTime(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1, 1);
            MedOprema.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void Mesec_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dt= new DateTime(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1, 1);
            if(sala.SelectedItem != null)
                MedOprema.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void MedOprema_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count <= 0)
                return;
            int kolona = MedOprema.SelectedCells[0].Column.DisplayIndex;
            int red = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            int index = 7 * red + kolona; ;
            DateTime date = new DateTime(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1, 1);
            int firstIndexOfCurrentMonth = (int)date.DayOfWeek;
            if (firstIndexOfCurrentMonth == 0)
                firstIndexOfCurrentMonth = 7;
            int lastIndexOfCurrentMonth = firstIndexOfCurrentMonth + DateTime.DaysInMonth(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1);
            if (index >= firstIndexOfCurrentMonth && index <= lastIndexOfCurrentMonth)
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            else
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            List<Renovation> ren = App.RenovationController.GetAllRenovations();
            foreach (Renovation r in ren)
            {
                if (r.StartDateTime.Date <= date.Date && r.EndDateTime.Date >= date.Date && r.RoomId.Equals(((Room)sala.SelectedItem).id))
                {
                    Window w = new RenoviranjeInfo(r);
                    w.ShowDialog();
                }
            }
            MedOprema.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void Mes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dt = new DateTime(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1, 1);
            MedOprema.ItemsSource = RenoviranjaCalendar.Data(dt, ((Room)sala.SelectedItem).id);
        }

        private void MedOprema_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }
    }
}
