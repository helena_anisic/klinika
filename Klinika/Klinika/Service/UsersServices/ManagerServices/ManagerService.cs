// File:    ManagerService.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:48:58 PM
// Purpose: Definition of Class ManagerService

using Klinika.Service.UsersServices.ManagerServices;
using Model.users.user;
using Repository.UsersRepositories.ManagerRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.ManagerServices
{
    public class ManagerService : IManagerService
    {
        private ManagerRepository _managerRepository;

        public ManagerService(ManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
        }
        public Boolean CreateManager(Manager manager)
        {
            List<Manager> managers = _managerRepository.GetAll(ManagerRepository.path);
            if (managers == null)
            {
                managers = new List<Manager>();
            }
            managers.Add(manager);
            _managerRepository.Save(managers, ManagerRepository.path);
            return true;
        }

        public Boolean EditManager(Manager manager)
        {
            if (DeleteManager(manager) == false)
                return false;
            return CreateManager(manager);
        }

        public Boolean DeleteManager(Manager manager)
        {
            List<Manager> managers = _managerRepository.GetAll(ManagerRepository.path);

            if (managers == null)
                return false;

            foreach (Manager m in managers)
            {
                if (m.Id.Equals(manager.Id))
                {
                    managers.Remove(m);
                    break;
                }
            }

            return _managerRepository.Save(managers, ManagerRepository.path);
        }

        public Manager GetManagerByAccount(Account account)
        {
            List<Manager> managers = _managerRepository.GetAll(ManagerRepository.path);
            if (managers == null)
                return null;
            foreach (Manager manager in managers)
            {
                if (manager.Username.Equals(account.Username) && manager.Password.Equals(account.Password))
                {
                    return manager;
                }
            }
            return null;
        }

        public List<Manager> GetAllManagers()
        {
            return _managerRepository.GetAll(ManagerRepository.path);
        }
    }
}