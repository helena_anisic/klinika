// File:    EquipmentRepository.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:32:03 PM
// Purpose: Definition of Class EquipmentRepository

using Klinika.Repository;
using Model.Storage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.HospitalRepositories
{
    public class EquipmentRepository : Repository<Equipment>
    {
        public static String path = @"..\..\Resources\Data\Equipment.txt";

    }
}