﻿#pragma checksum "..\..\..\..\View\Secretary\Account.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4ED9993E320E644BCE037A8D6FF49FA6703CF9C6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Klinika.View.Secretary;
using MahApps.Metro.IconPacks;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Klinika.View.Secretary {
    
    
    /// <summary>
    /// Account
    /// </summary>
    public partial class Account : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 164 "..\..\..\..\View\Secretary\Account.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonMojNalog;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\..\View\Secretary\Account.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonPromenaLozinke;
        
        #line default
        #line hidden
        
        
        #line 166 "..\..\..\..\View\Secretary\Account.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonKontakti;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\..\View\Secretary\Account.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonPomoc;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\..\View\Secretary\Account.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonOdjava;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Klinika;component/view/secretary/account.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\Secretary\Account.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 152 "..\..\..\..\View\Secretary\Account.xaml"
            ((MahApps.Metro.IconPacks.PackIconMaterial)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.PackIconMaterial_MouseDown_1);
            
            #line default
            #line hidden
            return;
            case 2:
            this.buttonMojNalog = ((System.Windows.Controls.Button)(target));
            
            #line 164 "..\..\..\..\View\Secretary\Account.xaml"
            this.buttonMojNalog.Click += new System.Windows.RoutedEventHandler(this.ButtonMojNalog_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.buttonPromenaLozinke = ((System.Windows.Controls.Button)(target));
            
            #line 165 "..\..\..\..\View\Secretary\Account.xaml"
            this.buttonPromenaLozinke.Click += new System.Windows.RoutedEventHandler(this.ButtonPromenaLozinke_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.buttonKontakti = ((System.Windows.Controls.Button)(target));
            
            #line 166 "..\..\..\..\View\Secretary\Account.xaml"
            this.buttonKontakti.Click += new System.Windows.RoutedEventHandler(this.ButtonKontakti_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.buttonPomoc = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.buttonOdjava = ((System.Windows.Controls.Button)(target));
            
            #line 168 "..\..\..\..\View\Secretary\Account.xaml"
            this.buttonOdjava.Click += new System.Windows.RoutedEventHandler(this.ButtonOdjava_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

