﻿using Controller.BlogFeedbackController;
using Klinika.Pages;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Patient.Pages
{
    /// <summary>
    /// Interaction logic for Anketa.xaml
    /// </summary>
    public partial class Anketa : Page
    {
        public Anketa()
        {
            InitializeComponent();
        }

        private void TabelaAnketa_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            int p1 = zvezdice.Value;
            int p2 = zvezdice1.Value;
            int p3 = zvezdice2.Value;
            int p4 = zvezdice3.Value;
            int p5 = zvezdice4.Value;
            Question q1 = new Question()
            {
                QuestionText = prvi.Text,
                QuestionGrade = p1
            };
            Question q2 = new Question()
            {
                QuestionText = drugi.Text,
                QuestionGrade = p2
            };
            Question q3 = new Question()
            {
                QuestionText = treci.Text,
                QuestionGrade = p3
            };
            Question q4 = new Question()
            {
                QuestionText = cetvrti.Text,
                QuestionGrade = p4
            };
            Question q5 = new Question()
            {
                QuestionText = peti.Text,
                QuestionGrade = p5
            };
            List<Question> pitanja = new List<Question>();
            pitanja.Add(q1);
            pitanja.Add(q2);
            pitanja.Add(q3);
            pitanja.Add(q4);
            pitanja.Add(q5);
            Survey survey = new Survey()
            {
                PublishingDate = DateTime.Now,
                IdOfRespondent = App.patient.Id,
                Questions = pitanja
            };
            App.SurveyController.CreateSurvey(survey);
            Klinika.Pages.Menu meni = new Klinika.Pages.Menu();
            meni.ContentBox.Content = new ONama();
            Application.Current.MainWindow.Content = meni;
        }

        private void Odbaci_Click_1(object sender, RoutedEventArgs e)
        {
            Klinika.Pages.Menu meni = new Klinika.Pages.Menu();
            meni.ContentBox.Content = new ONama();
            Application.Current.MainWindow.Content = meni;
        }
    }
}
