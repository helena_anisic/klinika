// File:    TypesOfSpecialization.cs
// Author:  Admin
// Created: Monday, April 20, 2020 5:17:37 PM
// Purpose: Definition of Class TypesOfSpecialization

using System;

namespace Model.users.doctor
{
   public class TypeOfSpecialization
   {
      public String Id { get; set; }
      public String Name { get; set; }

        public TypeOfSpecialization(String id, String name)
        {
            Id = id;
            Name = name;
        }

    }
}