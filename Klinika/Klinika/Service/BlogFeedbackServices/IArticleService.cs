﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.BlogFeedbackServices
{
    public interface IArticleService
    {
        Boolean CreateBlogArticle(BlogArticle blogArticle);
        BlogArticle GetBlogArticleById(String Id);
        List<BlogArticle> GetAllBlogArticles();
    }
}
