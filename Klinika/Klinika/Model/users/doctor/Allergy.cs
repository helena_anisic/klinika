// File:    Allergy.cs
// Author:  Admin
// Created: Monday, April 20, 2020 5:12:00 PM
// Purpose: Definition of Class Allergy

using System;

namespace Model.users.doctor
{
   public class Allergy
   {
      public String Id { get; set; }
      public String Name { get; set; }

        public Allergy(String id, String name)
        {
            Id = id;
            Name = name;
        }

    }
}