﻿using Controller.UsersController.SecretaryController;
using Controller.UsersController.UserController;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for mojNalog.xaml
    /// </summary>
    public partial class mojNalog : Page
    {
        public mojNalog()
           
        {
            InitializeComponent();
            List<Country> countries = App.LocationController.GetAllCountries();
            Grad.Text = App.secretary.City.Name;
            foreach (Country c in countries)
            {
                Drzava.Items.Add(c.Name);
            }
            if (App.secretary.City != null)
                Grad.Text = App.secretary.City.Name;
            if (App.secretary.Country != null)
            {
                Drzava.Text = App.secretary.Country.Name;
            }

            Ime.Text = App.secretary.Name;
            Prezime.Text = App.secretary.Surname;
            JMBG.Text = App.secretary.Id;
            
            BrojTelefona.Text = App.secretary.PhoneNumber;
            datum.Text = App.secretary.DateOfBirth.ToString("dd.MM.yyyy.");
            Adresa.Text = App.secretary.Address;
           
            
            email.Text = App.secretary.Email;

        }
        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<City> cities = App.LocationController.GetAllCitiesByCountryName(Drzava.SelectedValue.ToString());
            Grad.Items.Clear();
            foreach (City c in cities)
            {
                Grad.Items.Add(c.Name);
            }

        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            App.secretary.Name = Ime.Text;
            App.secretary.Surname = Prezime.Text;
            App.secretary.Id = JMBG.Text;
            App.secretary.PhoneNumber = BrojTelefona.Text;
            App.secretary.DateOfBirth = datum.SelectedDate.Value;
            App.secretary.Address = Adresa.Text;
            App.secretary.Email = email.Text;
            if (Drzava.SelectedValue != null)
            {
                App.secretary.Country = App.LocationController.GetCountryByName(Drzava.SelectedValue.ToString());
            }
            if (Grad.SelectedValue != null)
            {
                App.secretary.City = App.LocationController.GetCityByName(Grad.SelectedValue.ToString());
            }
            App.SecretaryController.EditSecretary(App.secretary);


            Application.Current.MainWindow.Content = new Pocetna();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Ime.IsEnabled = true;
            Prezime.IsEnabled = true;
            JMBG.IsEnabled = true;
            datum.IsEnabled = true;
            BrojTelefona.IsEnabled = true;
            Grad.IsEnabled = true;
            Drzava.IsEnabled = true;
            Adresa.IsEnabled = true;
            email.IsEnabled = true;

        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Account();
        }
    }
}
