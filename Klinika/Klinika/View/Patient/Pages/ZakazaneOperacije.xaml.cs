﻿using Klinika.i18N;
using Klinika.View.Patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for ZakazaneOperacije.xaml
    /// </summary>
    public partial class ZakazaneOperacije : Page
    {
        public ZakazaneOperacije()
        {
            InitializeComponent();
            godina.SelectedIndex = DateTime.Now.Year - 2020;
            mesec.SelectedIndex = DateTime.Now.Month - 1;
            MedOprema.ItemsSource = PreglediKalendar1.Data(DateTime.Now);
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
        }

        private void MedOprema_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count == 0)
                return;
            int kolona = MedOprema.SelectedCells[0].Column.DisplayIndex;
            int red = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            int index = 7 * red + kolona; ;
            DateTime date = new DateTime(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1, 1);
            int firstIndexOfCurrentMonth = (int)date.DayOfWeek;
            if (firstIndexOfCurrentMonth == 0)
                firstIndexOfCurrentMonth = 7;
            int lastIndexOfCurrentMonth = firstIndexOfCurrentMonth + DateTime.DaysInMonth(godina.SelectedIndex + 2020, mesec.SelectedIndex + 1);
            if (index >= firstIndexOfCurrentMonth && index <= lastIndexOfCurrentMonth)
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            else
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            Window win = new ListaTermina("o", date);
            win.ShowDialog();
        }

        private void Godina_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mesec.SelectedIndex == -1)
                return;
            DateTime date = new DateTime(Int16.Parse(godina.SelectedIndex.ToString()) + 2020, Int16.Parse(mesec.SelectedIndex.ToString()) + 1, 1);
            MedOprema.ItemsSource = PreglediKalendar1.Data(date);
        }

        private void Mesec_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime date = new DateTime(Int16.Parse(godina.SelectedIndex.ToString()) + 2020, Int16.Parse(mesec.SelectedIndex.ToString()) + 1, 1);
            MedOprema.ItemsSource = PreglediKalendar1.Data(date);
        }
    }
}
