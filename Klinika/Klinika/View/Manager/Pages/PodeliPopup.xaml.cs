﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for PodeliPopup.xaml
    /// </summary>
    public partial class PodeliPopup : Window
    {
        private int closeStatus;
        private int delova;
        public PodeliPopup(String description)
        {
            InitializeComponent();
            Description.Text = description;
            closeStatus = -1;
            delova = -1;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            this.closeStatus = 1;
            this.delova = int.Parse(brojDelova.Text);
            this.Close();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            this.closeStatus = 0;
            this.Close();
        }

        public int GetCloseStatus()
        {
            return closeStatus;
        }

        public int GetDelova()
        {
            return delova;
        }
    }
}
