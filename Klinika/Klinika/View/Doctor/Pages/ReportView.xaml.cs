﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportView : UserControl
    {
        public List<ResultOfMedicalExamination> ListResultsOfExamination { get; set; }
        public ReportView()
        {
            InitializeComponent();
            PatientsComboBox.ItemsSource = App.PatientController.GetAllPatients();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void btnShow_Click(object sender, RoutedEventArgs e)
        {
            ListResultsOfExamination = new List<ResultOfMedicalExamination>();
            Model.users.user.Patient patient = (Model.users.user.Patient)PatientsComboBox.SelectedItem;
            PatientFile patientFile = new PatientFile();

            foreach (PatientFile pF in App.PatientFileController.GetAllPatientFiles()) 
            {
                if (pF.Id.Equals(patient.Id)) 
                {
                    patientFile = pF;
                }
            }

            foreach (ResultOfMedicalExamination res in patientFile.ResultsOfMedicalExamination)
            {
                if (StartDate.SelectedDate < res.DateOfDiagnosis && res.DateOfDiagnosis < EndDate.SelectedDate)
                {
                    ListResultsOfExamination.Add(res);
                }
            }
           
            ReportList.ItemsSource = ListResultsOfExamination;
        }
    }
}
