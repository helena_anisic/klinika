﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for PremestiOpremu.xaml
    /// </summary>
    public partial class PremestiOpremu : Page
    {
        public PremestiOpremu()
        {
            InitializeComponent();
            fillForm();
        }

        private void PackIconMaterial_MouseEnter(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Visible;
        }

        private void PackIconMaterial_MouseLeave(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Hidden;
        }

        private void fillForm()
        {
            if (App.Pomoc == false)
                upitnik.Visibility = Visibility.Hidden;
            else
                upitnik.Visibility = Visibility.Visible;
            List<Room> rooms = App.RoomController.GetAllRooms();
            if (rooms == null)
                return;
            Room1.ItemsSource = rooms;
            Room2.ItemsSource = rooms;
            Room1.SelectedIndex = 0;
            Room2.SelectedIndex = 0;

            MedOprema.DataContext = new OpremaView((Room)Room1.SelectedItem);
            MedOprema1.DataContext = new OpremaView((Room)Room2.SelectedItem);
            MedOprema.HeadersVisibility = DataGridHeadersVisibility.Column;
            MedOprema1.HeadersVisibility = DataGridHeadersVisibility.Column;
        }

        private void Room1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema.DataContext = new OpremaView((Room)Room1.SelectedItem);
        }

        private void Room2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema1.DataContext = new OpremaView((Room)Room2.SelectedItem);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Room room1 = (Room)Room1.SelectedItem;
            Room room2 = (Room)Room2.SelectedItem;

            Equipment equipment1 = (Equipment)MedOprema.SelectedItem;

            if (room1 == null || room2 == null || equipment1 == null)
                return;
            if (room1.id.Equals(room2.id))
                return;
            if (room2.equipment == null)
                room2.equipment = new List<Equipment>();
            if (All.IsChecked == true)
            {
                foreach (Equipment ee in room1.equipment)
                    room2.equipment.Add(ee);
                room1.equipment = null;
                All.IsChecked = false;
            }
            else
            {
                room1.equipment.Remove(equipment1);
                room2.equipment.Add(equipment1);
            }

            App.RoomController.EditRoom(room1);
            App.RoomController.EditRoom(room2);

            MedOprema.DataContext = new OpremaView((Room)Room1.SelectedItem);
            MedOprema1.DataContext = new OpremaView((Room)Room2.SelectedItem);
        }
    }
}
