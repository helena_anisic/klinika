﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for IzmeniOpremu.xaml
    /// </summary>
    public partial class IzmeniOpremu : Window
    {
        Equipment equipment = new Equipment();
        public IzmeniOpremu(Equipment equipment)
        {
            this.equipment = equipment;
            InitializeComponent();
            id.Text = equipment.id.ToString();
            name.Text = equipment.name;
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            Room room = App.RoomController.GetRoomById("Magacin");
            foreach (Equipment ee in room.equipment)
            {
                if (ee.id.Equals(equipment.id))
                {
                    room.equipment.Remove(ee);
                    break;
                }
            }
            room.equipment.Add(new Equipment() { name = name.Text, id = id.Text });
            App.RoomController.EditRoom(room);
            this.Close();
        }
    }
}
