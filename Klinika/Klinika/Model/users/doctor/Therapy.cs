// File:    Therapy.cs
// Author:  helen
// Created: Friday, April 17, 2020 10:21:22 PM
// Purpose: Definition of Class Therapy

using Model.Storage;
using System;

namespace Model.users.doctor
{
    public class Therapy
    {
        public String Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public String Description { get; set; }
        public String WayToUseMedicine { get; set; }
        public double Quantity { get; set; }
        public String MedicineName { get; set; }

        public String MedicineManufacturer { get; set; }

    }
}