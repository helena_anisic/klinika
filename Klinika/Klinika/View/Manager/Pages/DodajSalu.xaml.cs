﻿using Controller.HospitalController;
using Model.Hospital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for DodajSalu.xaml
    /// </summary>
    public partial class DodajSalu : Window
    {
        public DodajSalu()
        {
            InitializeComponent();
            roomType.ItemsSource = App.RoomController.GetAllRoomTypes();
            roomType.SelectedIndex = 0;
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            Room room = new Room();
            room.id = id.Text;
            room.floor = int.Parse(floor.Text);
            if (roomType.SelectedIndex >= 0)
                room.roomType = (RoomType)roomType.SelectedItem;
            if (inUse.IsChecked == true)
                room.inUse = true;
            else
                room.inUse = false;
            App.RoomController.CreateRoom(room);
            this.Close();
        }
    }
}
