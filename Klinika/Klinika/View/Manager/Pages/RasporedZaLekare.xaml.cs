﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Klinika.View.Manager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for RasporedZaLekare.xaml
    /// </summary>
    public partial class RasporedZaLekare : Page
    {
        DateTime currDate = DateTime.Now;
        public RasporedZaLekare()
        {
            String date = DateTime.Now.ToString("dd.MM.yyyy");
            InitializeComponent();
            MedOprema1.ItemsSource = RadnoVremeLekara.Data(DateTime.Now);
            DataGridHeader.Header = date;

            MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, currDate, currDate);
            datumOd.SelectedDate = DateTime.Now;
            datumDo.SelectedDate = DateTime.Now;
            svi.IsChecked = true;
            lekari.ItemsSource = App.DoctorController.GetAllDoctors();
        }

        private void DatumOd_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datumDo.SelectedDate == null)
                return;
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(datumOd.SelectedDate.Value, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(datumOd.SelectedDate.Value, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void DatumDo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void prethodnaClick(object sender, RoutedEventArgs e)
        {
            currDate = currDate.AddMonths(-1);
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void sledecaClick(object sender, RoutedEventArgs e)
        {
            currDate = currDate.AddMonths(1);
            if (svi.IsChecked == true)
                MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            else
            {
                Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
                if (doctor == null)
                    return;
                MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
            }
        }

        private void Nazad_Click(object sender, RoutedEventArgs e)
        {
            MedOprema.Visibility = Visibility.Visible;
            datumOd.Visibility = Visibility.Visible;
            datumDo.Visibility = Visibility.Visible;
            odOkvir.Visibility = Visibility.Visible;
            doOkvir.Visibility = Visibility.Visible;
            GodinaLabel.Visibility = Visibility.Visible;
            MesecLabel.Visibility = Visibility.Visible;
            svi.Visibility = Visibility.Visible;
            pojedinacno.Visibility = Visibility.Visible;
            lekari.Visibility = Visibility.Visible;
            pButton.Visibility = Visibility.Visible;
            sButton.Visibility = Visibility.Visible;
            MedOprema1.Visibility = Visibility.Hidden;
            Nazad.Visibility = Visibility.Collapsed;
        }

        private void Svi_Checked(object sender, RoutedEventArgs e)
        {
            MedOprema.ItemsSource = ZauzetostSvihLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value);
            lekari.SelectedIndex = -1;
            lekari.IsEnabled = false;
        }

        private void Pojedinacno_Checked(object sender, RoutedEventArgs e)
        {
            lekari.IsEnabled = true;
            lekari.SelectedIndex = 0;
            Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
            if (doctor == null)
                return;
            MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
        }

        private void MedOprema_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void MedOprema_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count <= 0)
                return;
            int kolona = MedOprema.SelectedCells[0].Column.DisplayIndex;
            int red = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            int index = 7 * red + kolona; ;
            DateTime date = new DateTime(currDate.Year, currDate.Month, 1);
            int firstIndexOfCurrentMonth = (int)date.DayOfWeek;
            if (firstIndexOfCurrentMonth == 0)
                firstIndexOfCurrentMonth = 7;
            int lastIndexOfCurrentMonth = firstIndexOfCurrentMonth + DateTime.DaysInMonth(currDate.Year, currDate.Month);
            if (index >= firstIndexOfCurrentMonth && index <= lastIndexOfCurrentMonth)
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            else
            {
                date = date.AddDays(index - firstIndexOfCurrentMonth + 1);
            }
            MedOprema1.ItemsSource = RadnoVremeLekara.Data(date);
            String dateString = date.ToString("dd.MM.yyyy");
            DataGridHeader.Header = dateString;
            MedOprema.UnselectAllCells();
            if (svi.IsChecked == true)
            {
                MedOprema.Visibility = Visibility.Hidden;
                datumOd.Visibility = Visibility.Hidden;
                datumDo.Visibility = Visibility.Hidden;
                odOkvir.Visibility = Visibility.Hidden;
                doOkvir.Visibility = Visibility.Hidden;
                GodinaLabel.Visibility = Visibility.Hidden;
                MesecLabel.Visibility = Visibility.Hidden;
                svi.Visibility = Visibility.Hidden;
                pojedinacno.Visibility = Visibility.Hidden;
                lekari.Visibility = Visibility.Hidden;
                pButton.Visibility = Visibility.Hidden;
                sButton.Visibility = Visibility.Hidden;
                MedOprema1.Visibility = Visibility.Visible;
                Nazad.Visibility = Visibility.Visible;
            }
        }

        private void Lekari_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.users.user.Doctor doctor = (Model.users.user.Doctor)lekari.SelectedItem;
            if (doctor == null)
                return;
            MedOprema.ItemsSource = ZauzetostJednogLekara.Data(currDate, datumOd.SelectedDate.Value, datumDo.SelectedDate.Value, doctor);
        }
    }
}
