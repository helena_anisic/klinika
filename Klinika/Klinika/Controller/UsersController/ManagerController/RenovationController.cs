// File:    RenovationController.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:23:06 PM
// Purpose: Definition of Class RenovationController

using Klinika.Controller.UsersController.ManagerController;
using Klinika.Service.UsersServices.ManagerServices;
using Model.users.manager;
using Service.UsersServices.ManagerServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.ManagerController
{
    public class RenovationController : IRenovationController
    {
        private IRenovationService _renovationService;

        public RenovationController(IRenovationService renovationService)
        {
            _renovationService = renovationService;
        }
        public Boolean CreateRenovation(Renovation renovation)
        {
            if (renovation == null)
                return false;
            return _renovationService.CreateRenovation(renovation);
        }

        public Boolean EditRenovation(Renovation renovation)
        {
            if (renovation == null)
                return false;
            return _renovationService.EditRenovation(renovation);
        }

        public Boolean DeleteRenovation(Renovation renovation)
        {
            if (renovation == null)
                return false;
            return _renovationService.DeleteRenovation(renovation);
        }

        public List<Renovation> GetAllRenovations()
        {
            return _renovationService.GetAllRenovations();
        }
    }
}