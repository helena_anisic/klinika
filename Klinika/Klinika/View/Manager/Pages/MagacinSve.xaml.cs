﻿using Klinika.View.Manager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for MagacinSve.xaml
    /// </summary>
    public partial class MagacinSve : Page
    {
        public MagacinSve()
        {
            InitializeComponent();
            DataContext = new MedicinskaOpremaViewModel();
            MedOprema.HeadersVisibility = DataGridHeadersVisibility.Column;
        }

        private void Button_Click_Dodaj_Opremu(object sender, RoutedEventArgs e)
        {
            Window w = new DodajOpremu();
            w.ShowDialog();
        }

        private void Button_Click_Izmeni_Opremu(object sender, RoutedEventArgs e)
        {
            MedicinskaOprema item = (MedicinskaOprema)MedOprema.SelectedItem;
            Window w;
            if (item == null)
            {
                w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }
            //w = new IzmeniOpremu();
            //w.ShowDialog();
        }

        private void Button_Click_Obrisi_Opremu(object sender, RoutedEventArgs e)
        {
            MedicinskaOprema item = (MedicinskaOprema)MedOprema.SelectedItem;
            Window w;
            if (item == null)
                w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da obrisete!", "Ok");
            else
                w = new MessageBoxYesNo("Obriši opremu", "Da li želite da obrišete opremu\n" + item.Naziv + " id:" + item.ID + "?", "Odustani", "Obriši");
            w.ShowDialog();
        }
    }
}
