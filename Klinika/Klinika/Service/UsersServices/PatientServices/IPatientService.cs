﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.PatientServices
{
    public interface IPatientService
    {
        Boolean CreatePatient(Patient patient);
        Boolean EditPatient(Patient patient);
        Boolean DeletePatient(Patient patient);
        Patient GetPatientByAccount(Account account);
        Patient GetPatientById(String id);
        List<Patient> GetAllPatients();

    }
}
