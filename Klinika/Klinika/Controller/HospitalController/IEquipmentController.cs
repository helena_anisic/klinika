﻿using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.HospitalController
{
    public interface IEquipmentController
    {
        Boolean CreateEquipment(Equipment equipment);
        Boolean EditEquipment(Equipment equipment);
        Boolean DeleteEquipment(Equipment equipment);
        List<Equipment> GetAllEquipment();
        Boolean CreateConsumable(EquipmentQuantity equipment);
        Boolean EditConsumable(EquipmentQuantity equipment);
        Boolean DeleteConsumable(EquipmentQuantity equipment);
        List<EquipmentQuantity> GetAllConsumables();
        Boolean RemoveAllEquipmentFromRoom(Room room);


    }
}
