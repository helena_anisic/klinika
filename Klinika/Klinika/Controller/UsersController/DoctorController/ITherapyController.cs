﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.DoctorController
{
    public interface ITherapyController
    {
        Boolean CreateTherapy(Therapy therapy);
        List<Therapy> GetAllTherapies();
    }
}
