// File:    FeedbackService.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:07:01 AM
// Purpose: Definition of Class FeedbackService

using Klinika.Service.BlogFeedbackServices;
using Model.users.user;
using Repository.BlogFeedbackRepositories;
using System;
using System.Collections.Generic;

namespace Service.BlogFeedbackServices
{
    public class FeedbackService : IFeedbackService
    {
        private FeedbackRepository _feedbackRepository;

        public FeedbackService(FeedbackRepository feedbackRepository)
        {
            _feedbackRepository = feedbackRepository;
        }
        public Boolean CreateFeedback(Feedback feedback)
        {
            List<Feedback> feedbacks = _feedbackRepository.GetAll(FeedbackRepository.path);
            if (feedbacks == null)
            {
                feedbacks = new List<Feedback>();
            }
            feedbacks.Add(feedback);
            _feedbackRepository.Save(feedbacks, FeedbackRepository.path);
            return true;
        }
    }
}