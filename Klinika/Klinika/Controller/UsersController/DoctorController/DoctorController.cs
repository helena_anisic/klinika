// File:    DoctorController.cs
// Author:  nikol
// Created: Saturday, May 30, 2020 1:31:43 AM
// Purpose: Definition of Class DoctorController

using Klinika.Controller.UsersController.DoctorController;
using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using Model.users.user;
using Repository.UsersRepositories.DoctorRepositories;
using Service.UsersServices.DoctorServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.DoctorController
{
    public class DoctorController : IDoctorController
    {
        private IDoctorService _doctorService;
        private IAllergyService _allergyService;
        private ISpecializationService _specializationService;

        public DoctorController(IDoctorService doctorService, IAllergyService allergyService, ISpecializationService specializationService)
        {
            _doctorService = doctorService;
            _allergyService = allergyService;
            _specializationService = specializationService;
        }

        public DoctorController()
        {
        }

        public Boolean CreateDoctor(Doctor doctor)
        {
            if (doctor == null)
                return false;
            return _doctorService.CreateDoctor(doctor);
        }
        public Boolean EditDoctor(Doctor doctor)
        {
            return _doctorService.EditDoctor(doctor);
        }

        public List<Doctor> GetAllDoctors()
        {
            return _doctorService.GetAllDoctors();
        }

        public Doctor GetDoctorById(String id)
        {
            return _doctorService.GetDoctorById(id);
        }

        public Boolean CreateAllergy(Allergy allergy)
        {
            if (allergy == null)
                return false;
            return _allergyService.CreateAllergy(allergy);
        }

        public Boolean EditAllergy(Allergy allergy)
        {
            return _allergyService.EditAllergy(allergy);
        }

        public Boolean DeleteAllergy(Allergy allergy)
        {
            return _allergyService.DeleteAllergy(allergy);
        }

        public Boolean DeleteDoctor(Doctor doctor)
        {
            return _doctorService.DeleteDoctor(doctor);
        }

        public List<TypeOfSpecialization> GetAllSpecialization()
        {
            return _specializationService.GetAllSpecializations();
        }
    }
}