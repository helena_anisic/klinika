﻿using Controller.UsersController.PatientController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for PrikazTermina.xaml
    /// </summary>
    public partial class PrikazTermina : Page
    {
        public ExaminationAppointment ea = new ExaminationAppointment();
        public PrikazTermina(ExaminationAppointment ea)
        {
            InitializeComponent();
            this.ea = ea;
            //String id = Pacijent.Text;
            //Model.users.user.Patient patient = new Model.users.user.Patient();
            //PatientController pc = new PatientController();
            //patient = pc.GetPatientById(id);
            //Pacijent.Text = patient.Name;
            Pacijent.Text = ea.PatientId;
            Doktor.Text = ea.DoctorId;
            Soba.Text = ea.RoomId;
            start.Text = ea.StartDateTime.ToString("dd.MM.yyyy. HH:mm");
            end.Text = ea.EndDateTime.ToString("dd.MM.yyyy. HH:mm");

        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }
        private void PackIconMaterial_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            //ExaminationAppointment examinationAppointment = (ExaminationAppointment) 
            //Application.Current.MainWindow.Content = new Meni();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Pacijent.IsEnabled = true;
            Doktor.IsEnabled = true;
            Soba.IsEnabled = true;
            start.IsEnabled = true;
            end.IsEnabled = true;

        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {

            //ea.PatientId = Pacijent.Text;
            //ea.DoctorId = Doktor.Text;
            //ea.RoomId = Soba.Text;
            //ea.StartDateTime = (DateTime) start.Text.ToString()
        }
    }
}
