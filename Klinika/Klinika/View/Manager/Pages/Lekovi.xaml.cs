﻿using Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for Lekovi.xaml
    /// </summary>
    public partial class Lekovi : Page
    {
        public Lekovi()
        {
            InitializeComponent();
            MedOprema.ItemsSource = App.MedicineController.GetAllMedicines();
            MedOprema.HeadersVisibility = DataGridHeadersVisibility.Column;
        }

        private void Button_Click_Dodaj_Lek(object sender, RoutedEventArgs e)
        {
            HomePage hp = new HomePage();
            hp.setContentBox(new DodajLek());
            Application.Current.MainWindow.Content = hp;
        }
        private void Button_Click_Izmeni_Lek(object sender, RoutedEventArgs e)
        {
            Medicine medicine = (Medicine)MedOprema.SelectedItem;
            if (medicine == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti lek u tabeli koji\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new IzmeniLek(medicine));
        }

        private void Button_Click_Obrisi_Lek(object sender, RoutedEventArgs e)
        {
            Medicine medicine = (Medicine)MedOprema.SelectedItem;
            if (medicine == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti lek u tabeli koji\nzelite da obrisete!", "Ok");
                w.ShowDialog();
            }
            else
            {
                MessageBoxYesNo mb = new MessageBoxYesNo("Obriši lek", "Da li želite da obrišete lek\n" + medicine.Name + " id:" + medicine.Id + "?", "Odustani", "Obriši");
                mb.ShowDialog();
                if (mb.GetCloseStatus() == 1)
                {
                    App.MedicineController.DeleteMedicine(medicine);
                    MedOprema.ItemsSource = App.MedicineController.GetAllMedicines();
                }

            }
        }
        private void Button_Click_Premesti_Opremu(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new PremestiOpremu());
        }
    }
}
