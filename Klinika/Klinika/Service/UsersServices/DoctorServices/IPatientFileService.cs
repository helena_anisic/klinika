﻿using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.DoctorServices
{
    public interface IPatientFileService
    {
        Boolean CreatePatientFile(PatientFile patientFile);
        Boolean EditPatientFile(PatientFile patientFile);
        Boolean DeletePatientFile(PatientFile patientFile);
        List<PatientFile> GetAllPatientFiles();
        Boolean CreateResultOfExamination(Model.users.doctor.ResultOfMedicalExamination resultOfExamination);
        Boolean CreateTherapy(Model.users.doctor.Therapy therapy);
        Boolean EditTherapy(Model.users.doctor.Therapy therapy);
        Boolean DeleteTherapy(Model.users.doctor.Therapy therapy);
        Boolean CreateMedicalCares(Model.users.doctor.MedicalCare medicalCare);
        Boolean EditMedicalCare(Model.users.doctor.MedicalCare medicalCare);
        Boolean DeleteMedicalCare(Model.users.doctor.MedicalCare medicalCare);
        List<MedicalCare> GetAllMedicalCares();
        List<ResultOfMedicalExamination> GetAllResultOfMedicalExamination();
        PatientFile GetPatientFile(String id);

    }
}
