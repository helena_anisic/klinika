﻿using Klinika.i18N;
using Klinika.Pages;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Patient
{
    /// <summary>
    /// Interaction logic for TerapijaD.xaml
    /// </summary>
    public partial class TerapijaD : Window
    {
        private Therapy therapy;

        public TerapijaD(Therapy therapy)
        {
            this.therapy = therapy;
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            wayToUse.Text = therapy.WayToUseMedicine;
            description.Text = therapy.Description;
            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
