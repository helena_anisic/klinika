// File:    City.cs
// Author:  nikol
// Created: Friday, April 17, 2020 6:37:25 PM
// Purpose: Definition of Class City

using System;

namespace Model.users.user
{
    public class City
    {
        public String Name { get; set; }
        public int PostalCode { get; set; }

        public String CountryCode { get; set; }

        public City()
        {

        }

        public City(String name, int postalCode, String countryCode)
        {
            this.Name = name;
            this.PostalCode = postalCode;
            this.CountryCode = countryCode;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}