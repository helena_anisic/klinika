// File:    GuestService.cs
// Author:  nikol
// Created: Tuesday, May 26, 2020 9:27:59 PM
// Purpose: Definition of Class GuestService

using Model.users.user;
using System;
using Repository.UsersRepositories.SecretaryRepositories;

using System.Collections.Generic;
using Klinika.Service.UsersServices.SecretaryServices;

namespace Service.UsersServices.SecretaryServices
{
    public class GuestService : IGuestService

    {
        private GuestRepository _guestRepository;

        public GuestService(GuestRepository guestRepository)
        {
            _guestRepository = guestRepository;
        }
        public Boolean CreateGuestAccount(GuestAccount guestAccount)
        {
            List<GuestAccount> guest = _guestRepository.GetAll(GuestRepository.path);
            if (guest == null)
            {
                guest = new List<GuestAccount>();
            }
            guest.Add(guestAccount);
            _guestRepository.Save(guest, GuestRepository.path);
            return true;
        }


        public Boolean DeleteGuestAccount(GuestAccount guestAccount)
        {
            List<GuestAccount> guests = _guestRepository.GetAll(GuestRepository.path);

            if (guests == null)
                return false;

            foreach (GuestAccount g in guests)
            {
                if (g.id.Equals(guestAccount.id))
                {
                    guests.Remove(g);
                    break;
                }
            }

            return _guestRepository.Save(guests, GuestRepository.path);



        }
        public List<GuestAccount> GetAllGuestss() => _guestRepository.GetAll(GuestRepository.path);
    }
}