﻿using Controller.UsersController.ManagerController;
using Model.users.manager;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Manager.Data
{
    class RenoviranjaCalendar
    {
        public static List<Renovation> renovations;
        public struct Podaci
        {
            public int Dan { get; set; }
            public String Foreground { get; set; }
            public String Opis { get; set; }
            public String Background { get; set; }
        }
        public Podaci Ponedeljak { get; set; }
        public Podaci Utorak { get; set; }
        public Podaci Sreda { get; set; }
        public Podaci Cetvrtak { get; set; }
        public Podaci Petak { get; set; }
        public Podaci Subota { get; set; }
        public Podaci Nedelja { get; set; }
        public static String roomId { get; set; }


        public static ObservableCollection<RenoviranjaCalendar> Data(DateTime date, String roomID)
        {
            roomId = roomID;
            renovations = App.RenovationController.GetAllRenovations();
            ObservableCollection<RenoviranjaCalendar> lista = new ObservableCollection<RenoviranjaCalendar>();
            DateTime firstDateOfMonth = new DateTime(date.Year, date.Month, 1);
            int firstDayOfMonth = (int)firstDateOfMonth.DayOfWeek;
            if (firstDayOfMonth == 0)
                firstDayOfMonth = 7;
            List<DateTime> dani = new List<DateTime>();
            for (int i = firstDayOfMonth - 1; i > 0; i--)
                dani.Add(firstDateOfMonth.AddDays(-i));
            for (int i = 0; i < DateTime.DaysInMonth(date.Year, date.Month); i++)
                dani.Add(firstDateOfMonth.AddDays(i));
            for (int i = dani.Count; i < 42; i++)
                dani.Add(dani[i - 1].AddDays(1));
            for (int i = 0; i < 6; i++)
            {
                lista.Add(new RenoviranjaCalendar
                {
                    Ponedeljak = CellContent(dani[0 + i * 7], (dani[0 + i * 7].Month == date.Month) ? true : false),
                    Utorak = CellContent(dani[1 + i * 7], (dani[1 + i * 7].Month == date.Month) ? true : false),
                    Sreda = CellContent(dani[2 + i * 7], (dani[2 + i * 7].Month == date.Month) ? true : false),
                    Cetvrtak = CellContent(dani[3 + i * 7], (dani[3 + i * 7].Month == date.Month) ? true : false),
                    Petak = CellContent(dani[4 + i * 7], (dani[4 + i * 7].Month == date.Month) ? true : false),
                    Subota = CellContent(dani[5 + i * 7], (dani[5 + i * 7].Month == date.Month) ? true : false),
                    Nedelja = CellContent(dani[6 + i * 7], (dani[6 + i * 7].Month == date.Month) ? true : false),
                });
            }
            return lista;
        }

        public static Podaci CellContent(DateTime date, Boolean thisMonth)
        {
            String BgColor = (thisMonth) ? "" : "Silver";
            Tuple<String, String> tuple = GetRenovationOnDate(date);
            if (tuple == null)
                return new Podaci { Dan = date.Day, Foreground = BgColor };
            return new Podaci { Dan = date.Day, Foreground = "white", Opis = tuple.Item1, Background = tuple.Item2 };
        }

        public static Tuple<String, String> GetRenovationOnDate(DateTime date)
        {
            String color = "#00a65a"; //#f39c12
            foreach (Renovation renovation in renovations)
            {
                if (date.Date >= renovation.StartDateTime.Date && date.Date <= renovation.EndDateTime.Date && renovation.RoomId.Equals(roomId))
                {

                    return Tuple.Create("Ren", color);
                }
            }
            return null;
        }
    }
}
