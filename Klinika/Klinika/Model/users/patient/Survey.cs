// File:    Survey.cs
// Author:  helen
// Created: Sunday, April 19, 2020 7:22:04 PM
// Purpose: Definition of Class Survey

using System;
using System.Collections.Generic;

namespace Model.users.patient
{
   public class Survey
   {
      public DateTime PublishingDate { get; set; }
      public String IdOfRespondent { get; set; }
      
      public List<Question> Questions;
   
   }
}