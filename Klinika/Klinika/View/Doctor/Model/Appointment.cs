﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model
{
    [Serializable]
    public class Appointment
    {
        public Appointment()
        {
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string Room { get; set; }
        public string AppointmentKind { get; set; }

        public PatientClass Patient { get; set; }
        public User Doctor { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

    }
}
