﻿using Controller.UsersController.PatientController;
using Controller.UsersController.UserController;
using Klinika.Controller.UsersController.UserController;
using Klinika.i18N;
using Klinika.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika
{
    /// <summary>
    /// Interaction logic for PromeniLozinku.xaml
    /// </summary>
    public partial class PromeniLozinku : Window
    {
        public PromeniLozinku()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }
        }

        private void ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            // chechk old password...
            if(oldPassword.Password == App.patient.Password && newPassword.Password.Length > 8 && confirmNewPassword.Password == newPassword.Password)
            {
                App.patient.Password = newPassword.Password.ToString();
                App.PatientController.EditPatient(App.patient);
                this.Close();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void oldPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (oldPassword.Password != App.patient.Password)
            {
                oldPassword.BorderBrush = Brushes.Red;
                oldPasswordMessage.Visibility = Visibility.Visible;
            }
            else
            {
                oldPassword.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                oldPasswordMessage.Visibility = Visibility.Hidden;
            }
        }

        private void newPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (newPassword.Password.Length < 8)
            {
                newPassword.BorderBrush = Brushes.Red;
                newPasswordMessage.Visibility = Visibility.Visible;
            }
            else
            {
                newPassword.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                newPasswordMessage.Visibility = Visibility.Hidden;
            }
        }

        private void confirmNewPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (confirmNewPassword.Password != newPassword.Password)
            {
                confirmNewPassword.BorderBrush = Brushes.Red;
                confirmNewPasswordMessage.Visibility = Visibility.Visible;
            }
            else
            {
                confirmNewPassword.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                confirmNewPasswordMessage.Visibility = Visibility.Hidden;
            }
        }
    }
}
