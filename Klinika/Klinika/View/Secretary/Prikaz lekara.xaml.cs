﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for Prikaz_lekara.xaml
    /// </summary>
    public partial class Prikaz_lekara : Page
    {
        public Prikaz_lekara(Model.users.user.Doctor d)
        {
            
            
            InitializeComponent();
            Ime.Text = (String)d.Name;
            Prezime.Text = (String)d.Surname;
            JMBG.Text = (String)d.Id;
            BrojTelefona.Text = (String)d.PhoneNumber;
            Grad.Text = d.City.Name;
            Adresa.Text = (String)d.Address;
            datum.Text = d.DateOfBirth.ToString("dd.MM.yyyy.");
            Drzava.Text = d.Country.Name;
            email.Text = (String)d.Email;
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }

        private void BtnPrikazi_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new TerminiLekar();
        }
    }
}
