﻿using Model.Hospital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.HospitalServices
{
    public interface IRoomTypeService
    {
        Boolean CreateRoomType(RoomType roomType);
        Boolean DeleteRoomType(RoomType roomType);
        List<RoomType> GetAllRoomTypes();

    }
}
