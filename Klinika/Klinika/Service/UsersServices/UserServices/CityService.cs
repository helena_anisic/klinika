// File:    CityService.cs
// Author:  nikol
// Created: Friday, May 29, 2020 11:29:08 AM
// Purpose: Definition of Class CityService

using Klinika.Service.UsersServices.UserServices;
using Model.Hospital;
using Model.users.user;
using Repository.UsersRepositories.UserRepository;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.UserServices
{
    public class CityService : ICityService
    {
        private CityRepository _cityRepository;

        public CityService(CityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }
        public City GetCityByName(String cityName)
        {
            List<City> cities = _cityRepository.GetAll(CityRepository.path);
            if (cities == null)
                return null;
            foreach (City city in cities)
            {
                if (city.Name.Equals(cityName))
                {
                    return city;
                }
            }
            return null;
        }

        public List<City> GetAllCities() => _cityRepository.GetAll(CityRepository.path);
    }
}