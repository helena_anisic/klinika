﻿using Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for SpojPodeliSale.xaml
    /// </summary>
    public partial class SpojPodeliSale : Page
    {
        public SpojPodeliSale()
        {
            InitializeComponent();
            if (App.Pomoc == false)
                upitnik.Visibility = Visibility.Hidden;
            else
                upitnik.Visibility = Visibility.Visible;
            MedOprema.ItemsSource = App.RoomController.GetAllRooms();
        }

        private void spojSale(object sender, RoutedEventArgs e)
        {
            List<Room> rooms = MedOprema.SelectedItems.Cast<Room>().ToList();
            if (rooms.Count == 0)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti sale u tabeli koje\nzelite da spojite!", "Ok");
                w.ShowDialog();
            }
            else if (rooms.Count == 1)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti barem dve sale\n u tabeli koje zelite da spojite!", "Ok");
                w.ShowDialog();
            }
            else
            {
                Room newRoom = new Room();
                MessageBoxYesNo mb = new MessageBoxYesNo("Spoj sale", "Da li zelite da spojite obelezene sale?", "Ne", "Da");
                mb.ShowDialog();
                if (mb.GetCloseStatus() == 1)
                {
                    List<Equipment> equipment = new List<Equipment>();
                    foreach (Room r in rooms)
                    {
                        newRoom.id = r.id;
                        newRoom.inUse = false;
                        newRoom.roomType = new RoomType() { type = "Nedefinisano" };
                        newRoom.floor = r.floor;
                        if (r.equipment != null)
                        {
                            foreach (Equipment ee in r.equipment)
                            {
                                equipment.Add(ee);
                            }
                        }
                        App.RoomController.DeleteRoom(r);
                    }
                    newRoom.equipment = equipment;
                    App.RoomController.CreateRoom(newRoom);
                    MedOprema.ItemsSource = App.RoomController.GetAllRooms();
                }
            }
        }

        private void podeliSale(object sender, RoutedEventArgs e)
        {
            Room room = (Room)MedOprema.SelectedItem;
            if (room == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti salu u tabeli koju\nzelite da podelite!", "Ok");
                w.ShowDialog();
            }
            else
            {
                PodeliPopup pp = new PodeliPopup("Da li zelite da podelite salu " + room.id + " na");
                pp.ShowDialog();
                if (pp.GetCloseStatus() == 1)
                {
                    int delova = pp.GetDelova();
                    App.RoomController.DeleteRoom(room);

                    for(int i = 1; i <= delova; i++)
                    {
                        Room r = new Room();
                        r.id = room.id + "/" + i.ToString();
                        r.floor = room.floor;
                        r.inUse = false;
                        r.roomType = new RoomType() { type = "Nedefinisano" };
                        if (i == 1)
                            r.equipment = room.equipment;
                        App.RoomController.CreateRoom(r);
                    }

                    MedOprema.ItemsSource = App.RoomController.GetAllRooms();
                }
            }
        }

        private void PackIconMaterial_MouseEnter(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Visible;
        }

        private void PackIconMaterial_MouseLeave(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Hidden;
        }
    }
}
