﻿using Model.users.patient;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.PatientController
{
    public interface IExaminationAppointmentController
    {
        Boolean CreateExaminationAppointment(ExaminationAppointment examinationAppointment);
        Boolean EditExaminationAppointment(ExaminationAppointment examinationAppointment);
        Boolean DeleteExaminationAppointment(ExaminationAppointment examinationAppointment);
        List<ExaminationAppointment> GetAllAppointments();
        List<ExaminationAppointment> GetAllAppointmentsForDoctor(String doctorId);
        List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId);
        List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId, DateTime date);
        Doctor GetFreeDoctorForTerm(DateTime startDate, DateTime endDate);
        ExaminationAppointment GetFreeAppointment(DateTime startDate, DateTime endDate);
        ExaminationAppointment GetFreeAppointment(DateTime startDate, DateTime endDate, String doctorId);

    }
}
