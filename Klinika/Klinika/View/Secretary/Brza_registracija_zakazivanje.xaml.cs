﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for Brza_registracija_zakazivanje.xaml
    /// </summary>
    public partial class Brza_registracija_zakazivanje : Page
    {
        public Brza_registracija_zakazivanje()
        {
            InitializeComponent();
        }
        private void BtnRegistruj_Click(object sender, RoutedEventArgs e)
        {
            //Model.users.user.Patient p = new Model.users.user.Patient();
            //Application.Current.MainWindow.Content = new Zakazivanje_termina(p);
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new SearchPatient();
        }
    }
}
