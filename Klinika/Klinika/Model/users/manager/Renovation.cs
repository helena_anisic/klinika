// File:    Renovation.cs
// Author:  nikol
// Created: Saturday, April 18, 2020 6:44:28 PM
// Purpose: Definition of Class Renovation

using Model.users.user;
using System;

namespace Model.users.manager
{
    public class Renovation : Term
    {
        public String Id { get; set; }
        public String Description { get; set; }

    }
}