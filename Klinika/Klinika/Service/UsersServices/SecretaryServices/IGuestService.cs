﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.SecretaryServices
{
    public interface IGuestService
    {
        Boolean CreateGuestAccount(GuestAccount guestAccount);
        Boolean DeleteGuestAccount(GuestAccount guestAccount);
        List<GuestAccount> GetAllGuestss();

    }
}
