// File:    LocationController.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 12:46:00 PM
// Purpose: Definition of Class LocationController

using Klinika.Controller.UsersController.UserController;
using Klinika.Service.UsersServices.UserServices;
using Model.Hospital;
using Model.users.user;
using Service.UsersServices.UserServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.UserController
{
    public class LocationController : ILocationController
    {
        private ICityService _cityService;
        private ICountryService _countryService;

        public LocationController(ICityService cityService, ICountryService countryService)
        {
            _cityService = cityService;
            _countryService = countryService;
        }

        public LocationController()
        {
        }

        public List<City> GetAllCitiesByCountryName(String countryName)
        {
            List<City> cities = _cityService.GetAllCities();
            Country country = _countryService.GetCountryByName(countryName);
            if (cities == null || country == null)
                return null;

            List<City> citiesOfCountry = new List<City>();
            foreach(City city in cities)
            {
                if (city.CountryCode.Equals(country.Code))
                    citiesOfCountry.Add(city);
            }

            return citiesOfCountry;
        }

        public Country GetCountryByName(String countryName) => _countryService.GetCountryByName(countryName);

        public City GetCityByName(String cityName) => _cityService.GetCityByName(cityName);

        public List<Country> GetAllCountries() => _countryService.GetAllCountries();
    }
}