﻿using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.DoctorController
{
    public interface ISurgeryController
    {
        Boolean CreateSurgery(Surgery surgery);
        Boolean EditSurgery(Surgery surgery);
        Boolean DeleteSurgery(Surgery surgery);
        List<Surgery> GetAllSurgeries();
        List<Surgery> GetAllSurgeriesForDoctor(String doctorId);
        List<Surgery> GetAllSurgeriesForPatient(String patientId);
        List<Surgery> GetAllSurgeriesForPatient(String patientId, DateTime date);

    }
}
