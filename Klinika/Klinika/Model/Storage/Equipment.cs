// File:    Equipment.cs
// Author:  nikol
// Created: Saturday, April 18, 2020 6:37:07 PM
// Purpose: Definition of Class Equipment

using System;

namespace Model.Storage
{
    public class Equipment
    {
        public String id { get; set; }
        public String name { get; set; }

    }
}