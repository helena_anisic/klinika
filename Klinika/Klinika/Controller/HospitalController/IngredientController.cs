﻿using Klinika.Service.HospitalServices;
using Model.Storage;
using Service.HospitalServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.HospitalController
{
    public class IngredientController : IIngredientController
    {
        public IIngredientService _ingredientService;

        public IngredientController(IIngredientService ingredientService)
        {
            _ingredientService = ingredientService;
        }
        public List<Ingredient> GetAllIngredients() => _ingredientService.GetAllIngredients();
    }
}
