﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.users.user;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller.UsersController.PatientController;
using Controller.UsersController.UserController;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for RegistracijaPacijenta.xaml
    /// </summary>
    public partial class RegistracijaPacijenta : Page
    {
        Model.users.user.Patient patient = new Model.users.user.Patient();

        public RegistracijaPacijenta()
        {
            InitializeComponent();
            List<Country> countries = App.LocationController.GetAllCountries();
            foreach (Country c in countries)
            {
                Drzava.Items.Add(c.Name);
            }
            if (patient.City != null)
                Grad.Text = patient.City.Name;
            if (patient.Country != null)
            {
                Drzava.Text = patient.Country.Name;
            }
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }
        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<City> cities = App.LocationController.GetAllCitiesByCountryName(Drzava.SelectedValue.ToString());
            Grad.Items.Clear();
            foreach (City c in cities)
            {
                Grad.Items.Add(c.Name);
            }

        }
        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            patient.Name = Ime.Text;
            patient.Surname = Prezime.Text;
            patient.PhoneNumber = BrojTelefona.Text;
            patient.Id = JMBG.Text;
            patient.DateOfBirth = Datum.SelectedDate.Value;
            App.PatientController.CreatePatient(patient);
            patient.Address = Adresa.Text;
            patient.Email = email.Text;
            patient.Password = password.Password.ToString();
            if (Drzava.SelectedValue != null)
            {
                patient.Country = App.LocationController.GetCountryByName(Drzava.SelectedValue.ToString());
            }
            if (Grad.SelectedValue != null)
            {
                patient.City = App.LocationController.GetCityByName(Grad.SelectedValue.ToString());
            }

            App.PatientController.CreatePatient(patient);
            
            Application.Current.MainWindow.Content = new Meni();
        }
    }
}
