﻿using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.HospitalController
{
    public interface IMedicineController
    {
        Boolean CreateMedicine(Medicine medicine);
        List<MedicineType> GetAllMedicinesTypes();
        List<Medicine> GetAllMedicines();
        Boolean EditMedicine(Medicine medicine);
        Boolean DeleteMedicine(Medicine medicine);

    }
}
