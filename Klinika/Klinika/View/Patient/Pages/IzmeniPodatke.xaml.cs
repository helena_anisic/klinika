﻿using Controller.UsersController.PatientController;
using Controller.UsersController.UserController;
using Klinika.Controller.UsersController.UserController;
using Klinika.i18N;
using Microsoft.Win32;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for IzmeniPodatke.xaml
    /// </summary>
    public partial class IzmeniPodatke : Page
    {
        public IzmeniPodatke()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            fillForm();
        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {

            if (name.Text.Length != 0 && surname.Text.Length != 0 && dateOfBirth.SelectedDate != null && id.Text.Length == 13 && address.Text.Length != 0 && email.Text.Contains("@"))
            {
                changeUserData();
                App.PatientController.EditPatient(App.patient);

                Menu meni = new Menu();
                meni.ContentBox.Content = new Podaci();
                Application.Current.MainWindow.Content = meni;
            }
            else
            {
                if (name.Text.Length == 0)
                {
                    name.BorderBrush = Brushes.Red;
                    nameMessage.Visibility = Visibility.Visible;
                }
                if (surname.Text.Length == 0)
                {
                    surname.BorderBrush = Brushes.Red;
                    surnameMessage.Visibility = Visibility.Visible;
                }
                if (id.Text.Length != 13)
                {
                    id.BorderBrush = Brushes.Red;
                    idMessage.Visibility = Visibility.Visible;
                }
                if (dateOfBirth.SelectedDate == null)
                {
                    borderKalendar.BorderBrush = Brushes.Red;
                    kalendarMessage.Visibility = Visibility.Visible;
                }
                if (address.Text.Length == 0)
                {
                    address.BorderBrush = Brushes.Red;
                    addressMessage.Visibility = Visibility.Visible;
                }
                if (email.Text.Length == 0)
                {
                    email.BorderBrush = Brushes.Red;
                    emailMessage.Visibility = Visibility.Visible;
                }
            }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            Menu meni = new Menu();
            meni.ContentBox.Content = new Podaci();
            Application.Current.MainWindow.Content = meni;

        }

        private void AddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                App.patient.ProfileImageSource = op.FileName;
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
            }
            App.PatientController.EditPatient(App.patient);
            Menu meni = new Menu();
            meni.ContentBox.Content = new Podaci();
            Application.Current.MainWindow.Content = meni;
        }

        private void DeleteImage_Click(object sender, RoutedEventArgs e)
        {
            if (App.patient.ProfileImageSource != null)
            {
                imgPhoto.Source = new BitmapImage(new Uri("/View/Patient/Images/default.jpg", UriKind.Relative));
                App.patient.ProfileImageSource = null;
                App.PatientController.EditPatient(App.patient);
            }
            Menu meni = new Menu();
            meni.ContentBox.Content = new Podaci();
            Application.Current.MainWindow.Content = meni;
        }

        private void fillForm()
        {
            User user = App.patient;
            name.Text = user.Name;
            surname.Text = user.Surname;
            if (user.DateOfBirth != null)
                dateOfBirth.SelectedDate = user.DateOfBirth;
            else
                dateOfBirth.Text = "empty";
            id.Text = user.Id;
            if (user.Gender == Gender.male)
                male.IsChecked = true;
            else
                female.IsChecked = true;
            List<Country> countries = App.LocationController.GetAllCountries();
            foreach (Country c in countries)
            {
                country.Items.Add(c.Name);
            }
            if (user.Country != null)
            {
                country.SelectedItem = user.Country.Name;
                List<City> cities = App.LocationController.GetAllCitiesByCountryName(country.SelectedValue.ToString());
                city.Items.Clear();
                foreach (City c in cities)
                {
                    city.Items.Add(c.Name);
                }
                if (user.City != null)
                    city.SelectedValue = user.City.Name;
            }
            address.Text = user.Address;
            phone.Text = user.PhoneNumber;
            email.Text = user.Email;
            if (user.Doctor != null)
            {
                doctor.Text = "dr " + user.Doctor.Name + " " + user.Doctor.Surname;
            }
            if (user.ProfileImageSource != null)
                imgPhoto.Source = new BitmapImage(new Uri(user.ProfileImageSource));
        }

        public void changeUserData()
        {
            User user = App.patient;
            user.Name = name.Text;
            user.Surname = surname.Text;
            if (dateOfBirth.SelectedDate != null)
                user.DateOfBirth = dateOfBirth.SelectedDate.Value;
            user.Id = id.Text;
            if (male.IsChecked == true)
                user.Gender = Gender.male;
            else
                user.Gender = Gender.female;
            if (country.SelectedValue != null)
                user.Country = App.LocationController.GetCountryByName(country.SelectedValue.ToString());
            if (city.SelectedValue != null)
                user.City = App.LocationController.GetCityByName(city.SelectedValue.ToString());
            user.Address = address.Text;
            user.PhoneNumber = phone.Text;
            user.Email = email.Text;
        }

        private void name_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (name.Text.Length == 0)
            {
                name.BorderBrush = Brushes.Red;
                nameMessage.Visibility = Visibility.Visible;
            }
            else
            {
                if (nameMessage != null)
                {
                    name.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                    nameMessage.Visibility = Visibility.Hidden;
                }

            }
        }

        private void surname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (surname.Text.Length == 0)
            {
                surname.BorderBrush = Brushes.Red;
                surnameMessage.Visibility = Visibility.Visible;
            }
            else
            {
                if (surnameMessage != null)
                {
                    surname.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                    surnameMessage.Visibility = Visibility.Hidden;
                }

            }
        }

        private void dateOfBirth_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dateOfBirth.SelectedDate == null)
            {
                borderKalendar.BorderBrush = Brushes.Red;
                kalendarMessage.Visibility = Visibility.Visible;
            }
            else
            {
                if (kalendarMessage != null)
                {
                    borderKalendar.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                    kalendarMessage.Visibility = Visibility.Hidden;
                }

            }
        }

        private void id_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (idMessage != null)
            {
                if (id.Text.Length != 13)
                {
                    id.BorderBrush = Brushes.Red;
                    idMessage.Visibility = Visibility.Visible;
                }
                else
                {

                    id.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                    idMessage.Visibility = Visibility.Hidden;
                }

            }
        }

        private void address_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (address.Text.Length == 0)
            {
                address.BorderBrush = Brushes.Red;
                addressMessage.Visibility = Visibility.Visible;
            }
            else
            {
                if (addressMessage != null)
                {
                    address.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                    addressMessage.Visibility = Visibility.Hidden;
                }

            }
        }

        private void email_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!email.Text.Contains("@"))
            {
                email.BorderBrush = Brushes.Red;
                emailMessage.Visibility = Visibility.Visible;
            }
            else
            {
                if (emailMessage != null)
                {
                    email.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#296c92"));
                    emailMessage.Visibility = Visibility.Hidden;
                }

            }
        }
    }
}
