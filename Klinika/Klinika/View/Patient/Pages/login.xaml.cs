﻿using Klinika.i18N;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Repository.UsersRepositories.UserRepository;
using Model.users.user;
using Controller.UsersController.UserController;
using Klinika.Controller.UsersController.UserController;
using Controller.UsersController.PatientController;
using Model.users.doctor;
using Controller.HospitalController;
using Klinika.Controller.UsersController.DoctorController;
using Controller.BlogFeedbackController;
using Controller.UsersController.DoctorController;
using Model.users.patient;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public partial class login : Page
    {
        public login()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));;
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = App.PatientController.GetPatientByAccount(new Account(username.Text, password.Password.ToString()));
            if (patient != null)
            {
                App.patient = patient;;
                Application.Current.MainWindow.Content = new Menu();
            }
            else
            {
                usernameBorder.Background = Brushes.Red;
                Ciko.Foreground = Brushes.Red;
                passwordBorder.Background = Brushes.Red;
                pass.Foreground = Brushes.Red;
                invalidLoginMessage.Visibility = Visibility.Visible;
            }
            
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = new Patient();
            Application.Current.MainWindow.Content = new register(patient);
        }

        bool username_hasBeenClicked = false;
        private void username_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!username_hasBeenClicked)
            {
                TextBox box = sender as TextBox;
                box.Text = String.Empty;
                username_hasBeenClicked = true;
            }
        }

        bool password_hasBeenClicked = false;
        private void password_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!password_hasBeenClicked)
            {
                PasswordBox box = sender as PasswordBox;
                box.Password = String.Empty;
                password_hasBeenClicked = true;
            }
        }

        private void email_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (usernameBorder != null)
            {
                if (pass != null)
                {
                    passwordBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    pass.Foreground = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    invalidLoginMessage.Visibility = Visibility.Hidden;
                }
                if (!username.Text.Contains("@"))
                {
                    if (username_hasBeenClicked == false)
                        return;
                    usernameBorder.Background = Brushes.Red;
                    Ciko.Foreground = Brushes.Red;
                    usernameMessage.Visibility = Visibility.Visible;
                }
                else
                {
                    usernameBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    Ciko.Foreground = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    usernameMessage.Visibility = Visibility.Hidden;
                    invalidLoginMessage.Visibility = Visibility.Hidden;
                    if (pass != null)
                    {
                        passwordBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                        pass.Foreground = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                        invalidLoginMessage.Visibility = Visibility.Hidden;

                    }
                }
            }

        }

        private void password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (pass != null)
            {
                passwordBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                pass.Foreground = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                invalidLoginMessage.Visibility = Visibility.Hidden;
                if (username.Text.Contains("@"))
                {
                    usernameBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    Ciko.Foreground = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                    usernameMessage.Visibility = Visibility.Hidden;
                    invalidLoginMessage.Visibility = Visibility.Hidden;
                }
            }

        }
    }
}
