﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Manager.Data
{
    class SaleData
    {
        public string Naziv { get; set; }
        public int Sprat { get; set; }
        public string Namena { get; set; }
        public string Upotrebljiva { get; set; }

        public static ObservableCollection<SaleData> Data()
        {
            ObservableCollection<SaleData> lista = new ObservableCollection<SaleData>();
            lista.Add(new SaleData
            {
                Naziv = "S101",
                Sprat = 1,
                Namena = "Prijemna sala",
                Upotrebljiva = "Van upotrebe"
            });
            lista.Add(new SaleData
            {
                Naziv = "S102",
                Sprat = 1,
                Namena = "Operaciona sala",
                Upotrebljiva = "U upotrebi"
            });
            lista.Add(new SaleData
            {
                Naziv = "S202",
                Sprat = 2,
                Namena = "Prijemna sala",
                Upotrebljiva = "U upotrebi"
            });
            return lista;
        }
    }
}
