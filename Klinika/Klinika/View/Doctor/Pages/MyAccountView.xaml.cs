﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller.UsersController.DoctorController;
using Klinika.View.Secretary;
using Microsoft.Win32;
using Model.users.doctor;
using Model.users.user;
using Xamarin.Forms.Internals;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for MyAccountView.xaml
    /// </summary>
    public partial class MyAccountView : UserControl
    {
        private Model.users.user.Doctor doctor;
        private Random random = new Random();
        public MyAccountView()
        {
            InitializeComponent();
            doctor = LogIn.loggedInUser;
            ID.Text = doctor.Id;
            Name.Text = doctor.Name + " " + doctor.Surname;
            DateOfBirth.SelectedDate = doctor.DateOfBirth;
            ResidentialAddress.Text = doctor.Address;
            MobilePhone.Text = doctor.PhoneNumber;
            Email.Text = doctor.Email;
            City.Text = doctor.City.Name;
            Country.Text = doctor.Country.Name;
            Username.Text = doctor.Username;
            Password.Password = doctor.Password;
            specialization.Text = doctor.TypeOfSpecialization.Name;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void btnAddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Select image";
            fileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (.jpg;.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (.png)|.png";
            if (fileDialog.ShowDialog() == true)
            {
                App.doctor.ProfileImageSource = fileDialog.FileName;
                photo.Source = new BitmapImage(new Uri(fileDialog.FileName));
            }
            App.DoctorController.EditDoctor(App.doctor);

            MainWindow mainWindow = new MainWindow();
            DataContext = new MyAccountView();
        }

        private void btnDeleteImage_Click(object sender, RoutedEventArgs e)
        {
            if (App.doctor.ProfileImageSource != null)
            {
                photo.Source = new BitmapImage(new Uri("/View/Doctor/Images/doctor.png", UriKind.Relative));
                App.doctor.ProfileImageSource = null;
                App.DoctorController.EditDoctor(App.doctor);
            }
            MainWindow mainWindow = new MainWindow();
            DataContext = new MyAccountView();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Model.users.user.Doctor UpdateDoctor = new Model.users.user.Doctor();
            UpdateDoctor.Id = doctor.Id;
            UpdateDoctor.Name = Name.Text;
            UpdateDoctor.DateOfBirth = (DateTime)DateOfBirth.SelectedDate;
            UpdateDoctor.Address = ResidentialAddress.Text;
            UpdateDoctor.PhoneNumber = MobilePhone.Text;
            UpdateDoctor.Email = Email.Text;
            UpdateDoctor.City = new City(City.Text,random.Next(), random.Next().ToString());
            UpdateDoctor.Country = new Country(Country.Text, random.NextDouble().ToString());
            UpdateDoctor.Username = Username.Text;
            UpdateDoctor.Password = Password.Password;
            UpdateDoctor.TypeOfSpecialization = new TypeOfSpecialization(random.Next().ToString(), specialization.Text);

            for (int i = 0; i < App.DoctorController.GetAllDoctors().Count; i++)
            {
                if (App.DoctorController.GetAllDoctors().ElementAt(i).Id.Equals(UpdateDoctor.Id))
                {
                    App.DoctorController.EditDoctor(UpdateDoctor);
                }
            }
            doctor = UpdateDoctor;
            LogIn.loggedInUser = doctor;

            MainWindow mainWindow = new MainWindow();
            DataContext = new MyAccountView();
        }
    }
}
