﻿
using Klinika.i18N;
using Klinika.Service.UsersServices.UserServices;
using Model.users.doctor;
using Repository.BlogFeedbackRepositories;
using Service.BlogFeedbackServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Blog.xaml
    /// </summary>
    public partial class Blog : Page
    {
        public Model.users.doctor.BlogArticle blog = new Model.users.doctor.BlogArticle();
        
        public Blog()
        {
            InitializeComponent();
            TabelaBlog.ItemsSource = App.ArticleController.GetAllBlogArticles();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {

            Menu meni = new Menu();
            meni.ContentBox.Content = new BlogArticle(blog);
            Application.Current.MainWindow.Content = meni;
        }

        private void blog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void blog_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {

        }

        private void TabelaBlog_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            blog = (e.OriginalSource as FrameworkElement).DataContext as Model.users.doctor.BlogArticle;

        }
    }
}
