﻿using Klinika.Service.HospitalServices;
using Model.Hospital;
using Model.Storage;
using Service.HospitalServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.HospitalController
{
    public class EquipmentController : IEquipmentController
    {
        private IEquipmentService _equipmentService;
        private IConsumablesService _consumablesService;

        public EquipmentController(IEquipmentService equipmentService, IConsumablesService consumablesService)
        {
            _equipmentService = equipmentService;
            _consumablesService = consumablesService;
        }
        public Boolean CreateEquipment(Equipment equipment)
        {
            if (equipment == null)
                return false;
            return _equipmentService.CreateEquipment(equipment);
        }

        public Boolean EditEquipment(Equipment equipment)
        {
            if (equipment == null)
                return false;
            return _equipmentService.EditEquipment(equipment);
        }

        public Boolean DeleteEquipment(Equipment equipment)
        {
            if (equipment == null)
                return false;
            return _equipmentService.DeleteEquipment(equipment);
        }

        public List<Equipment> GetAllEquipment() => _equipmentService.GetAllEquipment();


        public Boolean CreateConsumable(EquipmentQuantity equipment)
        {
            if (equipment == null)
                return false;
            return _consumablesService.CreateConsumable(equipment);
        }

        public Boolean EditConsumable(EquipmentQuantity equipment)
        {
            if (equipment == null)
                return false;
            return _consumablesService.EditConsumable(equipment);
        }

        public Boolean DeleteConsumable(EquipmentQuantity equipment)
        {
            if (equipment == null)
                return false;
            return _consumablesService.DeleteConsumable(equipment);
        }

        public List<EquipmentQuantity> GetAllConsumables() => _consumablesService.GetAllConsumables();


        public Boolean RemoveAllEquipmentFromRoom(Room room) => _equipmentService.RemoveAllEquipmentFromRoom(room);

    }
}
