﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for PatientFileView.xaml
    /// </summary>
    public partial class PatientFileView : Window
    {
        private Model.users.user.Patient patientLocal;
        private Random random = new Random();

        public PatientFileView(Model.users.user.Patient patient, PatientFile patientFile)
        {
            InitializeComponent();
            patientLocal = patient;
            PatientName.Text = patientFile.PatientId;
            patientFileId.Text = patientFile.Id;
            anamnesis.Text = patientFile.Anamnesis;
            vaccubation.Text = patientFile.Vaccubation;
            phoneOfFamily.Text = patientFile.PhoneOfFamily;

            if (patientFile.Allergy != null)
            {
                foreach (Allergy allergy in patientFile.Allergy)
                {
                    AllergiesList.Items.Add(allergy.Name);
                }
            }
        }

        private void btnMedicalCare_Click(object sender, RoutedEventArgs e)
        {
            foreach (PatientFile patientFile in App.PatientFileController.GetAllPatientFiles())
            {
                if (patientFile.Id.Equals(patientLocal.Id))
                {
                    MedicalCareView medicalCareView = new MedicalCareView(patientFile, patientLocal);
                    medicalCareView.Show();
                }
            }
        }

        private void btnResult_Click(object sender, RoutedEventArgs e)
        {
            ResultOfExaminationView resultOfExamination = new ResultOfExaminationView(patientLocal);
            resultOfExamination.Show();
        }

        private void Button_SaveClick(object sender, RoutedEventArgs e)
        {
            foreach (PatientFile patientFile in App.PatientFileController.GetAllPatientFiles())
            {
                if (patientFile.Id.Equals(patientLocal.Id))
                {
                    patientFile.Anamnesis = anamnesis.Text;
                    patientFile.Vaccubation = vaccubation.Text;
                    patientFile.PhoneOfFamily = phoneOfFamily.Text;
                    patientFile.Allergy = new List<Allergy>();
                    foreach (var item in AllergiesList.Items)
                    {
                        patientFile.Allergy.Add(new Allergy(random.Next().ToString(), item.ToString()));
                    }
                    App.PatientFileController.EditPatientFile(patientFile);
                }
            }
            Close();
        }

        private void btnAddAllergy_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(allergy.Text))
                return;
            AllergiesList.Items.Add(allergy.Text);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
