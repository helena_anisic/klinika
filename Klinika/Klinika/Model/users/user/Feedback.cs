// File:    Feedback.cs
// Author:  nikol
// Created: Friday, April 17, 2020 9:32:01 PM
// Purpose: Definition of Class Feedback

using System;

namespace Model.users.user
{
    public class Feedback
    {
        public int Grade { get; set; }
        public String Comment { get; set; }
        public DateTime DateOfPublishing { get; set; }

    }
}