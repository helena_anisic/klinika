﻿using Controller.UsersController.ManagerController;
using Controller.UsersController.UserController;
using Microsoft.Win32;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for MojNalogIzmena.xaml
    /// </summary>
    public partial class MojNalogIzmena : Page
    {
        public MojNalogIzmena()
        {
            InitializeComponent();

            if (App.manager != null)
            {
                name.Text = App.manager.Name;
                surname.Text = App.manager.Surname;
                dateOfBirth.SelectedDate = App.manager.DateOfBirth;
                adress.Text = App.manager.Address;
                country.ItemsSource = App.LocationController.GetAllCountries();
                if (App.manager.Country != null)
                {
                    int i = 0;
                    foreach (Country c in country.Items)
                    {
                        if (c.Code.Equals(App.manager.Country.Code))
                        {
                            country.SelectedIndex = i;
                        }
                        i++;
                    }
                    city.ItemsSource = App.LocationController.GetAllCitiesByCountryName(App.manager.Country.Name);
                    if (App.manager.City != null)
                    {
                        i = 0;
                        foreach (City c in city.Items)
                        {
                            if (c.PostalCode == App.manager.City.PostalCode)
                            {
                                city.SelectedIndex = i;
                            }
                            i++;
                        }
                    }
                }
                phone.Text = App.manager.PhoneNumber;
                email.Text = App.manager.Email;

            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            bool valid = true;
            if (name.Text.Equals(""))
            {
                nameBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                nameBorder.BorderBrush = Brushes.Silver;
            }
            if (phone.Text.Equals(""))
            {
                phoneBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                phoneBorder.BorderBrush = Brushes.Silver;
            }
            if (email.Text.Equals(""))
            {
                emailBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                emailBorder.BorderBrush = Brushes.Silver;
            }
            if (valid)
            {
                logErr.Visibility = Visibility.Hidden;

                App.manager.Name = name.Text;
                App.manager.Surname = surname.Text;
                App.manager.DateOfBirth = dateOfBirth.SelectedDate.Value;
                App.manager.Address = adress.Text;
                App.manager.Country = (Country)country.SelectedItem;
                App.manager.City = (City)city.SelectedItem;
                App.manager.PhoneNumber = phone.Text;
                App.manager.Email = email.Text;
                App.manager.Username = email.Text;

                App.ManagerController.EditManager(App.manager);

                HomePage hp = (HomePage)Application.Current.MainWindow.Content;
                hp.setContentBox(new MojNalog());
            }
            else
            {
                logErr.Visibility = Visibility.Visible;
            }

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Window w = new IzmeniLozinku();
            w.ShowDialog();
        }

        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Address_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Phone_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Email_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            city.ItemsSource = App.LocationController.GetAllCitiesByCountryName(((Country)country.SelectedItem).Name);
        }
    }
}
