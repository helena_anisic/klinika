// File:    ExaminationAppointmentService.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:31:59 PM
// Purpose: Definition of Class ExaminationAppointmentService

using Klinika.Service.UsersServices.DoctorServices;
using Klinika.Service.UsersServices.ManagerServices;
using Klinika.Service.UsersServices.PatientServices;
using Model.users.manager;
using Model.users.patient;
using Model.users.user;
using Repository.UsersRepositories.PatientRepositories;
using Service.UsersServices.DoctorServices;
using Service.UsersServices.ManagerServices;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.PatientServices
{
    public class ExaminationAppointmentService : IExaminationAppointmentService
    
    {
        private ExaminationAppointmentRepository _examinationAppointmentRepository;
        private IWorkingPeriodService _workingPeriodService;
        private IDoctorService _doctorService;

        public ExaminationAppointmentService(ExaminationAppointmentRepository examinationAppointmentRepository, IWorkingPeriodService workingPeriodService, IDoctorService doctorService)
        {
            _examinationAppointmentRepository = examinationAppointmentRepository;
            _workingPeriodService = workingPeriodService;
            _doctorService = doctorService;
        }
        

        
        public Boolean CreateExaminationAppointment(ExaminationAppointment examinationAppointment)
        {
            List<ExaminationAppointment> examinationAppointments = _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);
            if (examinationAppointments == null)
                examinationAppointments = new List<ExaminationAppointment>();
            examinationAppointments.Add(examinationAppointment);
            return _examinationAppointmentRepository.Save(examinationAppointments, ExaminationAppointmentRepository.path);
        }

        public Boolean DeleteExaminationAppointment(Model.users.patient.ExaminationAppointment examinationAppointment)
        {
            List<ExaminationAppointment> examinationAppointments = _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);
            foreach (ExaminationAppointment e in examinationAppointments)
                if (e.Id.Equals(examinationAppointment.Id))
                {
                    examinationAppointments.Remove(e);
                    break;
                }
            return _examinationAppointmentRepository.Save(examinationAppointments, ExaminationAppointmentRepository.path);
        }

        public List<ExaminationAppointment> GetAllAppointments() => _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);

        public Boolean EditExaminationAppointment(ExaminationAppointment examinationAppointment)
        {
            if (DeleteExaminationAppointment(examinationAppointment) == false)
                return false;
            return CreateExaminationAppointment(examinationAppointment);
        }


        public List<ExaminationAppointment> GetAllAppointmentsForDoctor(String doctorId)
        {
            List<ExaminationAppointment> examinationAppointments = _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);
            List<ExaminationAppointment> retList = new List<ExaminationAppointment>();
            foreach (ExaminationAppointment e in examinationAppointments)
                if (e.DoctorId.Equals(doctorId))
                    retList.Add(e);
            return retList;
        }

        public List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId)
        {
            List<ExaminationAppointment> examinationAppointments = _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);
            List<ExaminationAppointment> ret = new List<ExaminationAppointment>();
            foreach (ExaminationAppointment e in examinationAppointments)
                if (e.PatientId.Equals(patientId))
                    ret.Add(e);
            return ret;
        }

        public List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId, DateTime date)
        {
            List<ExaminationAppointment> examinationAppointments = GetAllAppointmentsForPatient(patientId);
            List<ExaminationAppointment> ret = new List<ExaminationAppointment>();
            foreach (ExaminationAppointment e in examinationAppointments)
                if (e.StartDateTime.Date == date.Date)
                    ret.Add(e);
            return ret;
        }

        public List<ExaminationAppointment> GetAllAppointmentsForDay(DateTime date)
        {
            List<ExaminationAppointment> examinationAppointments = _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);
            List<ExaminationAppointment> ret = _examinationAppointmentRepository.GetAll(ExaminationAppointmentRepository.path);
            foreach (ExaminationAppointment e in examinationAppointments)
                if (e.StartDateTime.Date == date.Date)
                    ret.Add(e);
            return ret;
        }

        public Doctor GetFreeDoctorForTerm(DateTime startDateTime, DateTime endDateTime)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodService.GetAllWorkingPeriodsForDay(startDateTime);
            foreach (WorkingPeriod wp in workingPeriods)
                if (isTermFree(wp.DoctorId, startDateTime))
                    return _doctorService.GetDoctorById(wp.DoctorId);
            return null;
        }

        public Boolean isTermFree(String doctorId, DateTime startDateTime)
        {
            List<ExaminationAppointment> examinations = GetAllAppointmentsForDoctor(doctorId);
            foreach (ExaminationAppointment e in examinations)
                if (e.StartDateTime == startDateTime)
                    return false;
            return true;
        }
        public List<ExaminationAppointment> GetAllAppointmentsForPeriod(DateTime startDate, DateTime endDate)
        {
            List<ExaminationAppointment> examinations = GetAllAppointments();
            List<ExaminationAppointment> ret = new List<ExaminationAppointment>();
            foreach (ExaminationAppointment ee in examinations)
                if (ee.StartDateTime.Date >= startDate.Date && ee.EndDateTime.Date <= endDate.Date)
                    ret.Add(ee);
            return ret;
        }

        public List<ExaminationAppointment> GetAllAppointmentsForPeriod(DateTime startDate, DateTime endDate, String doctorId)
        {
            List<ExaminationAppointment> examinations = GetAllAppointments();
            List<ExaminationAppointment> ret = new List<ExaminationAppointment>();
            foreach (ExaminationAppointment ee in examinations)
                if (ee.StartDateTime.Date >= startDate.Date && ee.EndDateTime.Date <= endDate.Date && ee.DoctorId.Equals(doctorId))
                    ret.Add(ee);
            return ret;
        }

        public ExaminationAppointment GetFreeAppointment(DateTime startDateTime, DateTime endDateTime)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodService.GetAllWorkingPeriods(startDateTime, endDateTime);
            foreach (WorkingPeriod wp in workingPeriods)
            {
                DateTime term = wp.Shift.StartDateTime;
                while (term < wp.Shift.EndDateTime)
                {
                    if (isTermFree(wp.DoctorId, term))
                        return new ExaminationAppointment() { DoctorId = wp.DoctorId, StartDateTime = term, EndDateTime = term.AddMinutes(30), RoomId = wp.RoomId };
                    term = term.AddMinutes(30);
                }
            }
            return null;
        }

        public ExaminationAppointment GetFreeAppointment(DateTime startDateTime, DateTime endDateTime, String doctorId)
        {
            List<WorkingPeriod> workingPeriods = _workingPeriodService.GetAllWorkingPeriods(startDateTime, endDateTime, doctorId);
            foreach (WorkingPeriod wp in workingPeriods)
            {
                DateTime term = wp.Shift.StartDateTime;
                while (term < wp.Shift.EndDateTime)
                {
                    if (isTermFree(wp.DoctorId, term))
                        return new ExaminationAppointment() { DoctorId = wp.DoctorId, StartDateTime = term, EndDateTime = term.AddMinutes(30), RoomId = wp.RoomId };
                    term = term.AddMinutes(30);
                }
            }
            return null;
        }
    }
}