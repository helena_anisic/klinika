// File:    Ingredient.cs
// Author:  helen
// Created: Friday, April 17, 2020 10:46:46 PM
// Purpose: Definition of Class Ingredient

using System;
using System.Collections.Generic;

namespace Model.Storage
{
    public class Ingredient
    {
        public String name { get; set; }
        public Boolean IsAllergen { get; set; }

        public override string ToString()
        {
            return name + (IsAllergen == true ? "  Alergen" : "");
        }
    }
}