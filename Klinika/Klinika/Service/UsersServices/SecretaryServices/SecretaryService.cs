// File:    SecretaryService.cs
// Author:  nikol
// Created: Friday, May 29, 2020 4:49:01 PM
// Purpose: Definition of Class SecretaryService

using Klinika.Service.UsersServices.SecretaryServices;
using Model.users.user;
using Repository.UsersRepositories.SecretaryRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.SecretaryServices
{
   public class SecretaryService : ISecretaryService
   {
        private SecretaryRepository _secretaryRepository;

        public SecretaryService(SecretaryRepository secretaryRepository)
        {
            _secretaryRepository = secretaryRepository;
        }
        public List<Secretary> GetAllSecretaries() => _secretaryRepository.GetAll(SecretaryRepository.path);
      
      public Boolean EditSecretary(Secretary secretary)
      {
            if (DeleteSecretary(secretary) == false)
                return false;
            return CreateSecretary(secretary);
        }
      
      public Boolean CreateSecretary(Secretary secretary)
      {
            List<Secretary> secretaries = _secretaryRepository.GetAll(SecretaryRepository.path);
            if (secretaries == null)
            {
                secretaries = new List<Secretary>();
            }
            secretaries.Add(secretary);
            _secretaryRepository.Save(secretaries, SecretaryRepository.path);
            return true;
        }
      
      public Boolean DeleteSecretary(Secretary secretary)
      {
            List<Secretary> secretaries = _secretaryRepository.GetAll(SecretaryRepository.path);

            if (secretaries == null)
                return false;

            foreach (Secretary s in secretaries)
            {
                if (s.Id.Equals(secretary.Id))
                {
                    secretaries.Remove(s);
                    break;
                }
            }

            return _secretaryRepository.Save(secretaries, SecretaryRepository.path);
        }
        

        public Secretary GetSecretaryByAccount(Account account)
      {
            List<Secretary> secretaries = _secretaryRepository.GetAll(SecretaryRepository.path);
            if (secretaries == null)
                return null;
            foreach (Secretary secretary in secretaries)
            {
                if (secretary.Username.Equals(account.Username) && secretary.Password.Equals(account.Password))
                {
                    return secretary;
                }
            }
            return null;
        }
      
     
   
   }
}