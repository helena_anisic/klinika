﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for Meni.xaml
    /// </summary>
    public partial class Meni : Page
    {
        public Meni()
        {
            InitializeComponent();
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Pocetna();
        }

        private void ButtonRegistracijaPacijenata_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new RegistracijaPacijenta();
        }

        private void ButtonBrzaRegistracija_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new BrzaRegistracija();

        }

        private void ButtonRegistrovaniPacijenti_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new PretragaPacijenata();
        }

        private void ButtonLekari_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new pretragaLekari();
        }

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ButtonSale_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Sale();
        }

        private void ButtonZakazivanjeTermina_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new SearchPatient();
        }

        private void ButtonUpravljanjeRasporedom_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new UpravljanjeRasporedom();
        }
    }
}
