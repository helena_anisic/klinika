﻿using Controller.BlogFeedbackController;
using Klinika.i18N;
using Klinika.Pages;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika
{
    /// <summary>
    /// Interaction logic for OceniNas.xaml
    /// </summary>
    public partial class OceniNas : Window
    {
        public OceniNas()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            

            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }
        }

        private void Odbaci_Click(object sender, RoutedEventArgs e)
        {

            this.Close();
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            Feedback feedback = new Feedback() { DateOfPublishing = DateTime.Now, Comment = polje.Text, Grade = zvezdice.Value };
            App.FeedbackController.CreateFeedback(feedback);
            this.Close();
        }
    }
}
