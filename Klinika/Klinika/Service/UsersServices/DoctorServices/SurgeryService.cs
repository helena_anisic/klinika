// File:    SurgeryService.cs
// Author:  nikol
// Created: Tuesday, May 26, 2020 9:01:46 PM
// Purpose: Definition of Class SurgeryService

using Klinika.Service.UsersServices.DoctorServices;
using Model.users.patient;
using Repository.UsersRepositories.DoctorRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.DoctorServices
{
   public class SurgeryService : ISurgeryService
   {
        private SurgeryRepository _surgeryRepository;
        public SurgeryService(SurgeryRepository surgeryRepository)
        {
            _surgeryRepository = surgeryRepository;
        }
        public Boolean CreateSurgery(Model.users.patient.Surgery surgery)
      {
            List<Surgery> surgeries = _surgeryRepository.GetAll(SurgeryRepository.path);
            if ( surgeries == null)
            {
                surgeries = new List<Surgery>();
            }
            surgeries.Add(surgery);
            _surgeryRepository.Save(surgeries, SurgeryRepository.path);
            return true;
        }
      
      public Boolean EditSurgery(Model.users.patient.Surgery surgery)
      {
            if (DeleteSurgery(surgery) == false)
                return false;
            return CreateSurgery(surgery);
        }
      
      public Boolean DeleteSurgery(Model.users.patient.Surgery surgery)
      {
            List<Surgery> surgeries = _surgeryRepository.GetAll(SurgeryRepository.path);

            if (surgeries == null)
                return false;

            foreach (Surgery s in surgeries)
            {
                if (s.Id.Equals(surgery.Id))
                {
                    surgeries.Remove(s);
                    break;
                }
            }

            return _surgeryRepository.Save(surgeries, SurgeryRepository.path);
        }
      
      public Model.users.patient.Surgery GetSurgeryById(String Id)
      {
            List<Surgery> surgeries = _surgeryRepository.GetAll(SurgeryRepository.path);
            if (surgeries == null)
                return null;
            foreach (Surgery surgery in surgeries)
            {
                if (surgery.Id.Equals(Id))
                {
                    return surgery;
                }
            }
            return null;
      }

        public List<Surgery> GetAllSugeries()
        {
            return _surgeryRepository.GetAll(SurgeryRepository.path);
        }

        public List<Surgery> GetAllSurgeriesForDoctor(String doctorId)
        {
            List<Surgery> surgeries = _surgeryRepository.GetAll(SurgeryRepository.path);
            List<Surgery> retVal = new List<Surgery>();

            if (surgeries == null)
                return null;

            foreach (Surgery s in surgeries)
            {
                if (s.DoctorId.Equals(doctorId))
                {
                    retVal.Add(s);
                }
            }

            return retVal;
        }

        public List<Surgery> GetAllSurgeriesForPatient(String patientId)
        {
            List<Surgery> surgeries = _surgeryRepository.GetAll(SurgeryRepository.path);
            List<Surgery> retVal = new List<Surgery>();

            if (surgeries == null)
                return null;

            foreach (Surgery s in surgeries)
            {
                if (s.PatientId.Equals(patientId))
                {
                    retVal.Add(s);
                }
            }

            return retVal;
        }

        public List<Surgery> GetAllSurgeriesForPatient(String patientId, DateTime date)
        {
            List<Surgery> surgery = _surgeryRepository.GetAll(SurgeryRepository.path);
            List<Surgery> ret = new List<Surgery>();

            if (surgery == null)
                return null;

            foreach (Surgery s in surgery)
            {
                if (s.PatientId.Equals(patientId) && s.StartDateTime.Date == date.Date)
                {
                    ret.Add(s);
                }
            }

            return ret;
        }

        
   
   }
}