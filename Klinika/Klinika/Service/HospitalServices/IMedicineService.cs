﻿using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.HospitalServices
{
    public interface IMedicineService
    {
        Boolean CreateMedicine(Medicine medicine);
        List<MedicineType> GetAllMedicinesTypes();
        List<Medicine> GetAllMedicine();
        Boolean EditMedicine(Medicine medicine);
        Boolean DeleteMedicine(Medicine medicine);

    }
}
