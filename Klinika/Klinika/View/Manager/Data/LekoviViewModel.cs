﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Klinika.View.Manager.Data
{
    class LekoviViewModel
    {
        public ICollectionView OrdersView { set; get; }
        public LekoviViewModel()
        {
            IList<Lekovi> orders = new OrdersLekovi();
            OrdersView = CollectionViewSource.GetDefaultView(orders);
            OrdersView.GroupDescriptions.Add(new PropertyGroupDescription("Naziv"));
        }
    }
}
