﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Klinika.Repository;
using Repository.BlogFeedbackRepositories;
using Controller.BlogFeedbackController;
using Model.users.doctor;
using Service.BlogFeedbackServices;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for BlogView.xaml
    /// </summary>
    public partial class BlogView : UserControl
    {
        public BlogView()
        {
            InitializeComponent();
            BlogPostsList.ItemsSource = App.ArticleController.GetAllBlogArticles();
        }

        private void btnNewPost_Click(object sender, RoutedEventArgs e)
        {
            NewBlogPost newBlogPost = new NewBlogPost();
            newBlogPost.Show();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void Button_SearchClick(object sender, RoutedEventArgs e)
        {
            if (SearchBox.Text.Equals(""))
            {
                BlogPostsList.ItemsSource = App.ArticleController.GetAllBlogArticles();
            }
            else
            {
                List<BlogArticle> blogPosts = new List<BlogArticle>();
                foreach (BlogArticle b in App.ArticleController.GetAllBlogArticles())
                {
                    if (b.Title.ToLower().Contains(SearchBox.Text.ToLower()) || b.Content.ToString().ToLower().Contains(SearchBox.Text.ToLower())) 
                    {
                        blogPosts.Add(b);
                    } 
                }
                BlogPostsList.ItemsSource = blogPosts;
            }

        }
    }
}
