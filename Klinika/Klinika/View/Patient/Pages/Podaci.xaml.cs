﻿using Controller.UsersController.PatientController;
using Klinika.Controller.UsersController.UserController;
using Klinika.i18N;
using Microsoft.Win32;
using Model.users.doctor;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Podaci.xaml
    /// </summary>
    public partial class Podaci : Page
    {
        public Podaci()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            fillForm();
        }

        private void AddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                App.patient.ProfileImageSource = op.FileName;
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
            }
            App.PatientController.EditPatient(App.patient);
            Menu meni = new Menu();
            meni.ContentBox.Content = new Podaci();
            Application.Current.MainWindow.Content = meni;
        }

        private void DeleteImage_Click(object sender, RoutedEventArgs e)
        {
            if (App.patient.ProfileImageSource != null)
            {
                imgPhoto.Source = new BitmapImage(new Uri("/View/Patient/Images/default.jpg", UriKind.Relative));
                App.patient.ProfileImageSource = null;
                App.PatientController.EditPatient(App.patient);
                Menu meni = new Menu();
                meni.ContentBox.Content = new Podaci();
                Application.Current.MainWindow.Content = meni;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Window win = new PromeniLozinku();
            win.ShowDialog();
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {

            Menu meni = new Menu();
            meni.ContentBox.Content = new IzmeniPodatke();
            Application.Current.MainWindow.Content = meni;

        }

        private void fillForm()
        {
            User user = App.patient;
            name.Text = user.Name;
            surname.Text = user.Surname;
            if (user.DateOfBirth != null)
                dateOfBirth.Text = user.DateOfBirth.ToString("dd.MM.yyyy.");
            else
                dateOfBirth.Text = "empty";
            id.Text = user.Id;
            if (user.Gender == Gender.male)
                male.IsChecked = true;
            else
                female.IsChecked = true;
            if (user.City != null)
                city.Text = user.City.Name;
            if (user.Country != null)
            {
                country.Text = user.Country.Name;
            }
            address.Text = user.Address;
            phone.Text = user.PhoneNumber;
            email.Text = user.Email;
            if (user.Doctor != null)
            {
                doctor.Text = "dr " + user.Doctor.Name + " " + user.Doctor.Surname;
            }
            if(user.ProfileImageSource != null)
                imgPhoto.Source = new BitmapImage(new Uri(user.ProfileImageSource));
        }
    }
}
