﻿using Controller.UsersController.SecretaryController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for PromenaLozinke.xaml
    /// </summary>
    public partial class PromenaLozinke : Page
    {
        public PromenaLozinke()
        {
            InitializeComponent();
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Account();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (oldPassword.Password == App.secretary.Password && newPassword.Password.Length > 8 && newPassword1.Password == newPassword.Password)
            {
                App.secretary.Password = newPassword1.Password.ToString();

                App.SecretaryController.EditSecretary(App.secretary);
                Application.Current.MainWindow.Content = new Pocetna();
            }
            
        }
    }
}
