// File:    MedicineType.cs
// Author:  helen
// Created: Friday, April 17, 2020 10:46:46 PM
// Purpose: Definition of Class MedicineType

using System;

namespace Model.Storage
{
    public class MedicineType
    {
        public String name { get; set; }

        public override string ToString()
        {
            return name;
        }
    }
}