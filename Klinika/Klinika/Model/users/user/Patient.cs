// File:    Patient.cs
// Author:  nasta
// Created: Tuesday, June 2, 2020 7:57:13 PM
// Purpose: Definition of Class Patient

using Model.users.doctor;
using System;
using System.Collections.Generic;

namespace Model.users.user
{
    public class Patient : User
    {
        public bool Mode { get; set; }
        public Patient()
        {
        }
    }
}