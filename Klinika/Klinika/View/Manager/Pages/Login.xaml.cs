﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Repository.UsersRepositories.ManagerRepositories;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Klinika.View.Manager.Pages
{
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (emailErr == null)
                return;
            if (!emailValidation(UsernameTextBox.Text))
            {
                usernameBorder.BorderBrush = Brushes.Red;
                emailErr.Visibility = Visibility.Visible;
            }
            else
            {
                usernameBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(119, 119, 119));
                emailErr.Visibility = Visibility.Hidden;
            }

        }

        public bool emailValidation(String s)
        {
            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(s);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //if(passwordTextBox.Password.ToString().Equals("12345678"))
            //Application.Current.MainWindow.Content = new Video0();
            //Application.Current.MainWindow.Content = new HomePage();

            Model.users.user.Manager manager = App.ManagerController.GetManagerByAccount(new Model.users.user.Account(UsernameTextBox.Text, passwordTextBox.Password));
            App.manager = manager;
            if (manager != null)
            {
                logErr.Visibility = Visibility.Hidden;
                usernameBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(119, 119, 119));
                passwordBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(119, 119, 119));
                Application.Current.MainWindow.Content = new HomePage();
            }
            else
            {
                logErr.Visibility = Visibility.Visible;
                usernameBorder.BorderBrush = Brushes.Red;
                passwordBorder.BorderBrush = Brushes.Red;
            }
        }

        bool password_clicked = false;
        private void PasswordBorder_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!password_clicked)
            {
                PasswordBox box = sender as PasswordBox;
                box.Password = String.Empty;
                password_clicked = true;
            }
        }

        private void PasswordTextBod_PasswordChanged(object sender, RoutedEventArgs e)
        {

        }
    }
}
