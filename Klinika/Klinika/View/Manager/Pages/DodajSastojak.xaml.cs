﻿using Klinika.Controller.HospitalController;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for DodajSastojak.xaml
    /// </summary>
    public partial class DodajSastojak : Window
    {
        public Medicine medicine = new Medicine();
        public DodajSastojak(Medicine medicine)
        {
            this.medicine = medicine;
            InitializeComponent();
            List<Ingredient> ingredients = App.IngredientController.GetAllIngredients();
            foreach (Ingredient ingredient in ingredients)
            {
                LB1.Items.Add(ingredient);
            }
            if (medicine.Ingredients != null)
            {
                foreach (IngredientQuantity ingredient in medicine.Ingredients)
                {
                    LB2.Items.Add(ingredient);
                }
            }
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            medicine.Ingredients = null;
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (LB1.SelectedIndex < 0)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti sastojak koju\nzelite da dodate!", "Ok");
                w.ShowDialog();
                return;
            }
            Ingredient i = (Ingredient)LB1.SelectedItem;
            if (medicine.Ingredients == null)
                medicine.Ingredients = new List<IngredientQuantity>();
            if (quantity.Text.Equals(""))
            {
                Window w = new MessageBoxOk("Oprez", "Niste uneli kolicinu sastojka!", "Ok");
                w.ShowDialog();
                return;
            }
            foreach (IngredientQuantity ingredientQuantity in medicine.Ingredients)
            {
                if (ingredientQuantity.ingredient.name.Equals(i.name))
                {
                    ingredientQuantity.quantity += Int32.Parse(quantity.Text);
                    RefreshListBox2();
                    return;
                }
            }
            medicine.Ingredients.Add(new IngredientQuantity() { ingredient = new Ingredient() { IsAllergen = i.IsAllergen, name = i.name }, quantity = Int32.Parse(quantity.Text) });
            RefreshListBox2();
        }

        private void ObrisiSastojak_Click(object sender, RoutedEventArgs e)
        {
            IngredientQuantity i = (IngredientQuantity)LB2.SelectedItem;
            if (LB2.SelectedIndex < 0)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti sastojak koju\nzelite da obrisete!", "Ok");
                w.ShowDialog();
                return;
            }
            if (medicine.Ingredients == null)
                return;
            foreach (IngredientQuantity ingredientQuantity in medicine.Ingredients)
            {
                if (i.Equals(ingredientQuantity))
                {
                    medicine.Ingredients.Remove(i);
                    break;
                }
            }
            RefreshListBox2();
            if (LB2.Items.Count > 0)
                LB2.SelectedIndex = 0;
        }

        private void RefreshListBox2()
        {
            LB2.Items.Clear();
            List<IngredientQuantity> ingredients = medicine.Ingredients;
            foreach (IngredientQuantity ingredient in ingredients)
            {
                LB2.Items.Add(ingredient);
            }
        }
    }
}
