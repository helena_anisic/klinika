// File:    SpecializationService.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:27:44 AM
// Purpose: Definition of Class SpecializationService

using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using Repository.UsersRepositories.DoctorRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.DoctorServices
{
   public class SpecializationService : ISpecializationService
   {
        private SpecializationRepository _specializationRepository;

        public SpecializationService(SpecializationRepository specializationRepository)
        {
            _specializationRepository = specializationRepository;
        }
        public Boolean CreateSpecialization(Model.users.doctor.TypeOfSpecialization specialization)
      {
            List<TypeOfSpecialization> specializations = _specializationRepository.GetAll(SpecializationRepository.path);
            if (specializations == null)
            {
                specializations = new List<TypeOfSpecialization>();
            }
            specializations.Add(specialization);
            _specializationRepository.Save(specializations, SpecializationRepository.path);
            return true;
        }
      
      public Boolean EditSpecialization(Model.users.doctor.TypeOfSpecialization specialization)
      {
            if (DeleteSpecialization(specialization) == false)
                return false;
            return CreateSpecialization(specialization);
        }
      
      public Boolean DeleteSpecialization(Model.users.doctor.TypeOfSpecialization specialization)
      {
            List<TypeOfSpecialization> specializations = _specializationRepository.GetAll(SpecializationRepository.path);

            if (specializations == null)
                return false;

            foreach (TypeOfSpecialization typeOfSpecialization in specializations)
            {
                if (typeOfSpecialization.Id.Equals(specialization.Id))
                {
                    specializations.Remove(typeOfSpecialization);
                    break;
                }
            }

            return _specializationRepository.Save(specializations, SpecializationRepository.path);
        }
      
      public List<Model.users.doctor.TypeOfSpecialization> GetAllSpecializations()
      {
            return _specializationRepository.GetAll(SpecializationRepository.path);
        }
      
      public Model.users.doctor.TypeOfSpecialization GetSpecializationById(String Id)
      {
            List<TypeOfSpecialization> specializations = GetAllSpecializations();
            if (specializations == null)
                return null;
            foreach (TypeOfSpecialization specialization in specializations)
            {
                if (specialization.Id.Equals(Id))
                {
                    return specialization;
                }
            }
            return null;
        }
      
      
   
   }
}