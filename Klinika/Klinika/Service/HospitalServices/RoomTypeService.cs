﻿using Klinika.Repository.HospitalRepositories;
using Model.Hospital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.HospitalServices
{
    public class RoomTypeService : IRoomTypeService
    {
        private RoomTypeRepository _roomTypeRepository;

        public RoomTypeService(RoomTypeRepository roomTypeRepository)
        {
            _roomTypeRepository = roomTypeRepository;
        }

        public Boolean CreateRoomType(RoomType roomType)
        {
            List<RoomType> roomTypes = _roomTypeRepository.GetAll(RoomTypeRepository.path);
            if (roomTypes == null)
            {
                roomTypes = new List<RoomType>();
            }
            roomTypes.Add(roomType);
            _roomTypeRepository.Save(roomTypes, RoomTypeRepository.path);
            return true;
        }
        public Boolean DeleteRoomType(RoomType roomType)
        {
            List<RoomType> roomTypes = _roomTypeRepository.GetAll(RoomTypeRepository.path);

            if (roomTypes == null)
                return false;

            foreach (RoomType r in roomTypes)
            {
                if (r.type.Equals(roomType.type))
                {
                    roomTypes.Remove(r);
                    break;
                }
            }

            return _roomTypeRepository.Save(roomTypes, RoomTypeRepository.path);
        }
        public List<RoomType> GetAllRoomTypes() => _roomTypeRepository.GetAll(RoomTypeRepository.path);

    }
}
