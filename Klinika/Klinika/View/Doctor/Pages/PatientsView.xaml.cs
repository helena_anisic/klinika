﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.doctor;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for PatientsView.xaml
    /// </summary>
    public partial class PatientsView : UserControl
    {
        public PatientsView()
        {
            InitializeComponent();
            constructDataGridPatients();

            existingIds = new HashSet<string>();

            this.DataContext = this;
            Patients = new List<Model.users.user.Patient>();
            foreach (ExaminationAppointment appointment in App.ExaminationAppointmentController.GetAllAppointments())
            {
                if (appointment.DoctorId.Equals(LogIn.loggedInUser.Id) && !existingIds.Contains(appointment.PatientId))
                {
                    Patients.Add(App.PatientController.GetPatientById(appointment.PatientId));
                    existingIds.Add(appointment.PatientId);
                }
            }
        }

        public void constructDataGridPatients()
        {
            dataGridPatients.FrozenColumnCount = 9;
        }



        public List<Model.users.user.Patient> Patients { get; set; }

        public HashSet<string> existingIds { get; set; }

        private void ShowPatientFile(object sender, RoutedEventArgs e)
        {
            Model.users.user.Patient patient = ((FrameworkElement)sender).DataContext as Model.users.user.Patient;
            foreach (PatientFile patientFile in App.PatientFileController.GetAllPatientFiles()) 
            {
                if (patient.Id.Equals(patientFile.Id))
                {
                    PatientFileView patientFileView = new PatientFileView(patient, patientFile);
                    patientFileView.Show();
                }
            }
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
