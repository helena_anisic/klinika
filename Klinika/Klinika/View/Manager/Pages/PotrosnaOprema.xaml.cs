﻿using Klinika.Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for PotrosnaOprema.xaml
    /// </summary>
    public partial class PotrosnaOprema : Page
    {
        public PotrosnaOprema()
        {
            InitializeComponent();
            MedOprema.ItemsSource = App.EquipmentController.GetAllConsumables();
            MedOprema.HeadersVisibility = DataGridHeadersVisibility.Column;
        }

        private void Button_Click_Dodaj_Opremu(object sender, RoutedEventArgs e)
        {
            Window w = new DodajPotrosnuOpremu();
            w.ShowDialog();
            MedOprema.ItemsSource = App.EquipmentController.GetAllConsumables();
        }

        private void Button_Click_Izmeni_Opremu(object sender, RoutedEventArgs e)
        {
            EquipmentQuantity equipment = (EquipmentQuantity)MedOprema.SelectedItem;
            Window w;
            if (equipment == null)
            {
                w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }
            IzmeniPotrosnuOpremu ipo = new IzmeniPotrosnuOpremu(equipment);
            ipo.ShowDialog();
            MedOprema.ItemsSource = App.EquipmentController.GetAllConsumables();
        }

        private void Button_Click_Obrisi_Opremu(object sender, RoutedEventArgs e)
        {
            EquipmentQuantity equipment = (EquipmentQuantity)MedOprema.SelectedItem;
            if (equipment == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da obrisete!", "Ok");
                w.ShowDialog();
            }
            else
            {
                MessageBoxYesNo mb = new MessageBoxYesNo("Obriši opremu", "Da li želite da obrišete opremu\n" + equipment.equipmentB.name + " id:" + equipment.equipmentB.id + "?", "Odustani", "Obriši");
                mb.ShowDialog();

                if (mb.GetCloseStatus() == 1)
                {
                    App.EquipmentController.DeleteConsumable(equipment);
                    MedOprema.ItemsSource = App.EquipmentController.GetAllConsumables();
                }
            }
        }

        private void Button_Click_Premesti_Opremu(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new PremestiOpremu());
        }
    }
}
