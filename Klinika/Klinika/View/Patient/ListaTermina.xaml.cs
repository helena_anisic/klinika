﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Patient
{
    /// <summary>
    /// Interaction logic for ListaTermina.xaml
    /// </summary>
    public partial class ListaTermina : Window
    {
        public String parent;
        public DateTime date;
        public String vrsta;
        public ListaTermina(String parent, DateTime date)
        {
            this.date = date;
            this.parent = parent;
            InitializeComponent();
            this.vrsta = parent;
            if (parent.Equals("p")) {
                termini.ItemsSource = App.ExaminationAppointmentController.GetAllAppointmentsForPatient(App.patient.Id, date);
            } else if (parent.Equals("o"))
            {
                termini.ItemsSource = App.SurgeryController.GetAllSurgeriesForPatient(App.patient.Id, date);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Termini_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vrsta.Equals("p"))
            {
                ExaminationAppointment examinationAppointment = (ExaminationAppointment)termini.SelectedItem;
                if (examinationAppointment == null)
                    return;
                Window w = new PregledD(examinationAppointment);
                w.ShowDialog();
            }else if (vrsta.Equals("o"))
            {
                Surgery surgery = (Surgery)termini.SelectedItem;
                if (surgery == null)
                    return;
                Window w = new OperacijaDetaljno(surgery);
                w.ShowDialog();
            }
        }
    }
}
