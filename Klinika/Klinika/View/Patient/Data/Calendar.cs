﻿using Controller.UsersController.ManagerController;
using Controller.UsersController.PatientController;
using Model.users.manager;
using Model.users.patient;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Klinika.Data
{
    class Calendar
    {
        public struct podaci
        {
            public String Background { get; set; }
        }

        public String Termini { get; set; }
        public podaci Kolona1 { get; set; }
        public podaci Kolona2 { get; set; }
        public podaci Kolona3 { get; set; }
        public podaci Kolona4 { get; set; }
        public podaci Kolona5 { get; set; }
        public podaci Kolona6 { get; set; }
        public podaci Kolona7 { get; set; }


        public static ObservableCollection<Calendar> Data(DateTime currDate, DateTime startDate, DateTime endDate, Doctor doctor)
        {
            List<ExaminationAppointment> examinationAppointments = new List<ExaminationAppointment>();
            List<WorkingPeriod> workingPeriods = new List<WorkingPeriod>();
            if (doctor == null)
            {
                examinationAppointments = App.ExaminationAppointmentController.GetAllAppointments();
                workingPeriods = App.WorkingPeriodController.GetAllWorkingPeriods();
            }
            else
            {
                examinationAppointments = App.ExaminationAppointmentController.GetAllAppointmentsForDoctor(doctor.Id);
                workingPeriods = App.WorkingPeriodController.GetAllWorkingPeriodsForDoctor(doctor.Id);
            }
            String getData(DateTime curr, DateTime start, DateTime end, String termin)
            {
                if (!(curr >= start && curr <= end))
                    return "Silver";

                String[] parts = termin.Split(new string[] { " - " }, StringSplitOptions.None);
                String[] sTime = parts[0].Split(':');
                int sH = Int32.Parse(sTime[0]);
                int sM = Int32.Parse(sTime[1]);
                curr = curr.AddHours(sH);
                curr = curr.AddMinutes(sM);

                int radiLekar = 0;

                foreach (WorkingPeriod wp in workingPeriods)
                {
                    if (curr >= wp.Shift.StartDateTime && curr.AddMinutes(30) <= wp.Shift.EndDateTime)
                    {
                        radiLekar++;
                    }
                }
                if (radiLekar != 0)
                {
                    int terminZauzet = 0;

                    foreach (ExaminationAppointment ex in examinationAppointments)
                    {
                        if (ex.StartDateTime == curr && ex.EndDateTime == curr.AddMinutes(30))
                        {
                            if (doctor != null)
                            {
                                if (doctor.Id.Equals(ex.DoctorId))
                                    terminZauzet++;
                            }
                            else
                                terminZauzet++;
                        }
                    }
                    if (terminZauzet == 0)
                        return "";
                    else if (terminZauzet < radiLekar)
                        return "";
                    else
                        return "#b3ded1";
                }
                else
                {
                    return "#b3ded1";
                }
            }


            ObservableCollection<Calendar> lista = new ObservableCollection<Calendar>();
            List<string> termini = new List<string>();
            termini.Add("00:00 - 00:30");
            termini.Add("00:30 - 01:00");
            termini.Add("01:00 - 01:30");
            termini.Add("01:30 - 02:00");
            termini.Add("02:00 - 02:30");
            termini.Add("02:30 - 03:00");
            termini.Add("03:00 - 03:30");
            termini.Add("03:30 - 04:00");
            termini.Add("04:00 - 04:30");
            termini.Add("04:30 - 05:00");
            termini.Add("05:00 - 05:30");
            termini.Add("05:30 - 06:00");
            termini.Add("06:00 - 06:30");
            termini.Add("06:30 - 07:00");
            termini.Add("07:00 - 07:30");
            termini.Add("07:30 - 08:00");
            termini.Add("08:00 - 08:30");
            termini.Add("08:30 - 09:00");
            termini.Add("09:00 - 09:30");
            termini.Add("09:30 - 10:00");
            termini.Add("10:00 - 10:30");
            termini.Add("10:30 - 11:00");
            termini.Add("11:00 - 11:30");
            termini.Add("11:30 - 12:00");
            termini.Add("12:00 - 12:30");
            termini.Add("12:30 - 13:00");
            termini.Add("13:00 - 13:30");
            termini.Add("13:30 - 14:00");
            termini.Add("14:00 - 14:30");
            termini.Add("14:30 - 15:00");
            termini.Add("15:00 - 15:30");
            termini.Add("15:30 - 16:00");
            termini.Add("16:00 - 16:30");
            termini.Add("16:30 - 17:00");
            termini.Add("17:00 - 17:30");
            termini.Add("17:30 - 18:00");
            termini.Add("18:00 - 18:30");
            termini.Add("18:30 - 19:00");
            termini.Add("19:00 - 19:30");
            termini.Add("19:30 - 20:00");
            termini.Add("20:00 - 20:30");
            termini.Add("20:30 - 21:00");
            termini.Add("21:00 - 21:30");
            termini.Add("21:30 - 22:00");
            termini.Add("22:00 - 22:30");
            termini.Add("22:30 - 23:00");
            termini.Add("23:00 - 23:30");
            termini.Add("23:30 - 00:00");

            foreach (String t in termini)
            {
                lista.Add(new Calendar
                {
                    Termini = t,
                    Kolona1 = new podaci { Background = getData(currDate.Date, startDate, endDate, t) },
                    Kolona2 = new podaci { Background = getData(currDate.AddDays(1).Date, startDate, endDate, t) },
                    Kolona3 = new podaci { Background = getData(currDate.AddDays(2).Date, startDate, endDate, t) },
                    Kolona4 = new podaci { Background = getData(currDate.AddDays(3).Date, startDate, endDate, t) },
                    Kolona5 = new podaci { Background = getData(currDate.AddDays(4).Date, startDate, endDate, t) },
                    Kolona6 = new podaci { Background = getData(currDate.AddDays(5).Date, startDate, endDate, t) },
                    Kolona7 = new podaci { Background = getData(currDate.AddDays(6).Date, startDate, endDate, t) }
                });
            }
            return lista;
        }
    }
}
