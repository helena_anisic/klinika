﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.DoctorServices
{
    public interface IDoctorService
    {
        Boolean CreateDoctor(Doctor doctor);
        Boolean EditDoctor(Doctor doctor);
        List<Doctor> GetAllDoctors();
        Boolean DeleteDoctor(Doctor doctor);
        Doctor GetDoctorById(String id);

    }
}
