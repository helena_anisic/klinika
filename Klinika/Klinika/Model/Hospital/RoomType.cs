// File:    RoomType.cs
// Author:  helen
// Created: Saturday, April 18, 2020 4:25:17 PM
// Purpose: Definition of Class RoomType

using System;

namespace Model.Hospital
{
    public class RoomType
    {
        public String type { get; set; }

        public override string ToString()
        {
            return type;
        }
    }
}