// File:    WorkingPeriod.cs
// Author:  nikol
// Created: Tuesday, April 21, 2020 10:15:59 AM
// Purpose: Definition of Class WorkingPeriod

using System;

namespace Model.users.manager
{
    public class WorkingPeriod
    {
        public Shift Shift { get; set; }
        public String DoctorId { get; set; }
        public String RoomId { get; set; }

    }
}