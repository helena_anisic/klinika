﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model
{
    [Serializable]
    public class BlogPost
    {
        public User Doctor { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public DateTime DateOfCreation { get; set; }

        public BlogPost() { }
    }
}
