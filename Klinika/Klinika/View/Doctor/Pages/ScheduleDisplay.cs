﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Pages
{
    class ScheduleDisplay
    {
        public String Id { get; set; }
        public String Cause { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

        public ScheduleDisplay() { }
        public ScheduleDisplay(String id, String cause, DateTime startDateTime, DateTime endDateTime)
        {
            this.Id = id;
            this.Cause = cause;
            this.StartDateTime = startDateTime;
            this.EndDateTime = endDateTime;
        }
    }
}
