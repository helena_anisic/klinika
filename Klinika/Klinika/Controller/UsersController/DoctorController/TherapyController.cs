﻿using Klinika.Service.UsersServices.DoctorServices;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.DoctorController
{
    public class TherapyController : ITherapyController
    {
        private TherapyService _therapyService;

        public TherapyController(TherapyService therapyService)
        {
            _therapyService = therapyService;
        }
        public Boolean CreateTherapy(Therapy therapy)
        {
            if (therapy == null)
                return false;
            return _therapyService.CreateTherapy(therapy);
        }

        public List<Therapy> GetAllTherapies()
        {
            return _therapyService.GetAllTherapies();
        }
    }
}
