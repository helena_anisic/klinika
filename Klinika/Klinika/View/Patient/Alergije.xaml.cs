﻿using Controller.UsersController.DoctorController;
using Klinika.i18N;
using Klinika.Pages;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika
{
    /// <summary>
    /// Interaction logic for Alergije.xaml
    /// </summary>
    public partial class Alergije : Window
    {
        public Alergije()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            List<Allergy> alergije = App.PatientFileController.GetPatientFile(App.patient.Id).Allergy;
            List<String> nazivi = new List<String>();
            foreach(Allergy alergija in alergije)
            {
                nazivi.Add(alergija.Name);
            }
            listaAlergija.ItemsSource = nazivi;
            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style2"];
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
