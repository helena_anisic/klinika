﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Klinika.View.Manager.Pages
{
    class OpremaView
    {
        public ICollectionView OrdersView { set; get; }
        public OpremaView(Room room)
        {
            OrdersView = null;
            IList<Equipment> orders = null;
            if (room == null)
            {
                room = App.RoomController.GetRoomById("Magacin");
                orders = room.equipment;
            }
            else
                orders = room.equipment;
            if (orders == null)
                return;
            OrdersView = new CollectionViewSource { Source = orders }.View;
            //OrdersView = CollectionViewSource.GetDefaultView(orders);
            OrdersView.GroupDescriptions.Add(new PropertyGroupDescription("name"));
        }
    }
}
