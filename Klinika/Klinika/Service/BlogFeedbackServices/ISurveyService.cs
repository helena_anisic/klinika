﻿using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.BlogFeedbackServices
{
    public interface ISurveyService
    {
        Boolean CreateSurvey(Survey survey);
        List<Survey> GetAllSurveys();
    }
}
