// File:    EquipmentQuantity.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:39:31 AM
// Purpose: Definition of Class EquipmentQuantity

using System;

namespace Model.Storage
{
    public class EquipmentQuantity
    {
        public int quantity { get; set; }

        public Equipment equipmentB { get; set; }

    }
}