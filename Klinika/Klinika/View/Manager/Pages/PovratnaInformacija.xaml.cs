﻿using Controller.BlogFeedbackController;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for PovratnaInformacija.xaml
    /// </summary>
    public partial class PovratnaInformacija : Page
    {
        public PovratnaInformacija()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Feedback feedback = new Feedback() { DateOfPublishing = DateTime.Now, Comment = comment.Text, Grade = grade.Value};
            App.FeedbackController.CreateFeedback(feedback);
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new MojNalog());
        }
    }
}
