﻿using Controller.UsersController.SecretaryController;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for BrzaRegistracija.xaml
    /// </summary>
    public partial class BrzaRegistracija : Page
    {
        GuestAccount guestAccount = null;
        public BrzaRegistracija()
        {
            InitializeComponent();
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }

        private void BtnRegistruj_Click(object sender, RoutedEventArgs e)
        {
            GuestAccount guestAccount = new GuestAccount();
            guestAccount.jmbg = int.Parse( JMBG.Text);
            guestAccount.name = txtIme.Text;
            guestAccount.surname = txtPrezime.Text;
            App.GuestController.CreateGuestAccount(guestAccount);
            Application.Current.MainWindow.Content = new Meni();
        }
    }
}
