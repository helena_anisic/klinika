// File:    IngredientQuantity.cs
// Author:  Admin
// Created: Monday, April 20, 2020 5:42:21 PM
// Purpose: Definition of Class IngredientQuantity

using System;

namespace Model.Storage
{
    public class IngredientQuantity
    {
        public double quantity { get; set; }
        public Ingredient ingredient { get; set; }
        public override string ToString()
        {
            return ingredient.name + (ingredient.IsAllergen == true ? "  Alergen  " : "  ") + quantity + "mg";
        }
    }
}