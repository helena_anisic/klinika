// File:    ExaminationAppointmentController.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:31:59 PM
// Purpose: Definition of Class ExaminationAppointmentController

using Klinika.Controller.UsersController.PatientController;
using Klinika.Service.UsersServices.PatientServices;
using Model.users.patient;
using Model.users.user;
using Service.UsersServices.PatientServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.PatientController
{
    public class ExaminationAppointmentController : IExaminationAppointmentController
    {
        private IExaminationAppointmentService _examinationAppointmentService;
        public ExaminationAppointmentController(IExaminationAppointmentService examinationAppointmentService)
        {
            _examinationAppointmentService = examinationAppointmentService;
        }

        public ExaminationAppointmentController()
        {
        }

        public Boolean CreateExaminationAppointment(ExaminationAppointment examinationAppointment)
        {
            if (examinationAppointment == null)
                return false;
            return _examinationAppointmentService.CreateExaminationAppointment(examinationAppointment);
        }

        public Boolean EditExaminationAppointment(ExaminationAppointment examinationAppointment)
        {
            return _examinationAppointmentService.EditExaminationAppointment(examinationAppointment);
        }

        public Boolean DeleteExaminationAppointment(ExaminationAppointment examinationAppointment)
        {
            if (examinationAppointment == null)
                return false;
            return _examinationAppointmentService.DeleteExaminationAppointment(examinationAppointment);
        }

        public List<ExaminationAppointment> GetAllAppointments()
        {
            return _examinationAppointmentService.GetAllAppointments();
        }

        public List<ExaminationAppointment> GetAllAppointmentsForDoctor(String doctorId)
        {
            if (doctorId == null || doctorId.Equals(""))
                return null;
            return _examinationAppointmentService.GetAllAppointmentsForDoctor(doctorId);
        }

        public List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId)
        {
            if (patientId == null || patientId.Equals(""))
                return null;
            return _examinationAppointmentService.GetAllAppointmentsForPatient(patientId);
        }

        public List<ExaminationAppointment> GetAllAppointmentsForPatient(String patientId, DateTime date)
        {
            if (patientId == null || patientId.Equals("") || date == null)
                return null;
            return _examinationAppointmentService.GetAllAppointmentsForPatient(patientId, date);
        }

        public Doctor GetFreeDoctorForTerm(DateTime startDate, DateTime endDate)
        {
            return _examinationAppointmentService.GetFreeDoctorForTerm(startDate, endDate);
        }

        public ExaminationAppointment GetFreeAppointment(DateTime startDate, DateTime endDate)
        {
            return _examinationAppointmentService.GetFreeAppointment(startDate, endDate);
        }

        public ExaminationAppointment GetFreeAppointment(DateTime startDate, DateTime endDate, String doctorId)
        {
            return _examinationAppointmentService.GetFreeAppointment(startDate, endDate, doctorId);
        }
    }
}