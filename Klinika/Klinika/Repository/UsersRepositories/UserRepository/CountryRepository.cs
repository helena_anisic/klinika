// File:    CountryRepository.cs
// Author:  nikol
// Created: Friday, May 29, 2020 11:29:11 AM
// Purpose: Definition of Class CountryRepository

using Klinika.Repository;
using Model.Hospital;
using Model.users.user;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.UsersRepositories.UserRepository
{
    public class CountryRepository : Repository<Country>
    {
        public static String path = @"..\..\Resources\Data\Countries.txt";

    }
}