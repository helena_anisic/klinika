// File:    FeedbackController.cs
// Author:  nikol
// Created: Tuesday, June 2, 2020 10:07:01 AM
// Purpose: Definition of Class FeedbackController

using Klinika.Controller.BlogFeedbackController;
using Model.users.user;
using Service.BlogFeedbackServices;
using System;

namespace Controller.BlogFeedbackController
{
    public class FeedbackController : IFeedbackController
    {
        private FeedbackService _feedbackService;

        public FeedbackController(FeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        public Boolean CreateFeedback(Feedback feedback)
        {
            if (feedback == null)
            {
                return false;
            }
            return _feedbackService.CreateFeedback(feedback);
        }
    }
}