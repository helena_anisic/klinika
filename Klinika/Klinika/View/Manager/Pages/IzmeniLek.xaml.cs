﻿using Controller.HospitalController;
using Model.Storage;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for IzmeniLek.xaml
    /// </summary>
    public partial class IzmeniLek : Page
    {
        Medicine medicine = new Medicine();
        public IzmeniLek(Medicine medicine)
        {
            this.medicine = medicine;
            InitializeComponent();
            List<MedicineType> medicineTypes = App.MedicineController.GetAllMedicinesTypes();
            if (medicineType != null)
            {
                foreach (MedicineType mType in medicineTypes)
                {
                    medicineType.Items.Add(mType);
                }
            }
            fillForm();
            RefreshListBox();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            medicine.Status = MedicineStatus.inProces;
            readForm();
            App.MedicineController.EditMedicine(medicine);
            HomePage hp = new HomePage();
            hp.setContentBox(new Lekovi());
            Application.Current.MainWindow.Content = hp;
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            HomePage hp = new HomePage();
            hp.setContentBox(new Lekovi());
            Application.Current.MainWindow.Content = hp;
        }

        private void fillForm()
        {
            id.Text = medicine.Id.ToString();
            name.Text = medicine.Name;
            manufacturer.Text = medicine.Manufacturer;
            quantity.Text = medicine.Quantity.ToString();
            description.Text = medicine.Description;
            if (medicine.MedicineType == null)
                return;
            int i = 0;
            foreach (MedicineType m in medicineType.Items)
            {
                if (m.name.Equals(medicine.MedicineType.name))
                {
                    medicineType.SelectedIndex = i;
                    return;
                }
                i++;
            }
        }

        private void IzmeniSastojak_Click(object sender, RoutedEventArgs e)
        {
            DodajSastojak ds = new DodajSastojak(medicine);
            ds.ShowDialog();
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            ingredientsList.Items.Clear();
            if (medicine.Ingredients != null)
            {
                List<IngredientQuantity> ingredients = medicine.Ingredients;
                foreach (IngredientQuantity ingredient in ingredients)
                {
                    ingredientsList.Items.Add(ingredient);
                }
            }
        }

        private void readForm()
        {
            medicine.Id = id.Text;
            medicine.Name = name.Text;
            medicine.Manufacturer = manufacturer.Text;
            medicine.Quantity = Int32.Parse(quantity.Text);
            medicine.Description = description.Text;
            medicine.MedicineType = (MedicineType)medicineType.SelectedItem;

        }
    }
}
