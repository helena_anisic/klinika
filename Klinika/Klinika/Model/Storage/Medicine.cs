// File:    Medicine.cs
// Author:  helen
// Created: Friday, April 17, 2020 10:44:01 PM
// Purpose: Definition of Class Medicine

using Model.users.doctor;
using System;
using System.Collections.Generic;

namespace Model.Storage
{
    public class Medicine
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public int Quantity { get; set; }
        public string Manufacturer { get; set; }
        public String Description { get; set; }
        public DateTime ApprovalDate { get; set; }
        public MedicineStatus Status { get; set; }
        public List<IngredientQuantity> Ingredients { get; set; }
        public MedicineType MedicineType { get; set; }
    }
}