﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for MessageBoxYesNo.xaml
    /// </summary>
    public partial class MessageBoxYesNo : Window
    {
        private int closeStatus;
        public MessageBoxYesNo(String title, String description, String no, String yes)
        {
            InitializeComponent();
            Heading.Text = title;
            Description.Text = description;
            No.Content = no;
            Yes.Content = yes;
            closeStatus = -1;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            this.closeStatus = 1;
            this.Close();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            this.closeStatus = 0;
            this.Close();
        }

        public int GetCloseStatus()
        {
            return closeStatus;
        }
    }
}
