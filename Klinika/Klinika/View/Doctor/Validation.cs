﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Klinika.View.Doctor
{
    public class ValidationUsername : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {

            try
            {
                var s = value as string;
                if (string.IsNullOrEmpty(s))
                {
                    return new ValidationResult(false, "Enter username");
                }
                return new ValidationResult(true, null);
            }
            catch
            {
                return new ValidationResult(false, "Input error.");
            }
        }
    }

    public class ValidationPassword : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                var s = value as string;
                if (string.IsNullOrEmpty(s))
                {
                    return new ValidationResult(false, "Enter password.");
                }
                else if (s.Equals(s.Contains(" ")))
                {
                    return new ValidationResult(false, "Password must not contains space.");
                }
                else if (s.Equals(s.Contains("~")))
                {
                    return new ValidationResult(false, "Password must not contains ~.");
                }
                else if (s.Equals(s.Contains("`")))
                {
                    return new ValidationResult(false, "Password must not contains `.");
                }
                return new ValidationResult(true, null);
            }
            catch
            {
                return new ValidationResult(false, "Input error.");
            }
        }
    }

    public class ValidationRequired : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                var s = value as string;
                if (string.IsNullOrEmpty(s))
                {
                    return new ValidationResult(false, "This field is mandatory.");
                }
                return new ValidationResult(true, null);
            }
            catch
            {
                return new ValidationResult(false, "Input error.");
            }
        }
    }

    public class ValidationPatientName : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                var s = value as string;
                if (string.IsNullOrEmpty(s))
                {
                    return new ValidationResult(false, "Enter patient name.");
                }
                else if (s.Equals(s.Contains("0")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("1")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("2")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("3")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("4")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("5")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("6")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("7")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("8")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                else if (s.Equals(s.Contains("9")))
                {
                    return new ValidationResult(false, "Patient name must not contains number.");
                }
                return new ValidationResult(true, null);
            }
            catch
            {
                return new ValidationResult(false, "Input error.");
            }
        }
    }

    public class ValidationPatientID : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                var s = value as string;
                int r;
                long rr;
                if (Int32.TryParse(s, out r))
                {
                    if (r < 0)
                    {
                        return new ValidationResult(false, "Patient Id must not contains -.");
                    }

                    return new ValidationResult(true, null);
                }
                if (Int64.TryParse(s, out rr))
                {
                    if (rr > Int32.MaxValue)
                    {
                        return new ValidationResult(false, "To much numbers.");
                    }
                }

                return new ValidationResult(false, "Input numbers.");
            }
            catch
            {
                return new ValidationResult(false, "Input only numbers.");
            }
        }
    }
}
