﻿using Controller.BlogFeedbackController;
using Controller.UsersController.DoctorController;
using Klinika.i18N;
using Model.Storage;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        
        public Menu()
        {
            InitializeComponent();
            ContentBox.Content = new Homepage();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Homepage()));
            fillForm();
            if (App.patient.Mode == true)
            {
                this.Resources["font_style"] = this.Resources["font_style"];
                nav_pnl.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
                gornji.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
                this.Background = new SolidColorBrush(Color.FromRgb(34, 45, 50));
            }

            

        }

        private void ContentBox_Navigated(object sender, NavigationEventArgs e)
        {

        }

        private void MenuButton(object sender, RoutedEventArgs e)
        {
            if (PreglediIOperacije.Equals(sender) == true)
            {
                ContentBox.Content = new ZakazaniPregledi();
                PreglediIOperacije.Visibility = Visibility.Visible;
                ZakazaniPregledi.Visibility = Visibility.Visible;
                ZakazaneOperacije.Visibility = Visibility.Visible;
                ZakaziPregled.Visibility = Visibility.Visible;
                MojaTerapija.Visibility = Visibility.Visible;
                TrenutnaTerapija.Visibility = Visibility.Collapsed;
                IstorijaTerapija.Visibility = Visibility.Collapsed;
                Blog.Visibility = Visibility.Visible;
                MojNalog.Visibility = Visibility.Visible;
                MojiPodaci.Visibility = Visibility.Collapsed;
                MojKarton.Visibility = Visibility.Collapsed;
                Podesavanja.Visibility = Visibility.Collapsed;
                ONama.Visibility = Visibility.Visible;
                ONama2.Visibility = Visibility.Collapsed;
                NasiLekari.Visibility = Visibility.Collapsed;
                Kontakt.Visibility = Visibility.Collapsed;

            }
            else if (MojaTerapija.Equals(sender))
            {
                if (TrenutnaTerapija.Visibility == Visibility.Collapsed)
                {
                    ContentBox.Content = new TrenutnaTerapija();
                    PreglediIOperacije.Visibility = Visibility.Visible;
                    ZakazaniPregledi.Visibility = Visibility.Collapsed;
                    ZakazaneOperacije.Visibility = Visibility.Collapsed;
                    ZakaziPregled.Visibility = Visibility.Collapsed;
                    MojaTerapija.Visibility = Visibility.Visible;
                    TrenutnaTerapija.Visibility = Visibility.Visible;
                    IstorijaTerapija.Visibility = Visibility.Visible;
                    Blog.Visibility = Visibility.Visible;
                    MojNalog.Visibility = Visibility.Visible;
                    MojiPodaci.Visibility = Visibility.Collapsed;
                    MojKarton.Visibility = Visibility.Collapsed;
                    Podesavanja.Visibility = Visibility.Collapsed;
                    ONama.Visibility = Visibility.Visible;
                    ONama2.Visibility = Visibility.Collapsed;
                    NasiLekari.Visibility = Visibility.Collapsed;
                    Kontakt.Visibility = Visibility.Collapsed;
                }
                else
                {
                    PreglediIOperacije.Visibility = Visibility.Visible;
                    ZakazaniPregledi.Visibility = Visibility.Collapsed;
                    ZakazaneOperacije.Visibility = Visibility.Collapsed;
                    ZakaziPregled.Visibility = Visibility.Collapsed;
                    MojaTerapija.Visibility = Visibility.Visible;
                    TrenutnaTerapija.Visibility = Visibility.Collapsed;
                    IstorijaTerapija.Visibility = Visibility.Collapsed;
                    Blog.Visibility = Visibility.Visible;
                    MojNalog.Visibility = Visibility.Visible;
                    MojiPodaci.Visibility = Visibility.Collapsed;
                    MojKarton.Visibility = Visibility.Collapsed;
                    Podesavanja.Visibility = Visibility.Collapsed;
                    ONama.Visibility = Visibility.Visible;
                    ONama2.Visibility = Visibility.Collapsed;
                    NasiLekari.Visibility = Visibility.Collapsed;
                    Kontakt.Visibility = Visibility.Collapsed;
                }
            }
            else if (Blog.Equals(sender))
            {
                ContentBox.Content = new Blog();
            }
            else if (MojNalog.Equals(sender))
            {
                if (MojiPodaci.Visibility == Visibility.Collapsed)
                {
                    ContentBox.Content = new Podaci();
                    PreglediIOperacije.Visibility = Visibility.Visible;
                    ZakazaniPregledi.Visibility = Visibility.Collapsed;
                    ZakazaneOperacije.Visibility = Visibility.Collapsed;
                    ZakaziPregled.Visibility = Visibility.Collapsed;
                    MojaTerapija.Visibility = Visibility.Visible;
                    TrenutnaTerapija.Visibility = Visibility.Collapsed;
                    IstorijaTerapija.Visibility = Visibility.Collapsed;
                    Blog.Visibility = Visibility.Visible;
                    MojNalog.Visibility = Visibility.Visible;
                    MojiPodaci.Visibility = Visibility.Visible;
                    MojKarton.Visibility = Visibility.Visible;
                    Podesavanja.Visibility = Visibility.Visible;
                    ONama.Visibility = Visibility.Visible;
                    ONama2.Visibility = Visibility.Collapsed;
                    NasiLekari.Visibility = Visibility.Collapsed;
                    Kontakt.Visibility = Visibility.Collapsed;
                }

                else
                {
                    PreglediIOperacije.Visibility = Visibility.Visible;
                    MojiPodaci.Visibility = Visibility.Collapsed;
                    MojKarton.Visibility = Visibility.Collapsed;
                    Podesavanja.Visibility = Visibility.Collapsed;
                }
            }
            else if (ONama.Equals(sender))
            {
                if (ONama2.Visibility == Visibility.Collapsed)
                {
                    ContentBox.Content = new ONama();
                    PreglediIOperacije.Visibility = Visibility.Visible;
                    ZakazaniPregledi.Visibility = Visibility.Collapsed;
                    ZakazaneOperacije.Visibility = Visibility.Collapsed;
                    ZakaziPregled.Visibility = Visibility.Collapsed;
                    MojaTerapija.Visibility = Visibility.Visible;
                    TrenutnaTerapija.Visibility = Visibility.Collapsed;
                    IstorijaTerapija.Visibility = Visibility.Collapsed;
                    Blog.Visibility = Visibility.Visible;
                    MojNalog.Visibility = Visibility.Visible;
                    MojiPodaci.Visibility = Visibility.Collapsed;
                    MojKarton.Visibility = Visibility.Collapsed;
                    Podesavanja.Visibility = Visibility.Collapsed;
                    ONama.Visibility = Visibility.Visible;
                    ONama2.Visibility = Visibility.Visible;
                    NasiLekari.Visibility = Visibility.Visible;
                    Kontakt.Visibility = Visibility.Visible;
                }

                else
                {
                    ONama.Visibility = Visibility.Visible;
                    ONama2.Visibility = Visibility.Collapsed;
                    NasiLekari.Visibility = Visibility.Collapsed;
                    Kontakt.Visibility = Visibility.Collapsed;
                }

            }
            else if (Podesavanja.Equals(sender))
            {

                ContentBox.Content = new Settings();
            }
            else if (MojKarton.Equals(sender))
            {
                ContentBox.Content = new Karton();
            }
            else if (MojiPodaci.Equals(sender))
            {
                ContentBox.Content = new Podaci();
            }
            else if (IstorijaTerapija.Equals(sender))
            {
                ContentBox.Content = new IstorijaTerapija();
            }
            else if (TrenutnaTerapija.Equals(sender))
            {
                ContentBox.Content = new TrenutnaTerapija();
            }
            else if (Kontakt.Equals(sender))
            {
                ContentBox.Content = new Kontakt();
            }
            else if (NasiLekari.Equals(sender))
            {
                ContentBox.Content = new NasiLekari();
            }
            else if (ZakazaniPregledi.Equals(sender))
            {
                ContentBox.Content = new ZakazaniPregledi();
            }
            else if (ZakazaneOperacije.Equals(sender))
            {
                ContentBox.Content = new ZakazaneOperacije();
            }
            else if (ZakaziPregled.Equals(sender))
            {
                ContentBox.Content = new ZakaziPregled();
            }
            else if (ONama2.Equals(sender))
            {
                ContentBox.Content = new ONama();
            }



        }

        

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (odjava.Equals(sender))
            {
                Window win = new Odjava();
                win.ShowDialog();
            }
            
        }

        private void PreglediIOperacije_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ContentBox.Content = new ZakazaniPregledi();
            PreglediIOperacije.Visibility = Visibility.Visible;
            ZakazaniPregledi.Visibility = Visibility.Visible;
            ZakazaneOperacije.Visibility = Visibility.Visible;
            ZakaziPregled.Visibility = Visibility.Visible;
            MojaTerapija.Visibility = Visibility.Visible;
            TrenutnaTerapija.Visibility = Visibility.Collapsed;
            IstorijaTerapija.Visibility = Visibility.Collapsed;
            Blog.Visibility = Visibility.Visible;
            MojNalog.Visibility = Visibility.Visible;
            MojiPodaci.Visibility = Visibility.Collapsed;
            MojKarton.Visibility = Visibility.Collapsed;
            Podesavanja.Visibility = Visibility.Collapsed;
            ONama.Visibility = Visibility.Visible;
            ONama2.Visibility = Visibility.Collapsed;
            NasiLekari.Visibility = Visibility.Collapsed;
            Kontakt.Visibility = Visibility.Collapsed;
        }


        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (pocetna.Equals(sender))
            {
                ContentBox.Content = new Homepage();
            }
           
        }



        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (MojiPodaci.Visibility == Visibility.Collapsed)
            {
                ContentBox.Content = new Podaci();
                PreglediIOperacije.Visibility = Visibility.Visible;
                ZakazaniPregledi.Visibility = Visibility.Collapsed;
                ZakazaneOperacije.Visibility = Visibility.Collapsed;
                ZakaziPregled.Visibility = Visibility.Collapsed;
                MojaTerapija.Visibility = Visibility.Visible;
                TrenutnaTerapija.Visibility = Visibility.Collapsed;
                IstorijaTerapija.Visibility = Visibility.Collapsed;
                Blog.Visibility = Visibility.Visible;
                MojNalog.Visibility = Visibility.Visible;
                MojiPodaci.Visibility = Visibility.Visible;
                MojKarton.Visibility = Visibility.Visible;
                Podesavanja.Visibility = Visibility.Visible;
                ONama.Visibility = Visibility.Visible;
                ONama2.Visibility = Visibility.Collapsed;
                NasiLekari.Visibility = Visibility.Collapsed;
                Kontakt.Visibility = Visibility.Collapsed;
            }

            else
            {
                PreglediIOperacije.Visibility = Visibility.Visible;
                MojiPodaci.Visibility = Visibility.Collapsed;
                MojKarton.Visibility = Visibility.Collapsed;
                Podesavanja.Visibility = Visibility.Collapsed;
            }
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (MojiPodaci.Visibility == Visibility.Collapsed)
            {
                ContentBox.Content = new Settings();
                PreglediIOperacije.Visibility = Visibility.Visible;
                ZakazaniPregledi.Visibility = Visibility.Collapsed;
                ZakazaneOperacije.Visibility = Visibility.Collapsed;
                ZakaziPregled.Visibility = Visibility.Collapsed;
                MojaTerapija.Visibility = Visibility.Visible;
                TrenutnaTerapija.Visibility = Visibility.Collapsed;
                IstorijaTerapija.Visibility = Visibility.Collapsed;
                Blog.Visibility = Visibility.Visible;
                MojNalog.Visibility = Visibility.Visible;
                MojiPodaci.Visibility = Visibility.Visible;
                MojKarton.Visibility = Visibility.Visible;
                Podesavanja.Visibility = Visibility.Visible;
                ONama.Visibility = Visibility.Visible;
                ONama2.Visibility = Visibility.Collapsed;
                NasiLekari.Visibility = Visibility.Collapsed;
                Kontakt.Visibility = Visibility.Collapsed;
            }

            else
            {
                PreglediIOperacije.Visibility = Visibility.Visible;
                MojiPodaci.Visibility = Visibility.Collapsed;
                MojKarton.Visibility = Visibility.Collapsed;
                Podesavanja.Visibility = Visibility.Collapsed;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ONama2.Visibility == Visibility.Collapsed)
            {
                ContentBox.Content = new ONama();
                PreglediIOperacije.Visibility = Visibility.Visible;
                ZakazaniPregledi.Visibility = Visibility.Collapsed;
                ZakazaneOperacije.Visibility = Visibility.Collapsed;
                ZakaziPregled.Visibility = Visibility.Collapsed;
                MojaTerapija.Visibility = Visibility.Visible;
                TrenutnaTerapija.Visibility = Visibility.Collapsed;
                IstorijaTerapija.Visibility = Visibility.Collapsed;
                Blog.Visibility = Visibility.Visible;
                MojNalog.Visibility = Visibility.Visible;
                MojiPodaci.Visibility = Visibility.Collapsed;
                MojKarton.Visibility = Visibility.Collapsed;
                Podesavanja.Visibility = Visibility.Collapsed;
                ONama.Visibility = Visibility.Visible;
                ONama2.Visibility = Visibility.Visible;
                NasiLekari.Visibility = Visibility.Visible;
                Kontakt.Visibility = Visibility.Visible;
            }

            else
            {
                ONama.Visibility = Visibility.Visible;
                ONama2.Visibility = Visibility.Collapsed;
                NasiLekari.Visibility = Visibility.Collapsed;
                Kontakt.Visibility = Visibility.Collapsed;
            }
        }

        private void fillForm()
        {
            name.Text = App.patient.Name;
            surname.Text = App.patient.Surname;
            //if (App.patient.profileImageSource != null)
                //imgPhoto.Source = new BitmapImage(new Uri(App.patient.profileImageSource));
        }
    }

    
}
