﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.users.doctor;

namespace Klinika.Service.UsersServices.DoctorServices
{
    public interface IAllergyService
    {
        Boolean CreateAllergy(Allergy allergy);
        Boolean EditAllergy(Model.users.doctor.Allergy allergy);
        List<Model.users.doctor.Allergy> GetAllAllergies();
        Model.users.doctor.Allergy GetAllergy(String id);
        Boolean DeleteAllergy(Model.users.doctor.Allergy allergy);

    }
}
