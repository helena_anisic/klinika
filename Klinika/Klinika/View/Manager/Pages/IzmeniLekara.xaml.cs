﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.UserController;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for IzmeniLekara.xaml
    /// </summary>
    public partial class IzmeniLekara : Page
    {
        public Model.users.user.Doctor doctor;
        public IzmeniLekara(Model.users.user.Doctor doctor)
        {
            this.doctor = doctor;
            InitializeComponent();
            id.IsEnabled = false;
            if (doctor != null)
            {
                id.Text = doctor.Id;
                name.Text = doctor.Name;
                surname.Text = doctor.Surname;
                dateOfBirth.SelectedDate = doctor.DateOfBirth;
                adress.Text = doctor.Address;
                country.ItemsSource = App.LocationController.GetAllCountries();
                if (doctor.Country != null)
                {
                    int i = 0;
                    foreach (Country c in country.Items)
                    {
                        if (c.Code.Equals(doctor.Country.Code))
                        {
                            country.SelectedIndex = i;
                        }
                        i++;
                    }
                    city.ItemsSource = App.LocationController.GetAllCitiesByCountryName(doctor.Country.Name);
                    if (doctor.City != null)
                    {
                        i = 0;
                        foreach (City c in city.Items)
                        {
                            if (c.PostalCode == doctor.City.PostalCode)
                            {
                                city.SelectedIndex = i;
                            }
                            i++;
                        }
                    }
                }
                phone.Text = doctor.PhoneNumber;
                email.Text = doctor.Email;

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new SpisakLekara());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            doctor.Id = id.Text;
            doctor.Name = name.Text;
            doctor.Surname = surname.Text;
            doctor.DateOfBirth = dateOfBirth.SelectedDate.Value;
            doctor.Address = adress.Text;
            doctor.Country = (Country)country.SelectedItem;
            doctor.City = (City)city.SelectedItem;
            doctor.PhoneNumber = phone.Text;
            doctor.Email = email.Text;
            doctor.Username = email.Text;

            App.DoctorController.EditDoctor(doctor);

            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new SpisakLekara());
        }

        private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            city.ItemsSource = App.LocationController.GetAllCitiesByCountryName(((Country)country.SelectedItem).Name);
        }
    }
}
