﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Klinika.View.Manager.Data;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for MedicinskaOpremaIAparati.xaml
    /// </summary>
    public partial class MedicinskaOpremaIAparati : Page
    {
        public MedicinskaOpremaIAparati()
        {
            InitializeComponent();
            DataContext = new OpremaView(null);
            MedOprema.HeadersVisibility = DataGridHeadersVisibility.Column;
        }

        private void Button_Click_Dodaj_Opremu(object sender, RoutedEventArgs e)
        {
            Window w = new DodajOpremu();
            w.ShowDialog();
            DataContext = new OpremaView(null);
        }

        private void Button_Click_Izmeni_Opremu(object sender, RoutedEventArgs e)
        {
            Equipment equipment = (Equipment)MedOprema.SelectedItem;
            Window w;
            if (equipment == null)
            {
                w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da izmenite!", "Ok");
                w.ShowDialog();
                return;
            }
            w = new IzmeniOpremu(equipment);
            w.ShowDialog();
            DataContext = new OpremaView(null);
        }

        private void Button_Click_Obrisi_Opremu(object sender, RoutedEventArgs e)
        {
            Equipment equipment = (Equipment)MedOprema.SelectedItem;
            if (equipment == null)
            {
                Window w = new MessageBoxOk("Oprez", "Morate obeleziti opremu u tabeli koju\nzelite da obrisete!", "Ok");
            }
            else
            {
                MessageBoxYesNo mb = new MessageBoxYesNo("Obriši opremu", "Da li želite da obrišete opremu\n" + equipment.name + " id:" + equipment.id + "?", "Odustani", "Obriši");
                mb.ShowDialog();
                if (mb.GetCloseStatus() == 1)
                {
                    Room room = App.RoomController.GetRoomById("Magacin");
                    if (room == null)
                        return;
                    foreach (Equipment ee in room.equipment)
                    {
                        if (ee.id.Equals(equipment.id))
                        {
                            room.equipment.Remove(ee);
                            break;
                        }
                    }
                    App.RoomController.EditRoom(room);
                    DataContext = new OpremaView(null);
                }

            }
        }

        private void Button_Click_Premesti_Opremu(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new PremestiOpremu());
        }
    }
}
