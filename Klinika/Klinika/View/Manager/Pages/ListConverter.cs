﻿using Model.Storage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Klinika.View.Manager.Pages
{
    public class ListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<IngredientQuantity> ingredients = (List<IngredientQuantity>)value;
            String ret = "";
            if (ingredients == null)
                return ret;
            foreach (IngredientQuantity ingredient in ingredients)
            {
                ret += ingredient.ingredient.name + " " + ingredient.quantity + "mg;  ";
            }
            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
