﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.SecretaryServices
{
    public interface ISecretaryService
    {
        List<Secretary> GetAllSecretaries();
        Boolean EditSecretary(Secretary secretary);
        Boolean CreateSecretary(Secretary secretary);
        Boolean DeleteSecretary(Secretary secretary);
        Secretary GetSecretaryByAccount(Account account);

    }
}
