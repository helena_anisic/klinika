// File:    ExaminationAppointmentRepository.cs
// Author:  helen
// Created: Wednesday, May 13, 2020 4:31:59 PM
// Purpose: Definition of Class ExaminationAppointmentRepository

using Klinika.Repository;
using Model.users.patient;
using Model.users.user;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.UsersRepositories.PatientRepositories
{
    public class ExaminationAppointmentRepository : Repository<ExaminationAppointment>
    {
        public static String path = @"..\..\Resources\Data\Appointments.txt";

    }
}