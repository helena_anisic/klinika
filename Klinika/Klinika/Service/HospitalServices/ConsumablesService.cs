﻿using Klinika.Repository.HospitalRepositories;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.HospitalServices
{
    public class ConsumablesService : IConsumablesService
    {
        private ConsumablesRepository _consumablesRepository;

        public ConsumablesService(ConsumablesRepository consumablesRepository)
        {
            _consumablesRepository = consumablesRepository;
        }

        public Boolean CreateConsumable(EquipmentQuantity equipment)
        {
            List<EquipmentQuantity> equipmentList = _consumablesRepository.GetAll(ConsumablesRepository.path);
            if (equipmentList == null)
            {
                equipmentList = new List<EquipmentQuantity>();
            }
            equipmentList.Add(equipment);
            _consumablesRepository.Save(equipmentList, ConsumablesRepository.path);
            return true;
        }

        public List<EquipmentQuantity> GetAllConsumables() => _consumablesRepository.GetAll(ConsumablesRepository.path);

        public Boolean EditConsumable(EquipmentQuantity equipment)
        {
            if (DeleteConsumable(equipment) == false)
                return false;
            return CreateConsumable(equipment);
        }

        public Boolean DeleteConsumable(EquipmentQuantity equipment)
        {
            List<EquipmentQuantity> equipmentList = _consumablesRepository.GetAll(ConsumablesRepository.path);

            if (equipmentList == null)
                return false;

            foreach (EquipmentQuantity e in equipmentList)
            {
                if (e.equipmentB.id.Equals(equipment.equipmentB.id))
                {
                    equipmentList.Remove(e);
                    break;
                }
            }

            return _consumablesRepository.Save(equipmentList, ConsumablesRepository.path);
        }
    }
}
