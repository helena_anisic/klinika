// File:    Question.cs
// Author:  helen
// Created: Sunday, April 19, 2020 7:22:05 PM
// Purpose: Definition of Class Question

using System;

namespace Model.users.patient
{
   public class Question
   {
      public String QuestionText { get; set; }
      public int QuestionGrade { get; set; }
   
   }
}