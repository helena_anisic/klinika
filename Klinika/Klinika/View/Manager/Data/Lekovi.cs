﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Manager.Data
{
    public class Lekovi
    {
        public int ID { get; set; }
        public string Naziv { get; set; }
        public string Proizvodjac { get; set; }
        public string Tip { get; set; }
        public int Kolicina { get; set; }
        public string Sastav { get; set; }
        public string Namena { get; set; }
        public string Validiran { get; set; }
    }

    public class OrdersLekovi : ObservableCollection<Lekovi>
    {
        public OrdersLekovi()
        {
            this.Add(new Lekovi
            {
                ID = 242,
                Naziv = "Brufen 500mg",
                Proizvodjac = "Galenika",
                Tip = "Tablete",
                Kolicina = 30,
                Sastav = "Ibuprofen",
                Namena = "bol u grlu",
                Validiran = "Ceka na validaciju"
            });
            this.Add(new Lekovi
            {
                ID = 243,
                Naziv = "Brufen 500mg",
                Proizvodjac = "Galenika",
                Tip = "Tablete",
                Kolicina = 30,
                Sastav = "Ibuprofen",
                Namena = "bol u grlu",
                Validiran = "Ceka na validaciju"
            });
            this.Add(new Lekovi
            {
                ID = 102,
                Naziv = "Metafex",
                Proizvodjac = "Goodwill",
                Tip = "Tablete",
                Kolicina = 20,
                Sastav = "Ibuprofen; Paracetamol",
                Namena = "bol u grlu",
                Validiran = "Odobren"
            });
            this.Add(new Lekovi
            {
                ID = 103,
                Naziv = "Metafex",
                Proizvodjac = "Goodwill",
                Tip = "Tablete",
                Kolicina = 20,
                Sastav = "Ibuprofen; Paracetamol",
                Namena = "bol u grlu",
                Validiran = "Odobren"
            });
            this.Add(new Lekovi
            {
                ID = 104,
                Naziv = "Metafex",
                Proizvodjac = "Goodwill",
                Tip = "Tablete",
                Kolicina = 20,
                Sastav = "Ibuprofen; Paracetamol",
                Namena = "bol u grlu",
                Validiran = "Odobren"
            });
        }
    }
}
