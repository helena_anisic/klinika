﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for AddSurgery.xaml
    /// </summary>
    public partial class AddSurgery : Window
    {

        private Surgery surgery;
        private List<Model.users.user.Doctor> Doctors;
        private List<Model.users.user.Patient> Patients;

        public DateTime Date { get; set; }

        public AddSurgery()
        {
            InitializeComponent();
            surgery = new Surgery();
            surgery.StartDateTime = new DateTime();
            surgery.EndDateTime = new DateTime();
            Doctors = App.DoctorController.GetAllDoctors();
            Patients = App.PatientController.GetAllPatients();

            DoctorsComboBox.ItemsSource = Doctors;
            PatientsComboBox.ItemsSource = Patients;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            surgery.Id = surgeryId.Text;
            surgery.Cause = typeOfSurgery.Text;
            surgery.Urgency = false;
            surgery.PatientId = ((Model.users.user.Patient)PatientsComboBox.SelectedItem).Id;
            surgery.DoctorId = ((Model.users.user.Doctor)DoctorsComboBox.SelectedItem).Id;
            surgery.RoomId = roomId.Text;


            if (date.SelectedDate.HasValue)
            {
                string dateCalendar = date.SelectedDate.Value.ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                Date = (DateTime)date.SelectedDate;
            }
            DateTime start = (DateTime)startTime.Value;
            surgery.StartDateTime = new DateTime(Date.Year, Date.Month, Date.Day, start.Hour, start.Minute, 0);
            DateTime end = (DateTime)endTime.Value;
            surgery.EndDateTime = new DateTime(Date.Year, Date.Month, Date.Day, end.Hour, end.Minute, 0);

            App.SurgeryController.CreateSurgery(surgery);

            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
