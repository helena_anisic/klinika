﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Manager.Data
{
}
public class MedicinskaOprema
{
    public int ID { get; set; }
    public string Naziv { get; set; }
}

public class Orders : ObservableCollection<MedicinskaOprema>
{
    public Orders()
    {
        this.Add(new MedicinskaOprema
        {
            ID = 101,
            Naziv = "Sto"
        });
        this.Add(new MedicinskaOprema

        {
            ID = 201,
            Naziv = "Stolice"
        });
        this.Add(new MedicinskaOprema

        {
            ID = 244,
            Naziv = "Kanta"
        });
        this.Add(new MedicinskaOprema
        {
            ID = 155,
            Naziv = "Sto"
        });
        this.Add(new MedicinskaOprema

        {
            ID = 318,
            Naziv = "Stolice"
        });
        this.Add(new MedicinskaOprema
        {
            ID = 812,
            Naziv = "Sto"
        });
        this.Add(new MedicinskaOprema

        {
            ID = 444,
            Naziv = "Kanta"
        });
        this.Add(new MedicinskaOprema
        {
            ID = 607,
            Naziv = "Sto"
        });
        this.Add(new MedicinskaOprema

        {
            ID = 198,
            Naziv = "Stolice"
        });
        this.Add(new MedicinskaOprema

        {
            ID = 654,
            Naziv = "Kanta"
        });
    }
}
