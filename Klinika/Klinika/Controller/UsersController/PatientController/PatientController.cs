// File:    PatientController.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 6:08:24 PM
// Purpose: Definition of Class PatientController

using Klinika.Controller.UsersController.PatientController;
using Klinika.Service.UsersServices.PatientServices;
using Model.users.user;
using Service.UsersServices.PatientServices;
using System;
using System.Collections.Generic;

namespace Controller.UsersController.PatientController
{
    public class PatientController : IPatientFileController
    {
        private IPatientService _patientService;
        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        public PatientController()
        {
        }

        public Boolean CreatePatient(Patient patient)
        {
            if (patient == null)
            {
                return false;
            }
            return _patientService.CreatePatient(patient);
        }

        public Boolean EditPatient(Patient patient) => _patientService.EditPatient(patient);

        public Boolean DeletePatient(Patient patient) => _patientService.DeletePatient(patient);


        public Patient GetPatientById(String id) => _patientService.GetPatientById(id);

        public Patient GetPatientByAccount(Account account) => _patientService.GetPatientByAccount(account);

        public List<Patient> GetAllPatients() => _patientService.GetAllPatients();
    }
}