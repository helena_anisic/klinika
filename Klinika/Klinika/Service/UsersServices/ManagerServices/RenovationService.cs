// File:    RenovationService.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:23:06 PM
// Purpose: Definition of Class RenovationService

using Klinika.Service.UsersServices.ManagerServices;
using Model.users.manager;
using Repository.UsersRepositories.ManagerRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.ManagerServices
{
    public class RenovationService : IRenovationService
    {
        private RenovationRepository _renovationRepository;

        public RenovationService(RenovationRepository renovationRepository)
        {
            _renovationRepository = renovationRepository;
        }
        public Boolean CreateRenovation(Renovation renovation)
        {
            List<Renovation> renovations = _renovationRepository.GetAll(RenovationRepository.path);
            if (renovations == null)
            {
                renovations = new List<Renovation>();
            }
            renovations.Add(renovation);
            _renovationRepository.Save(renovations, RenovationRepository.path);
            return true;
        }

        public Boolean EditRenovation(Renovation renovation)
        {
            if (DeleteRenovation(renovation) == false)
                return false;
            return CreateRenovation(renovation);
        }

        public Boolean DeleteRenovation(Renovation renovation)
        {
            List<Renovation> renovations = _renovationRepository.GetAll(RenovationRepository.path);

            if (renovations == null)
                return false;

            foreach (Renovation r in renovations)
            {
                if (r.Id.Equals(renovation.Id))
                {
                    renovations.Remove(r);
                    break;
                }
            }

            return _renovationRepository.Save(renovations, RenovationRepository.path);
        }

        public List<Renovation> GetAllRenovations()
        {
            return _renovationRepository.GetAll(RenovationRepository.path);
        }
    }
}