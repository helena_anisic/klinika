// File:    ArticleController.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 4:57:19 PM
// Purpose: Definition of Class ArticleController

using Klinika.Controller.BlogFeedbackController;
using Klinika.Service.BlogFeedbackServices;
using Model.users.doctor;
using Service.BlogFeedbackServices;
using System;
using System.Collections.Generic;

namespace Controller.BlogFeedbackController
{
    public class ArticleController : IArticleController
    {
        private IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }
        public Boolean CreateBlogArticle(BlogArticle blogArticle) => _articleService.CreateBlogArticle(blogArticle);

        public BlogArticle GetBlogArticleById(String id) => _articleService.GetBlogArticleById(id);

        public List<BlogArticle> GetAllBlogArticles() => _articleService.GetAllBlogArticles();

    }
}