﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Model.users.doctor;
using Syncfusion.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for ResultOfExaminationView.xaml
    /// </summary>
    public partial class ResultOfExaminationView : Window
    {
        public Model.users.user.Patient patient { get; set; }
        public ResultOfExaminationView(Model.users.user.Patient p)
        {
            InitializeComponent();
            patient = p;
            patientName.Text = p.Name;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.IsEnabled = false;

                PrintDialog printDialog = new PrintDialog();
                if (printDialog.ShowDialog() == true)
                {
                    printDialog.PrintVisual(print, "Invoice");
                }
            }
            finally
            {
                this.IsEnabled = true;
            }

        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            ResultOfMedicalExamination res = new ResultOfMedicalExamination();
            Therapy therapy = new Therapy();
            res.Id = patient.Id;
            res.Symptoms = symptoms.Text;
            res.Diagnosis = diagnosis.Text;
            res.Content = print.Text;
            res.DateOfDiagnosis = DateTime.Now;

            therapy.Id = res.Id;
            therapy.StartDate = startDate.SelectedDate.ToDateTime();
            therapy.EndDate = endDate.SelectedDate.ToDateTime();
            therapy.Description = description.Text;
            therapy.WayToUseMedicine = wayToUse.Text;
            therapy.Quantity = double.Parse(quantity.Text);
            therapy.MedicineName = medicineName.Text;

            App.PatientFileController.CreateResultOfExamination(res);
            App.PatientFileController.CreateTherapy(therapy);

            Close();
        }
    }
}
