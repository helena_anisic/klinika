﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.BlogFeedbackController
{
    public interface IFeedbackController
    {
        Boolean CreateFeedback(Feedback feedback);
    }
}
