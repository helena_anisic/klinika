﻿using Controller.UsersController.PatientController;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for TerminiSale.xaml
    /// </summary>
    public partial class TerminiSale : Page
    {
        public TerminiSale()
        {
            InitializeComponent();
            List<ExaminationAppointment> pac = App.ExaminationAppointmentController.GetAllAppointments();
            /*pac.Add(new Lekari { imeIprezime = "Petar Petrovic", termini = "06:00", soba = "201", hitan = true });
            pac.Add(new Lekari { imeIprezime = "Marko Markovic", termini = "07:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Jovan Jovic", termini = "08:00", soba = "201", hitan = true });
            pac.Add(new Lekari { imeIprezime = "Marina Jankovic", termini = "09:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Jelena Jovanovic", termini = "10:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Marko Nikolic", termini = "11:00", soba = "201", hitan = true });
            pac.Add(new Lekari { imeIprezime = "Petar Marinkovic", termini = "12:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Marko Simic", termini = "13:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Petar Petrovic", termini = "14:00", soba = "201", hitan = true });
            pac.Add(new Lekari { imeIprezime = "Marko Markovic", termini = "07:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Jovan Jovic", termini = "16:00", soba = "201", hitan = true });
            pac.Add(new Lekari { imeIprezime = "Marina Jankovic", termini = "17:00", soba = "201", hitan = true });
            pac.Add(new Lekari { imeIprezime = "Jelena Jovanovic", termini = "18:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Marko Nikolic", termini = "19:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Petar Marinkovic", termini = "20:00", soba = "201", hitan = false });
            pac.Add(new Lekari { imeIprezime = "Marko Simic", termini = "21:00", soba = "201", hitan = false });*/
            dataGrid.ItemsSource = pac;
            
        }

        

        class Lekari
        {
            public string termini { get; set; }
            public string soba { get; set; }
            public bool hitan { get; set; }
            public string imeIprezime { get; set; }
        }
        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new UpravljanjeRasporedom();
        }

        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ExaminationAppointment ea = (ExaminationAppointment)dataGrid.SelectedItem;
            Application.Current.MainWindow.Content = new PrikazTermina(ea);
        }
    }
}
