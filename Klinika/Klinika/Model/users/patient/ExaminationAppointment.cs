// File:    ExaminationAppointment.cs
// Author:  nikol
// Created: Monday, April 20, 2020 12:15:58 PM
// Purpose: Definition of Class ExaminationAppointment

using Model.users.manager;
using Model.users.user;
using System;

namespace Model.users.patient
{
    public class ExaminationAppointment : Term
    {
        public String Id { get; set; }
        public String Cause { get; set; }
        public Boolean Urgency { get; set; }
        public String PatientCondition { get; set; }
        public String DoctorId { get; set; }
        public String PatientId { get; set; }

        public override string ToString()
        {
            return StartDateTime.ToString("HH:mm") + " sala " + RoomId;
        }
    }
}