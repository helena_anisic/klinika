﻿using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.DoctorServices
{
    public interface ISurgeryService
    {
        Boolean CreateSurgery(Model.users.patient.Surgery surgery);
        Boolean EditSurgery(Model.users.patient.Surgery surgery);
        Boolean DeleteSurgery(Model.users.patient.Surgery surgery);
        Model.users.patient.Surgery GetSurgeryById(String Id);
        List<Surgery> GetAllSugeries();
        List<Surgery> GetAllSurgeriesForDoctor(String doctorId);
        List<Surgery> GetAllSurgeriesForPatient(String patientId);
        List<Surgery> GetAllSurgeriesForPatient(String patientId, DateTime date);

    }
}
