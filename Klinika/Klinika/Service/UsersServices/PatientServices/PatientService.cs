// File:    PatientService.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 6:08:24 PM
// Purpose: Definition of Class PatientService

using Klinika.Service.UsersServices.PatientServices;
using Model.users.user;
using Repository.UsersRepositories.PatientRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.PatientServices
{
    public class PatientService : IPatientService
    {
        private PatientRepository _patientRepository;
        public PatientService(PatientRepository patientRepository)
        {
            _patientRepository = patientRepository;
        }
        public Boolean CreatePatient(Patient patient)
        {
            List<Patient> patients = _patientRepository.GetAll(PatientRepository.path);
            if (patients == null)
            {
                patients = new List<Patient>();
            }
            patients.Add(patient);
            _patientRepository.Save(patients, PatientRepository.path);
            return true;
        }

        public Boolean EditPatient(Patient patient)
        {
            if (DeletePatient(patient) == false)
                return false;
            return CreatePatient(patient);
        }

        public Boolean DeletePatient(Patient patient)
        {
            List<Patient> patients = _patientRepository.GetAll(PatientRepository.path);

            if (patients == null)
                return false;

            foreach (Patient p in patients)
            {
                if (p.Id.Equals(patient.Id))
                {
                    patients.Remove(p);
                    break;
                }
            }

            return _patientRepository.Save(patients, PatientRepository.path);
        }

        public Patient GetPatientByAccount(Account account)
        {
            List<Patient> patients = _patientRepository.GetAll(PatientRepository.path);
            if (patients == null)
                return null;
            foreach (Patient patient in patients)
            {
                if (patient.Username == null || patient.Password == null)
                    continue;
                if (patient.Username.Equals(account.Username) && patient.Password.Equals(account.Password))
                {
                    return patient;
                }
            }
            return null;
        }

        public Patient GetPatientById(String id)
        {
            List<Patient> patients = _patientRepository.GetAll(PatientRepository.path);
            if (patients == null)
                return null;
            foreach (Patient patient in patients)
            {
                if (patient.Id.Equals(id))
                {
                    return patient;
                }
            }
            return null;
        }

        public List<Patient> GetAllPatients() => _patientRepository.GetAll(PatientRepository.path);
    }
}