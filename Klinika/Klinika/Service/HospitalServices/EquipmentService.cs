// File:    EquipmentService.cs
// Author:  nikol
// Created: Wednesday, May 13, 2020 4:32:03 PM
// Purpose: Definition of Class EquipmentService

using Klinika.Controller.HospitalController;
using Klinika.Service.HospitalServices;
using Model.Hospital;
using Model.Storage;
using Repository.HospitalRepositories;
using System;
using System.Collections.Generic;

namespace Service.HospitalServices
{
    public class EquipmentService : IEquipmentService
    {
        private EquipmentRepository _equipmentRepository;
        private IRoomService _roomService;

        public EquipmentService(EquipmentRepository equipmentRepository, IRoomService roomService)
        {
            _equipmentRepository = equipmentRepository;
            _roomService = roomService;
        }
        public Boolean CreateEquipment(Equipment equipment)
        {
            List<Equipment> equipmentList = _equipmentRepository.GetAll(EquipmentRepository.path);
            if (equipmentList == null)
            {
                equipmentList = new List<Equipment>();
            }
            equipmentList.Add(equipment);
            _equipmentRepository.Save(equipmentList, EquipmentRepository.path);
            return true;
        }

        public List<Equipment> GetAllEquipment() => _equipmentRepository.GetAll(EquipmentRepository.path);
 

        public Boolean EditEquipment(Equipment equipment)
        {
            if (DeleteEquipment(equipment) == false)
                return false;
            return CreateEquipment(equipment);
        }

        public Boolean DeleteEquipment(Equipment equipment)
        {
            List<Equipment> equipmentList = _equipmentRepository.GetAll(EquipmentRepository.path);

            if (equipmentList == null)
                return false;

            foreach (Equipment e in equipmentList)
            {
                if (e.id.Equals(equipment.id))
                {
                    equipmentList.Remove(e);
                    break;
                }
            }

            return _equipmentRepository.Save(equipmentList, EquipmentRepository.path);
        }


        public Boolean RemoveAllEquipmentFromRoom(Room room)
        {
            if (room == null)
                return false;
            if (room.equipment == null)
                return false;
            List<Equipment> equipmentList = _equipmentRepository.GetAll(EquipmentRepository.path);
            foreach (Equipment equipment in room.equipment)
            {
                equipmentList.Add(equipment);
            }
            room.equipment = null;
            _roomService.EditRoom(room);
            return _equipmentRepository.Save(equipmentList, EquipmentRepository.path);
        }

    }
}