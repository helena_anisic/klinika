﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for MojNalog.xaml
    /// </summary>
    public partial class MojNalog : Page
    {
        public MojNalog()
        {
            InitializeComponent();
            if (App.Pomoc == true)
                toggle.IsChecked = true;
            else
                toggle.IsChecked = false;
            if (App.manager != null)
            {
                name.Text = App.manager.Name + " " + App.manager.Surname;
                dateOfBirth.Text = App.manager.DateOfBirth.ToString("dd/MM/yyyy");
                if (App.manager.City != null && App.manager.Country != null)
                    place.Text = App.manager.City.Name + ", " + App.manager.Country.Name;
                adress.Text = App.manager.Address;
                phone.Text = App.manager.PhoneNumber;
                email.Text = App.manager.Email;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HomePage hp = (HomePage)Application.Current.MainWindow.Content;
            hp.setContentBox(new MojNalogIzmena());
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Window w = new IzmeniLozinku();
            w.ShowDialog();
        }

        private void Toggle_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Toggle_Checked(object sender, RoutedEventArgs e)
        {
            App.Pomoc = true;
        }

        private void Toggle_Unchecked(object sender, RoutedEventArgs e)
        {
            App.Pomoc = false;
        }
    }
}
