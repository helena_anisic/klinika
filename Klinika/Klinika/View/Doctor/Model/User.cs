﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Doctor.Model
{
    [Serializable]
    public class User
    {
        public User()
        {
        }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ID { get; set; }
        public string DateOfBirth { get; set; }
        public string MobilePhone { get; set; }
        public string Phone { get; set; }
        public string EMail { get; set; }
        public string ResidentialAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

    }
}
