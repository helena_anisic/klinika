// File:    ArticleRepository.cs
// Author:  nikol
// Created: Thursday, May 28, 2020 4:57:19 PM
// Purpose: Definition of Class ArticleRepository

using Klinika.Repository;
using Model.users.doctor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository.BlogFeedbackRepositories
{
    public class ArticleRepository : Repository<BlogArticle>
    {
        public static String path = @"..\..\Resources\Data\BlogArticle.txt";
    }
}