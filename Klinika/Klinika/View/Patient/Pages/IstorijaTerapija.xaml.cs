﻿using Klinika.Controller.UsersController.DoctorController;
using Klinika.Data;
using Klinika.i18N;
using Klinika.View.Patient;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for IstorijaTerapija.xaml
    /// </summary>
    public partial class IstorijaTerapija : Page
    {
        public Therapy therapy;

        public IstorijaTerapija()
        {
            InitializeComponent();
            List<Therapy> sveTerapije = App.TherapyController.GetAllTherapies();
            List<Therapy> istorijaTerapije = new List<Therapy>();
            foreach (Therapy terapija in sveTerapije)
            {
                if (terapija.EndDate < DateTime.Now)
                {
                    istorijaTerapije.Add(terapija);
                }
            }
            MedOprema.ItemsSource = istorijaTerapije;
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
        }

        private void MedOprema_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            therapy = (e.OriginalSource as FrameworkElement).DataContext as Therapy;
        }

        private void PackIconMaterial_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Window win = new TerapijaD(therapy);
            win.ShowDialog();
        }
    }
}
