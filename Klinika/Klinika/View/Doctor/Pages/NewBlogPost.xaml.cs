﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Controller.BlogFeedbackController;
using Model.users.doctor;
using Repository.BlogFeedbackRepositories;
using Service.BlogFeedbackServices;

namespace Klinika.View.Doctor.Pages
{
    /// <summary>
    /// Interaction logic for NewBlogPost.xaml
    /// </summary>
    public partial class NewBlogPost : Window
    {
        public BlogArticle BlogPost;
        public NewBlogPost()
        {
            InitializeComponent();
            BlogPost = new BlogArticle();
        }

        private void btnPublish_Click(object sender, RoutedEventArgs e)
        {
            BlogPost.Content = Subject.Text;
            BlogPost.Title = Subject.Text;
            BlogPost.PublishingDate = new DateTime();

            App.ArticleController.CreateBlogArticle(BlogPost);
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
