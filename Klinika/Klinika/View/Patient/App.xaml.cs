﻿using Controller.BlogFeedbackController;
using Controller.HospitalController;
using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Controller.UsersController.PatientController;
using Controller.UsersController.SecretaryController;
using Controller.UsersController.UserController;
using Klinika.Controller.BlogFeedbackController;
using Klinika.Controller.HospitalController;
using Klinika.Controller.UsersController.DoctorController;
using Klinika.Controller.UsersController.ManagerController;
using Klinika.Controller.UsersController.PatientController;
using Klinika.Controller.UsersController.SecretaryController;
using Klinika.Controller.UsersController.UserController;
using Klinika.Repository.HospitalRepositories;
using Klinika.Repository.UsersRepositories.DoctorRepositories;
using Klinika.Service.HospitalServices;
using Klinika.Service.UsersServices.DoctorServices;
using Model.users.patient;
using Model.users.user;
using Repository.BlogFeedbackRepositories;
using Repository.HospitalRepositories;
using Repository.UsersRepositories.DoctorRepositories;
using Repository.UsersRepositories.ManagerRepositories;
using Repository.UsersRepositories.PatientRepositories;
using Repository.UsersRepositories.SecretaryRepositories;
using Repository.UsersRepositories.UserRepository;
using Service.BlogFeedbackServices;
using Service.HospitalServices;
using Service.UsersServices.DoctorServices;
using Service.UsersServices.ManagerServices;
using Service.UsersServices.PatientServices;
using Service.UsersServices.SecretaryServices;
using Service.UsersServices.UserServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Klinika
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Manager manager;
        public static Patient patient;
        public static Secretary secretary;
        public static Doctor doctor;
        public static bool Pomoc = true;
        

        void App_Startup(object sender, StartupEventArgs e)

        {
            //View.Manager.ManagerWindow mw = new View.Manager.ManagerWindow();
            //mw.Show();

            //View.Secretary.SecretaryWindow sw = new View.Secretary.SecretaryWindow();
            //sw.Show();

            //Klinika.View.Doctor.Pages.FirstWindow firstWindow = new View.Doctor.Pages.FirstWindow();
            //firstWindow.Show();
            
        }
        public App()
        {
            ArticleRepository articleRepository = new ArticleRepository();
            FeedbackRepository feedbackRepository = new FeedbackRepository();
            SurveyRepository surveyRepository = new SurveyRepository();
            EquipmentRepository equipmentRepository = new EquipmentRepository();
            ConsumablesRepository consumablesRepository = new ConsumablesRepository();
            RoomRepository roomRepository = new RoomRepository();
            RoomTypeRepository roomTypeRepository = new RoomTypeRepository();
            IngredientRepository ingredientRepository = new IngredientRepository();
            MedicineRepository medicineRepository = new MedicineRepository();
            MedicineTypeRepository medicineTypeRepository = new MedicineTypeRepository();
            UserRepository userRepository = new UserRepository();
            CityRepository cityRepository = new CityRepository();
            CountryRepository countryRepository = new CountryRepository();
            SecretaryRepository secretaryRepository = new SecretaryRepository();
            GuestRepository guestRepository = new GuestRepository();
            PatientRepository patientRepository = new PatientRepository();
            WorkingPeriodRepository workingPeriodRepository = new WorkingPeriodRepository();
            AllergyRepository allergyRepository = new AllergyRepository();
            DoctorRepository doctorRepository = new DoctorRepository();
            SpecializationRepository specializationRepository = new SpecializationRepository();
            ExaminationAppointmentRepository examinationAppointmentRepository = new ExaminationAppointmentRepository();
            ManagerRepository managerRepository = new ManagerRepository();
            RenovationRepository renovationRepository = new RenovationRepository();
            SurgeryRepository surgeryRepository = new SurgeryRepository();
            TherapyRepository therapyRepository = new TherapyRepository();
            PatientFileRepository patientFileRepository = new PatientFileRepository();

            var articleService = new ArticleService(articleRepository);
            var feedbackService = new FeedbackService(feedbackRepository);
            var surveyService = new SurveyService(surveyRepository);
            var consumablesService = new ConsumablesService(consumablesRepository);
            var roomService = new RoomService(roomRepository);
            var equipmentService = new EquipmentService(equipmentRepository,roomService);
            var roomTypeService = new RoomTypeService(roomTypeRepository);
            var ingredientService = new IngredientService(ingredientRepository);
            var medicineService = new MedicineService(medicineRepository,medicineTypeRepository);
            var userService = new UserService(userRepository);
            var cityService = new CityService(cityRepository);
            var countryService = new CountryService(countryRepository);
            var secretaryService = new SecretaryService(secretaryRepository);
            var guestService = new GuestService(guestRepository);
            var patientService = new PatientService(patientRepository);
            var doctorService = new DoctorService(doctorRepository);
            var workingPeriodService = new WorkingPeriodService(workingPeriodRepository,doctorService);
            var allergyService = new AllergyService(allergyRepository);
            var specializationService = new SpecializationService(specializationRepository);
            var examinationAppointmentService = new ExaminationAppointmentService(examinationAppointmentRepository, workingPeriodService, doctorService);
            var managerService = new ManagerService(managerRepository);
            var renovationService = new RenovationService(renovationRepository);
            var surgeryService = new SurgeryService(surgeryRepository);
            var therapyService = new TherapyService(therapyRepository);
            var patientFileService = new PatientFileService(patientFileRepository);

            ArticleController = new ArticleController(articleService);
            FeedbackController = new FeedbackController(feedbackService);
            SurveyController = new SurveyController(surveyService);
            RoomController = new RoomController(roomService,roomTypeService);
            EquipmentController = new EquipmentController(equipmentService,consumablesService);
            IngredientController = new IngredientController(ingredientService);
            MedicineController = new MedicineController(medicineService);
            UserController = new UserController(userService);
            LocationController = new LocationController(cityService,countryService);
            SecretaryController = new SecretaryController(secretaryService);
            GuestController = new GuestController(guestService);
            PatientController = new PatientController(patientService);
            WorkingPeriodController = new WorkingPeriodController(workingPeriodService);
            SpecializationController = new SpecializationController(specializationService);
            DoctorController = new DoctorController(doctorService,allergyService,specializationService);
            ExaminationAppointmentController = new ExaminationAppointmentController(examinationAppointmentService);
            ManagerController = new ManagerController(managerService);
            RenovationController = new RenovationController(renovationService);
            SurgeryController = new SurgeryController(surgeryService);
            TherapyController = new TherapyController(therapyService);
            PatientFileController = new PatientFileController(patientFileService);
        }
        public static IArticleController ArticleController { get; private set; }
        public static IFeedbackController FeedbackController { get; private set; }
        public static ISurveyController SurveyController { get; private set; }
        public static IEquipmentController EquipmentController { get; private set; }
        public static IRoomController RoomController { get; private set; }
        public static IIngredientController IngredientController { get; private set; }
        public static IMedicineController MedicineController { get; private set; }
        public static IUserController UserController { get; private set; }
        public static ILocationController LocationController { get; private set; }
        public static ISecretaryController SecretaryController { get; private set; }
        public static IGuestController GuestController { get; private set; }
        public static IPatientFileController PatientController { get; private set; }
        public static IWorkingPeriodController WorkingPeriodController { get; private set; }
        public static IDoctorController DoctorController { get; private set; }
        public static ISpecializationController SpecializationController { get; private set; }
        public static IExaminationAppointmentController ExaminationAppointmentController { get; private set; }
        public static IManagerController ManagerController { get; private set; }
        public static IRenovationController RenovationController { get; private set; }
        public static ISurgeryController SurgeryController { get; private set; }
        public static ITherapyController TherapyController { get; private set; }
        public static IPatietnFileController PatientFileController { get; private set; }
        
        

        
    }
}
