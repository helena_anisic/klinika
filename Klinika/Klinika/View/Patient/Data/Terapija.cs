﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Data
{
    class Terapija
    {
        public string Naziv { get; set; }
        public string Proizvodjac { get; set; }
        public string Pocetak { get; set; }
        public string Zavrsetak { get; set; }
        public string Napomena { get; set; }
        public static ObservableCollection<Terapija> Data()
        {
            ObservableCollection<Terapija> lista = new ObservableCollection<Terapija>();
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            lista.Add(new Terapija
            {
                Naziv = "Brufen 400mg",
                Proizvodjac = "Galenika",
                Pocetak = "13.4.2020",
                Zavrsetak = "15.4.2020",
                Napomena = ""
            });
            return lista;
        }
    }
}
