﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller.HospitalController;
using Controller.UsersController.DoctorController;
using Controller.UsersController.PatientController;
using Klinika.View.Patient;
using Model.users.patient;
using Model.users.user;

namespace Klinika.View.Secretary
{
    /// <summary>
    /// Interaction logic for Zakazivanje_termina.xaml
    /// </summary>
    public partial class Zakazivanje_termina : Page
    {
        
        public Zakazivanje_termina(Model.users.user.Patient p)
        {
            
            InitializeComponent();
            Model.users.user.Patient patient = new Model.users.user.Patient();
            patient.Id = p.Id;
            dateFrom.SelectedDate = DateTime.Now;
            dateTo.SelectedDate = DateTime.Now.AddDays(6);
            doctor.ItemsSource = App.DoctorController.GetAllDoctors();
           




        }

       

        private void PackIconMaterial_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }

        private void BtnUpravljanje_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new UpravljanjeRasporedom();
        }

        private void Odustanak_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Meni();
        }

        private void dateFromChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dateTo.SelectedDate == null)
                return;
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, null);

            Kolona1.Header = dateFrom.SelectedDate.Value.ToString("dd.MM.yyyy");
            Kolona2.Header = dateFrom.SelectedDate.Value.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = dateFrom.SelectedDate.Value.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = dateFrom.SelectedDate.Value.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = dateFrom.SelectedDate.Value.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = dateFrom.SelectedDate.Value.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = dateFrom.SelectedDate.Value.AddDays(6).ToString("dd.MM.yyyy");
        }
        private void dateToChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, null);

            Kolona1.Header = dateFrom.SelectedDate.Value.ToString("dd.MM.yyyy");
            Kolona2.Header = dateFrom.SelectedDate.Value.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = dateFrom.SelectedDate.Value.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = dateFrom.SelectedDate.Value.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = dateFrom.SelectedDate.Value.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = dateFrom.SelectedDate.Value.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = dateFrom.SelectedDate.Value.AddDays(6).ToString("dd.MM.yyyy");
        }
        private void Doctor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Model.users.user.Doctor)doctor.SelectedItem);
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if ((Model.users.user.Doctor)doctor.SelectedItem == null)
                return;
            DateTime pocetak = DateTime.ParseExact(Kolona1.Header.ToString(), "dd.MM.yyyy", null);
            MedOprema.ItemsSource = Data.Calendar.Data(pocetak, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Model.users.user.Doctor)doctor.SelectedItem);

            Kolona1.Header = pocetak.ToString("dd.MM.yyyy");
            Kolona2.Header = pocetak.AddDays(1).ToString("dd.MM.yyyy");
            Kolona3.Header = pocetak.AddDays(2).ToString("dd.MM.yyyy");
            Kolona4.Header = pocetak.AddDays(3).ToString("dd.MM.yyyy");
            Kolona5.Header = pocetak.AddDays(4).ToString("dd.MM.yyyy");
            Kolona6.Header = pocetak.AddDays(5).ToString("dd.MM.yyyy");
            Kolona7.Header = pocetak.AddDays(6).ToString("dd.MM.yyyy");
        }
        private void zakazi_Click(object sender, RoutedEventArgs e)
        {
            if (MedOprema.SelectedCells.Count <= 0)
                return;
            int kolona = MedOprema.SelectedCells[0].Column.DisplayIndex;
            int red = MedOprema.Items.IndexOf(MedOprema.SelectedCells[0].Item);
            String datum = MedOprema.SelectedCells[0].Column.Header.ToString();

            int sati = red / 2;
            int psati = sati;
            int minuta = (red % 2 == 1) ? 30 : 0;
            String termin = (sati < 10) ? "0" + sati.ToString() : sati.ToString();
            termin += ":";
            termin += (minuta == 30) ? "30" : "00";
            if (minuta == 30)
                sati++;
            if (sati == 24)
                sati = 0;
            termin += " - ";
            termin += (sati < 10) ? "0" + sati.ToString() : sati.ToString();
            termin += ":";
            termin += (minuta == 30) ? "00" : "30";

            Model.users.user.Doctor dc = (Model.users.user.Doctor)doctor.SelectedItem;

            DateTime date = DateTime.ParseExact(datum, "dd.MM.yyyy", null);
            date = date.AddHours(psati).AddMinutes(minuta);

            if (dc == null)
            {
                dc = App.ExaminationAppointmentController.GetFreeDoctorForTerm(date, date.AddHours(sati).AddMinutes(minuta));
            }


            Window win = new PotvrdaTermina1(termin, date, dc);
            win.ShowDialog();
            MedOprema.ItemsSource = Data.Calendar.Data(dateFrom.SelectedDate.Value, dateFrom.SelectedDate.Value, dateTo.SelectedDate.Value, (Model.users.user.Doctor)doctor.SelectedItem);

        }
        private void MedOprema_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void MedOprema_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {

        }


    }
}
