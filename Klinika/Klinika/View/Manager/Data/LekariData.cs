﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.View.Manager.Data
{
    class LekariData
    {
        public string ImeiPrezime { get; set; }
        public string Struka { get; set; }
        public string DatumRodjenja { get; set; }
        public string UlicaIBroj { get; set; }
        public string Mesto { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }

        public static ObservableCollection<LekariData> Data()
        {
            ObservableCollection<LekariData> lista = new ObservableCollection<LekariData>();
            lista.Add(new LekariData
            {
                ImeiPrezime = "Petar Petrovic",
                Struka = "Hirurgija",
                DatumRodjenja = "01.01.1912.",
                UlicaIBroj = "Njegoseva 12",
                Mesto = "Novi Sad",
                Email = "petar@petrovic.co.rs",
                Telefon = "060/200 200"
            });
            lista.Add(new LekariData
            {
                ImeiPrezime = "Mika Mikic",
                Struka = "Opsta praksa",
                DatumRodjenja = "12.04.1960.",
                UlicaIBroj = "Bulevar Oslobodjenja 12",
                Mesto = "Novi Sad",
                Email = "mika@mikic.co.rs",
                Telefon = "060/200 201"
            });
            lista.Add(new LekariData
            {
                ImeiPrezime = "Jadranka Petrovic",
                Struka = "Stomatologija",
                DatumRodjenja = "06.06.1956.",
                UlicaIBroj = "Jevrejska 1",
                Mesto = "Novi Sad",
                Email = "jadranka@bobica.co.rs",
                Telefon = "060/200 202"
            });
            return lista;
        }
    }
}
