﻿using Klinika.i18N;
using Klinika.View.Patient.Data;
using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.Pages
{
    /// <summary>
    /// Interaction logic for register.xaml
    /// </summary>
    public partial class register : Page
    {
        Patient patient = null;
        public register()
        {
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));

        }

        public register(Patient patient)
        {
            this.patient = patient;
            InitializeComponent();
            LocUtil.SwitchLanguage(this, LocUtil.GetCurrentCultureName(new Settings()));
            fillForm();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Patient patient = new Patient();
            patient.Name = name.Text;
            patient.Surname = surname.Text;
            patient.Id = id.Text;
            patient.PhoneNumber = phone.Text;
            if (dateOfBirth.SelectedDate != null)
                patient.DateOfBirth = dateOfBirth.SelectedDate.Value;
            if (male.IsChecked == true)
                patient.Gender = Gender.male;
            else
                patient.Gender = Gender.female;

            if (name.Text.Length != 0 && surname.Text.Length != 0 && id.Text.Length == 13 && dateOfBirth.SelectedDate != null)
            {
                Application.Current.MainWindow.Content = new register2(patient);
            }
            else
            {
                if(name.Text.Length == 0)
                {
                    name.BorderBrush = Brushes.Red;
                    nameMessage.Visibility = Visibility.Visible;
                }
                if(surname.Text.Length == 0)
                {
                    surname.BorderBrush = Brushes.Red;
                    surnameMessage.Visibility = Visibility.Visible;
                }
                if(id.Text.Length != 13)
                {
                    id.BorderBrush = Brushes.Red;
                    idMessage.Visibility = Visibility.Visible;
                }
                if(dateOfBirth.SelectedDate == null)
                {
                    borderKalendar.BorderBrush = Brushes.Red;
                    kalendarMessage.Visibility = Visibility.Visible;
                }
            }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new login();
        }

        private void fillForm()
        {
            name.Text = patient.Name;
            surname.Text = patient.Surname;
            if (patient.DateOfBirth != null)
                dateOfBirth.SelectedDate = patient.DateOfBirth;
            else
                dateOfBirth.Text = "empty";
            id.Text = patient.Id;
            phone.Text = patient.PhoneNumber;
            if (patient.Gender == Gender.male)
                male.IsChecked = true;
            else
                female.IsChecked = true;
        }

        private void name_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (name.Text.Length == 0)
            {
                name.BorderBrush = Brushes.Red;
                nameMessage.Visibility = Visibility.Visible;
            }
            else
            {
                name.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                nameMessage.Visibility = Visibility.Hidden;
            }

        }

        private void surname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (surname.Text.Length == 0)
            {
                surname.BorderBrush = Brushes.Red;
                surnameMessage.Visibility = Visibility.Visible;
            }
            else
            {
                surname.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                surnameMessage.Visibility = Visibility.Hidden;
            }
        }

        private void id_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(id.Text.Length != 13)
            {
                id.BorderBrush = Brushes.Red;
                idMessage.Visibility = Visibility.Visible;
            }
            else
            {
                id.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                idMessage.Visibility = Visibility.Hidden;
            }
        }

        private void dateOfBirth_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dateOfBirth.SelectedDate == null)
            {
                borderKalendar.BorderBrush = Brushes.Red;
                kalendarMessage.Visibility = Visibility.Visible;
            }
            else
            {
                borderKalendar.BorderBrush = (Brush)(new BrushConverter().ConvertFrom("#121212"));
                kalendarMessage.Visibility = Visibility.Hidden;
            }
        }


    }
}
