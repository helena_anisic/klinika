﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for SaleStatistika.xaml
    /// </summary>
    public partial class SaleStatistika : Page
    {
        public SaleStatistika()
        {
            InitializeComponent();
            this.Cartesian();
        }

        public void Cartesian()
        {
            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title="Sala S101:", Values=new ChartValues<double>{4,6,5,5},
                }
            };

            Labels = new[] { "8h", "9h", "10h", "11h" };
            DataContext = this;
        }

        //
        public Func<double, string> yFormatter { get; set; }
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
    }
}