﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.UserController
{
    public interface ILocationController
    {
        List<City> GetAllCitiesByCountryName(String countryName);
        Country GetCountryByName(String countryName);
        City GetCityByName(String cityName);
        List<Country> GetAllCountries();

    }
}
