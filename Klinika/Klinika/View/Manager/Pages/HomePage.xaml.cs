﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public HomePage()
        {
            InitializeComponent();
            ContentBox.Content = new HomeP();
        }

        public void setContentBox(Page page)
        {
            ContentBox.Content = page;
        }
        private void ListViewItem_MouseEnter(object sender, MouseEventArgs e)
        {
            if (Tg_Btn.IsChecked == true)
            {
                tt_home.Visibility = Visibility.Collapsed;
                tt_Magacin.Visibility = Visibility.Collapsed;
                tt_Sale.Visibility = Visibility.Collapsed;
                tt_Renoviranja.Visibility = Visibility.Collapsed;
                tt_Lekari.Visibility = Visibility.Collapsed;
                tt_MojNalog.Visibility = Visibility.Collapsed;
            }
            else
            {
                tt_home.Visibility = Visibility.Visible;
                tt_Magacin.Visibility = Visibility.Visible;
                tt_Sale.Visibility = Visibility.Visible;
                tt_Renoviranja.Visibility = Visibility.Visible;
                tt_Lekari.Visibility = Visibility.Visible;
                tt_MojNalog.Visibility = Visibility.Visible;
            }
        }

        private void ListViewItem_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void mdl(object sender, MouseButtonEventArgs e)
        {

        }

        private void SLC(object sender, SelectionChangedEventArgs e)
        {

        }

        private void MDCL(object sender, MouseButtonEventArgs e)
        {

        }

        private void mm(object sender, RoutedEventArgs e)
        {
            if (Tg_Btn.IsChecked != true)
            {
                MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                PotrosnaOprema.Visibility = Visibility.Collapsed;
                Lekovi.Visibility = Visibility.Collapsed;
                PrikazSala.Visibility = Visibility.Collapsed;
                OpremaUSalama.Visibility = Visibility.Collapsed;
                Statistika.Visibility = Visibility.Collapsed;
                ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                PremestiOpremu.Visibility = Visibility.Collapsed;
                SpojPodeliSale.Visibility = Visibility.Collapsed;
                NapraviRaspored.Visibility = Visibility.Collapsed;
                IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                SpisakLekara.Visibility = Visibility.Collapsed;
                DodajLekara.Visibility = Visibility.Collapsed;
                OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                PovratnaInfromacija.Visibility = Visibility.Collapsed;
                OdjaviSe.Visibility = Visibility.Collapsed;
            }
            if (HomeBtn.Equals(sender) == true)
            {
                ContentBox.Content = new HomeP();
                if (Tg_Btn.IsChecked == true)
                {
                    ContentBox.Content = new HomeP();
                    MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                    PotrosnaOprema.Visibility = Visibility.Collapsed;
                    Lekovi.Visibility = Visibility.Collapsed;
                    PrikazSala.Visibility = Visibility.Collapsed;
                    OpremaUSalama.Visibility = Visibility.Collapsed;
                    Statistika.Visibility = Visibility.Collapsed;
                    ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                    PremestiOpremu.Visibility = Visibility.Collapsed;
                    SpojPodeliSale.Visibility = Visibility.Collapsed;
                    NapraviRaspored.Visibility = Visibility.Collapsed;
                    IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                    SpisakLekara.Visibility = Visibility.Collapsed;
                    DodajLekara.Visibility = Visibility.Collapsed;
                    OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                    PovratnaInfromacija.Visibility = Visibility.Collapsed;
                    OdjaviSe.Visibility = Visibility.Collapsed;
                }
            }
            else if (Magacin.Equals(sender))
            {
                ContentBox.Content = new Magacin();
                if (MedicinskaOpremaIAparati.Visibility == Visibility.Collapsed && Tg_Btn.IsChecked == true)
                {
                    MedicinskaOpremaIAparati.Visibility = Visibility.Visible;
                    PotrosnaOprema.Visibility = Visibility.Visible;
                    Lekovi.Visibility = Visibility.Visible;
                    PrikazSala.Visibility = Visibility.Collapsed;
                    OpremaUSalama.Visibility = Visibility.Collapsed;
                    Statistika.Visibility = Visibility.Collapsed;
                    ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                    PremestiOpremu.Visibility = Visibility.Collapsed;
                    SpojPodeliSale.Visibility = Visibility.Collapsed;
                    NapraviRaspored.Visibility = Visibility.Collapsed;
                    IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                    SpisakLekara.Visibility = Visibility.Collapsed;
                    DodajLekara.Visibility = Visibility.Collapsed;
                    OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                    PovratnaInfromacija.Visibility = Visibility.Collapsed;
                    OdjaviSe.Visibility = Visibility.Collapsed;
                }

                else
                {
                    MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                    PotrosnaOprema.Visibility = Visibility.Collapsed;
                    Lekovi.Visibility = Visibility.Collapsed;
                }
            }
            else if (Sale.Equals(sender))
            {
                ContentBox.Content = new Sale();
                if (PrikazSala.Visibility == Visibility.Collapsed && Tg_Btn.IsChecked == true)
                {
                    PrikazSala.Visibility = Visibility.Visible;
                    OpremaUSalama.Visibility = Visibility.Visible;
                    Statistika.Visibility = Visibility.Visible;
                    MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                    PotrosnaOprema.Visibility = Visibility.Collapsed;
                    Lekovi.Visibility = Visibility.Collapsed;
                    ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                    PremestiOpremu.Visibility = Visibility.Collapsed;
                    SpojPodeliSale.Visibility = Visibility.Collapsed;
                    NapraviRaspored.Visibility = Visibility.Collapsed;
                    IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                    SpisakLekara.Visibility = Visibility.Collapsed;
                    DodajLekara.Visibility = Visibility.Collapsed;
                    OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                    PovratnaInfromacija.Visibility = Visibility.Collapsed;
                    OdjaviSe.Visibility = Visibility.Collapsed;
                }

                else
                {
                    PrikazSala.Visibility = Visibility.Collapsed;
                    OpremaUSalama.Visibility = Visibility.Collapsed;
                    Statistika.Visibility = Visibility.Collapsed;
                }
            }
            else if (Renoviranja.Equals(sender))
            {
                ContentBox.Content = new Renoviranja();
                if (ZakažiRenoviranje.Visibility == Visibility.Collapsed && Tg_Btn.IsChecked == true)
                {
                    ZakažiRenoviranje.Visibility = Visibility.Visible;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Visible;
                    PremestiOpremu.Visibility = Visibility.Visible;
                    SpojPodeliSale.Visibility = Visibility.Visible;
                    MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                    PotrosnaOprema.Visibility = Visibility.Collapsed;
                    Lekovi.Visibility = Visibility.Collapsed;
                    PrikazSala.Visibility = Visibility.Collapsed;
                    OpremaUSalama.Visibility = Visibility.Collapsed;
                    Statistika.Visibility = Visibility.Collapsed;
                    NapraviRaspored.Visibility = Visibility.Collapsed;
                    IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                    SpisakLekara.Visibility = Visibility.Collapsed;
                    DodajLekara.Visibility = Visibility.Collapsed;
                    OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                    PovratnaInfromacija.Visibility = Visibility.Collapsed;
                    OdjaviSe.Visibility = Visibility.Collapsed;
                }

                else
                {
                    ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                    PremestiOpremu.Visibility = Visibility.Collapsed;
                    SpojPodeliSale.Visibility = Visibility.Collapsed;
                }
            }
            else if (Lekari.Equals(sender))
            {
                ContentBox.Content = new Lekari();
                if (NapraviRaspored.Visibility == Visibility.Collapsed && Tg_Btn.IsChecked == true)
                {
                    NapraviRaspored.Visibility = Visibility.Visible;
                    IzveštajOPlanuRada.Visibility = Visibility.Visible;
                    SpisakLekara.Visibility = Visibility.Visible;
                    DodajLekara.Visibility = Visibility.Visible;
                    MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                    PotrosnaOprema.Visibility = Visibility.Collapsed;
                    Lekovi.Visibility = Visibility.Collapsed;
                    PrikazSala.Visibility = Visibility.Collapsed;
                    OpremaUSalama.Visibility = Visibility.Collapsed;
                    Statistika.Visibility = Visibility.Collapsed;
                    ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                    PremestiOpremu.Visibility = Visibility.Collapsed;
                    SpojPodeliSale.Visibility = Visibility.Collapsed;
                    OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                    PovratnaInfromacija.Visibility = Visibility.Collapsed;
                    OdjaviSe.Visibility = Visibility.Collapsed;
                }

                else
                {
                    NapraviRaspored.Visibility = Visibility.Collapsed;
                    IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                    SpisakLekara.Visibility = Visibility.Collapsed;
                    DodajLekara.Visibility = Visibility.Collapsed;
                }
            }
            else if (MojNalog.Equals(sender))
            {
                ContentBox.Content = new MojNalog();
                LV.SelectedItems.Clear();
                if (OsnovnaPodešavanja.Visibility == Visibility.Collapsed && Tg_Btn.IsChecked == true)
                {
                    OsnovnaPodešavanja.Visibility = Visibility.Visible;
                    PovratnaInfromacija.Visibility = Visibility.Visible;
                    OdjaviSe.Visibility = Visibility.Visible;
                    MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
                    PotrosnaOprema.Visibility = Visibility.Collapsed;
                    Lekovi.Visibility = Visibility.Collapsed;
                    PrikazSala.Visibility = Visibility.Collapsed;
                    OpremaUSalama.Visibility = Visibility.Collapsed;
                    Statistika.Visibility = Visibility.Collapsed;
                    ZakažiRenoviranje.Visibility = Visibility.Collapsed;
                    PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
                    PremestiOpremu.Visibility = Visibility.Collapsed;
                    SpojPodeliSale.Visibility = Visibility.Collapsed;
                    NapraviRaspored.Visibility = Visibility.Collapsed;
                    IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
                    SpisakLekara.Visibility = Visibility.Collapsed;
                    DodajLekara.Visibility = Visibility.Collapsed;
                }
                else
                {
                    OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
                    PovratnaInfromacija.Visibility = Visibility.Collapsed;
                    OdjaviSe.Visibility = Visibility.Collapsed;
                }
            }
            else if (MedicinskaOpremaIAparati.Equals(sender))
            {
                ContentBox.Content = new MedicinskaOpremaIAparati();
            }
            else if (PotrosnaOprema.Equals(sender))
            {
                ContentBox.Content = new PotrosnaOprema();
            }
            else if (Lekovi.Equals(sender))
            {
                ContentBox.Content = new Lekovi();
            }
            else if (PrikazSala.Equals(sender))
            {
                ContentBox.Content = new PrikazSala();
            }
            else if (OpremaUSalama.Equals(sender))
            {
                ContentBox.Content = new OpremaUSalama();
            }
            else if (Statistika.Equals(sender))
            {
                ContentBox.Content = new SaleStatistika();
            }
            else if (OsnovnaPodešavanja.Equals(sender))
            {
                ContentBox.Content = new MojNalogIzmena();
            }
            else if (PovratnaInfromacija.Equals(sender))
            {
                ContentBox.Content = new PovratnaInformacija();
            }
            else if (OdjaviSe.Equals(sender))
            {
                Application.Current.MainWindow.Content = new Login();
            }
            else if (SpisakLekara.Equals(sender))
            {
                ContentBox.Content = new SpisakLekara();
            }
            else if (DodajLekara.Equals(sender))
            {
                ContentBox.Content = new DodajLekara();
            }
            else if (ZakažiRenoviranje.Equals(sender))
            {
                ContentBox.Content = new ZakaziRenoviranje();
            }
            else if (PrikazZakazanihRenoviranja.Equals(sender))
            {
                ContentBox.Content = new ZakazanaRenoviranja();
            }
            else if (PremestiOpremu.Equals(sender))
            {
                ContentBox.Content = new PremestiOpremu();
            }
            else if (SpojPodeliSale.Equals(sender))
            {
                ContentBox.Content = new SpojPodeliSale();
            }
            else if (IzveštajOPlanuRada.Equals(sender))
            {
                ContentBox.Content = new RasporedZaLekare();
            }
            else if (NapraviRaspored.Equals(sender))
            {
                ContentBox.Content = new NapraviRaspored();
            }
        }

        private void Tg_Btn_unchecked(object sender, RoutedEventArgs e)
        {
            MedicinskaOpremaIAparati.Visibility = Visibility.Collapsed;
            PotrosnaOprema.Visibility = Visibility.Collapsed;
            Lekovi.Visibility = Visibility.Collapsed;
            PrikazSala.Visibility = Visibility.Collapsed;
            OpremaUSalama.Visibility = Visibility.Collapsed;
            Statistika.Visibility = Visibility.Collapsed;
            ZakažiRenoviranje.Visibility = Visibility.Collapsed;
            PrikazZakazanihRenoviranja.Visibility = Visibility.Collapsed;
            PremestiOpremu.Visibility = Visibility.Collapsed;
            SpojPodeliSale.Visibility = Visibility.Collapsed;
            NapraviRaspored.Visibility = Visibility.Collapsed;
            IzveštajOPlanuRada.Visibility = Visibility.Collapsed;
            SpisakLekara.Visibility = Visibility.Collapsed;
            DodajLekara.Visibility = Visibility.Collapsed;
            OsnovnaPodešavanja.Visibility = Visibility.Collapsed;
            PovratnaInfromacija.Visibility = Visibility.Collapsed;
            OdjaviSe.Visibility = Visibility.Collapsed;
        }

        private void Button_Click_Logout(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Login();
        }
    }
}
