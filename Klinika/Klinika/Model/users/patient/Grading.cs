// File:    Grading.cs
// Author:  helen
// Created: Sunday, April 19, 2020 7:25:30 PM
// Purpose: Definition of Enum Grading

using System;

namespace Model.users.patient
{
   public enum Grading
   {
      veryPoor,
      poor,
      average,
      good,
      excellent
   }
}