// File:    DoctorService.cs
// Author:  Admin
// Created: Saturday, May 30, 2020 1:31:43 AM
// Purpose: Definition of Class DoctorService

using Klinika.Service.UsersServices.DoctorServices;
using Model.users.user;
using Repository.UsersRepositories.DoctorRepositories;
using System;
using System.Collections.Generic;

namespace Service.UsersServices.DoctorServices
{
    public class DoctorService : IDoctorService
    {
        private DoctorRepository _doctorRepository;

        public DoctorService(DoctorRepository doctorRepository)
        {
            _doctorRepository = doctorRepository;
        }
        public Boolean CreateDoctor(Doctor doctor)
        {
            List<Doctor> doctors = _doctorRepository.GetAll(DoctorRepository.path);
            if (doctors == null)
            {
                doctors = new List<Doctor>();
            }
            doctors.Add(doctor);
            _doctorRepository.Save(doctors, DoctorRepository.path);
            return true;
        }

        public Boolean EditDoctor(Doctor doctor)
        {
            if (DeleteDoctor(doctor) == false)
                return false;
            return CreateDoctor(doctor);
        }

        public List<Doctor> GetAllDoctors()
        {
            return _doctorRepository.GetAll(DoctorRepository.path);
        }

        public Boolean DeleteDoctor(Doctor doctor)
        {
            List<Doctor> doctors = _doctorRepository.GetAll(DoctorRepository.path);

            if (doctors == null)
                return false;

            foreach (Doctor d in doctors)
            {
                if (d.Id.Equals(doctor.Id))
                {
                    doctors.Remove(d);
                    break;
                }
            }

            return _doctorRepository.Save(doctors, DoctorRepository.path);
        }

        public Doctor GetDoctorById(String id)
        {
            List<Doctor> doctors = GetAllDoctors();
            if (doctors == null)
                return null;
            foreach (Doctor doctor in doctors)
            {
                if (doctor.Id.Equals(id))
                {
                    return doctor;
                }
            }
            return null;
        }
    }
}