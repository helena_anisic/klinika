﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Model.Hospital;
using Model.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for DodajOpremu.xaml
    /// </summary>
    public partial class DodajOpremu : Window
    {
        public DodajOpremu()
        {
            InitializeComponent();
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            Room room = App.RoomController.GetRoomById("Magacin");
            room.equipment.Add(new Equipment() { name = name.Text, id = id.Text });
            App.RoomController.EditRoom(room);
            this.Close();
        }
    }
}
