﻿using Controller.UsersController.DoctorController;
using Controller.UsersController.ManagerController;
using Controller.UsersController.PatientController;
using Model.users.manager;
using Model.users.patient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Klinika.View.Manager.Data
{
    class TerminiData
    {
        public String sati { get; set; }
        public String opis { get; set; }

        public static ObservableCollection<TerminiData> Data(DateTime date, String roomId)
        {
            ObservableCollection<TerminiData> lista = new ObservableCollection<TerminiData>();

            List<string> termini = new List<string>();
            termini.Add("00:00 - 00:30");
            termini.Add("00:30 - 01:00");
            termini.Add("01:00 - 01:30");
            termini.Add("01:30 - 02:00");
            termini.Add("02:00 - 02:30");
            termini.Add("02:30 - 03:00");
            termini.Add("03:00 - 03:30");
            termini.Add("03:30 - 04:00");
            termini.Add("04:00 - 04:30");
            termini.Add("04:30 - 05:00");
            termini.Add("05:00 - 05:30");
            termini.Add("05:30 - 06:00");
            termini.Add("06:00 - 06:30");
            termini.Add("06:30 - 07:00");
            termini.Add("07:00 - 07:30");
            termini.Add("07:30 - 08:00");
            termini.Add("08:00 - 08:30");
            termini.Add("08:30 - 09:00");
            termini.Add("09:00 - 09:30");
            termini.Add("09:30 - 10:00");
            termini.Add("10:00 - 10:30");
            termini.Add("10:30 - 11:00");
            termini.Add("11:00 - 11:30");
            termini.Add("11:30 - 12:00");
            termini.Add("12:00 - 12:30");
            termini.Add("12:30 - 13:00");
            termini.Add("13:00 - 13:30");
            termini.Add("13:30 - 14:00");
            termini.Add("14:00 - 14:30");
            termini.Add("14:30 - 15:00");
            termini.Add("15:00 - 15:30");
            termini.Add("15:30 - 16:00");
            termini.Add("16:00 - 16:30");
            termini.Add("16:30 - 17:00");
            termini.Add("17:00 - 17:30");
            termini.Add("17:30 - 18:00");
            termini.Add("18:00 - 18:30");
            termini.Add("18:30 - 19:00");
            termini.Add("19:00 - 19:30");
            termini.Add("19:30 - 20:00");
            termini.Add("20:00 - 20:30");
            termini.Add("20:30 - 21:00");
            termini.Add("21:00 - 21:30");
            termini.Add("21:30 - 22:00");
            termini.Add("22:00 - 22:30");
            termini.Add("22:30 - 23:00");
            termini.Add("23:00 - 23:30");
            termini.Add("23:30 - 00:00");

            foreach (String t in termini)
            {
                bool upisano = false;
                List<Model.users.patient.ExaminationAppointment> exList = App.ExaminationAppointmentController.GetAllAppointments();
                foreach (Model.users.patient.ExaminationAppointment e in exList)
                {
                    String tt = e.StartDateTime.ToString("HH:mm") + " - " + e.StartDateTime.AddMinutes(30).ToString("HH:mm");
                    if (e.StartDateTime.Date == date.Date && e.RoomId.Equals(roomId) && tt.Equals(t))
                    {
                        lista.Add(new TerminiData { sati = t, opis = "Pregled   " + App.DoctorController.GetDoctorById(e.DoctorId).ToString() });
                        upisano = true;
                        break;
                    }
                }
                List<Renovation> renovations = App.RenovationController.GetAllRenovations();
                foreach (Renovation r in renovations)
                {

                    int sod = int.Parse(t.Substring(0, 2));
                    int sdo = int.Parse(t.Substring(3, 2));
                    int eod = int.Parse(t.Substring(8, 2));
                    int edo = int.Parse(t.Substring(11, 2));

                    DateTime currStart = new DateTime(date.Year, date.Month, date.Day, sod, sdo, 0);
                    DateTime currEnd = new DateTime(date.Year, date.Month, date.Day, (eod == 0) ? 23 : eod, edo, 0);
                    if (eod == 0)
                        currEnd = currEnd.AddHours(1);
                    if (r.StartDateTime <= currStart && r.EndDateTime >= currEnd)
                    {
                        if (r.RoomId.Equals(roomId))
                        {
                            lista.Add(new TerminiData { sati = t, opis = "Renoviranje" });
                            upisano = true;
                            break;
                        }
                    }
                }
                List<Surgery> surgeries = App.SurgeryController.GetAllSurgeries();
                foreach (Surgery s in surgeries)
                {

                    int sod = int.Parse(t.Substring(0, 2));
                    int sdo = int.Parse(t.Substring(3, 2));
                    int eod = int.Parse(t.Substring(8, 2));
                    int edo = int.Parse(t.Substring(11, 2));

                    DateTime currStart;
                    DateTime currEnd;
                    if (t.Equals("00:00 - 00:30"))
                    {
                        currStart = new DateTime(date.AddDays(-1).Year, date.AddDays(-1).Month, date.AddDays(-1).Day, 23, sdo, 0);
                        currEnd = new DateTime(date.AddDays(-1).Year, date.AddDays(-1).Month, date.AddDays(-1).Day, 23, edo, 0);
                        currStart = currStart.AddHours(1);
                        currEnd = currEnd.AddHours(1);
                    }
                    else if (t.Equals("23:30 - 00:00"))
                    {
                        currStart = new DateTime(date.Year, date.Month, date.Day, 23, sdo, 0);
                        currEnd = new DateTime(date.Year, date.Month, date.Day, 23, edo, 0);
                        currEnd = currEnd.AddHours(1);
                    }
                    else
                    {
                        currStart = new DateTime(date.Year, date.Month, date.Day, sod, sdo, 0);
                        currEnd = new DateTime(date.Year, date.Month, date.Day, eod, edo, 0);
                    }
                    if (s.StartDateTime <= currStart && s.EndDateTime >= currEnd)
                    {
                        if (s.RoomId.Equals(roomId))
                        {
                            lista.Add(new TerminiData { sati = t, opis = "Operacija" });
                            upisano = true;
                            break;
                        }
                    }
                }
                if (upisano == false)
                {
                    lista.Add(new TerminiData { sati = t, opis = "" });
                }
                else
                {
                    upisano = false;
                }
            }

            return lista;
        }
    }
}
