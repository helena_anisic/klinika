﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Controller.UsersController.PatientController
{
    public interface IPatientFileController
    {
        Boolean CreatePatient(Patient patient);
        Boolean EditPatient(Patient patient);
        Boolean DeletePatient(Patient patient);
        Patient GetPatientById(String id);
        Patient GetPatientByAccount(Account account);
        List<Patient> GetAllPatients();

    }
}
