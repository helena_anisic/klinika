﻿using Controller.HospitalController;
using Klinika.Controller.HospitalController;
using Model.Storage;
using Model.users.doctor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klinika.View.Manager.Pages
{
    /// <summary>
    /// Interaction logic for DodajLek.xaml
    /// </summary>
    public partial class DodajLek : Page
    {
        public Medicine medicine = new Medicine();
        public DodajLek()
        {
            InitializeComponent();
            if (App.Pomoc == false)
                upitnik.Visibility = Visibility.Hidden;
            else
                upitnik.Visibility = Visibility.Visible;
            List<MedicineType> medicineTypes = App.MedicineController.GetAllMedicinesTypes();
            if (medicineType != null)
            {
                foreach (MedicineType mType in medicineTypes)
                {
                    medicineType.Items.Add(mType);
                }
            }
            RefreshListBox();
        }

        private void Button_Click_Odustani(object sender, RoutedEventArgs e)
        {
            HomePage hp = new HomePage();
            hp.setContentBox(new Lekovi());
            Application.Current.MainWindow.Content = hp;
        }

        private void Button_Click_Dodaj(object sender, RoutedEventArgs e)
        {
            bool valid = true;
            if (id.Text.Equals(""))
            {
                idBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                idBorder.BorderBrush = Brushes.Silver;
            }
            if (name.Text.Equals(""))
            {
                nameBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                nameBorder.BorderBrush = Brushes.Silver;
            }
            if (manufacturer.Text.Equals(""))
            {
                manufacturerBorder.BorderBrush = Brushes.Red;
                valid = false;
            }
            else
            {
                manufacturerBorder.BorderBrush = Brushes.Silver;
            }
            if (valid)
            {
                logErr.Visibility = Visibility.Hidden;
                medicine.Status = MedicineStatus.inProces;
                readForm();
                App.MedicineController.CreateMedicine(medicine);
                HomePage hp = new HomePage();
                hp.setContentBox(new Lekovi());
                Application.Current.MainWindow.Content = hp;
            }
            else
            {
                logErr.Visibility = Visibility.Visible;
            }
        }

        private void PackIconMaterial_MouseEnter(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Visible;
        }

        private void PackIconMaterial_MouseLeave(object sender, MouseEventArgs e)
        {
            help1.Visibility = Visibility.Hidden;
        }

        private void DodajSastojak_Click(object sender, RoutedEventArgs e)
        {
            DodajSastojak ds = new DodajSastojak(medicine);
            ds.ShowDialog();
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            ingredientsList.Items.Clear();
            if (medicine.Ingredients != null)
            {
                List<IngredientQuantity> ingredients = medicine.Ingredients;
                foreach (IngredientQuantity ingredient in ingredients)
                {
                    ingredientsList.Items.Add(ingredient);
                }
            }
        }

        private void readForm()
        {
            medicine.Id =id.Text;
            medicine.Name = name.Text;
            medicine.Manufacturer = manufacturer.Text;
            if (!quantity.Text.Equals(""))
                medicine.Quantity = Int32.Parse(quantity.Text);
            medicine.Description = description.Text;
            medicine.MedicineType = (MedicineType)medicineType.SelectedItem;

        }

        private void Id_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
