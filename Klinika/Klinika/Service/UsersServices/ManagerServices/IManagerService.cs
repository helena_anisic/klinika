﻿using Model.users.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klinika.Service.UsersServices.ManagerServices
{
    public interface IManagerService
    {
        Boolean CreateManager(Manager manager);
        Boolean EditManager(Manager manager);
        Boolean DeleteManager(Manager manager);
        Manager GetManagerByAccount(Account account);
        List<Manager> GetAllManagers();

    }
}
